// -*- C++ -*-

#define STLIB_PERFORMANCE

#include "stlib/ads/utility/ParseOptionsArguments.h"
#include "stlib/geom/kernel/Ball.h"
#include "stlib/numerical/constants.h"
#include "stlib/sfc/DoOverlap.h"
#include "stlib/sfc/OverlapQuery.h"
#include "stlib/sfc/overlapMpi.h"
#include "stlib/performance/PerformanceMpi.h"

#include <random>

// The program name.
std::string programName;

std::size_t const DefaultNumObjects = 1 << 20;

// Exit with a usage message.
void
helpMessage()
{
  if (stlib::mpi::commRank() == 0) {
    std::cout
      << "Usage:\n"
      << programName
      << " [-n=N] [-r=R] [-h]\n"
      << "-n: The number of balls per process. The default is "
      << DefaultNumObjects << ".\n"
      << "-r: The radius of each ball. By default the balls have the same "
      << "total volume as the unit cube on each process.\n"
      << "-t: Rotate the coordinates basis.\n"
      << "-b: Use the bounding box method for gathering.\n"
      << "-d: Distribute the balls with an SFC.\n"
      << "-s: Locally sort the balls with an SFC.\n"
      << "-z: Locally sort the balls by the z coordinate.\n"
      << '\n'
      << "Note that -s and -z may not both be specified.\n";
  }
  MPI_Finalize();
  exit(0);
}

int
main(int argc, char* argv[])
{
  constexpr std::size_t Dimension = 3;
  typedef stlib::sfc::Traits<Dimension, float> Traits;
  typedef Traits::Float Float;
  typedef Traits::BBox BBox;
  typedef stlib::geom::Ball<Float, Dimension> Object;

  using stlib::performance::Scope;
  using stlib::performance::start;
  using stlib::performance::stop;
  using stlib::performance::record;

  // MPI initialization.
  MPI_Init(&argc, &argv);
  MPI_Comm const comm = MPI_COMM_WORLD;
  int const commSize = stlib::mpi::commSize();
  int const commRank = stlib::mpi::commRank();
  record("MPI processes", commSize);

  // Parse the options.
  stlib::ads::ParseOptionsArguments parser(argc, argv);
  programName = parser.getProgramName();
  if (parser.getOption('h')) {
    helpMessage();
  }
  std::size_t numObjects = DefaultNumObjects;
  parser.getOption('n', &numObjects);
  Float radius = 0;
  parser.getOption('r', &radius);
  if (radius == 0) {
    // Set the total volume of the objects to match that of the unit cube.
    radius = std::pow(3. / (4. * numObjects * 
                            stlib::numerical::Constants<double>::Pi()), 1./3.);
  }
  record("Ball radius", radius);
  std::array<std::array<Float, Dimension>, Dimension> basis =
    {{{{1,0,0}}, {{0,1,0}}, {{0,0,1}}}};
  if (parser.getOption('t')) {
    //  1  1  1
    // -1  1  0
    // -1 -1  2
    Float const isr3 = 1 / std::sqrt(Float(3));
    Float const isr2 = 1 / std::sqrt(Float(2));
    Float const isr6 = 1 / std::sqrt(Float(6));
    basis = std::array<std::array<Float, Dimension>, Dimension>
      {{{{isr3,isr3,isr3}}, {{-isr2,isr2,0}}, {{-isr6,-isr6,2*isr6}}}};
  }
  bool const useBBox = parser.getOption('b');
  bool const distributeWithSfc = parser.getOption('d');
  bool const sortSfc = parser.getOption('s');
  bool const sortZ = parser.getOption('z');
  if (sortSfc && sortZ) {
    helpMessage();
  }

  // Determine the extents for the process grid.
  std::array<std::size_t, Dimension> extents = {{1, 1, 1}};
  while (product(extents) < std::size_t(commSize)) {
    *std::min_element(extents.begin(), extents.end()) *= 2;
  }
  // Calculate the lower corner of the Cartesian domain for this process.
  std::array<std::size_t, Dimension> const strides =
    {{1, extents[0], extents[0] * extents[1]}};
  std::array<std::size_t, Dimension> multiIndex;
  {
    std::size_t index = commRank;
    multiIndex[2] = index / strides[2];
    index = index % strides[2];
    multiIndex[1] = index / strides[1];
    index = index % strides[1];
    multiIndex[0] = index;
  }
  std::array<Float, Dimension> lowerCorner = {{0, 0, 0}};
  for (std::size_t i = 0; i != Dimension; ++i) {
    lowerCorner += Float(multiIndex[i]) * basis[i];
  }

  // Uniformly-distributed random points for the centers.
  std::vector<Object> objects(numObjects, Object{lowerCorner, radius});
  std::mt19937_64 engine(commRank);
  std::uniform_real_distribution<Float> distribution(0, 1);
  for (auto&& object: objects) {
    for (std::size_t i = 0; i != Dimension; ++i) {
      object.center += distribution(engine) * basis[i];
    }
  }

  if (distributeWithSfc) {
    stlib::sfc::distribute<Float, Dimension>(&objects, comm);
  }
  record("Number of objects", objects.size());

  if (sortSfc) {
    Scope _("Sort local objects with an SFC");
    stlib::sfc::sortByCodes<std::uint32_t>(&objects);
  }
  else if (sortZ) {
    Scope _("Sort local objects by the z coordinate");
    std::sort(objects.begin(), objects.end(),
              [](Object const& a, Object const& b)
              {return a.center[2] < b.center[2];});
  }

  // Barrier to make timings more consistent.
  stlib::mpi::barrier();

  // Gather the relevant objects. The query windows are just bounding boxes 
  // around the local objects.
  std::vector<BBox> const objectBoxes = stlib::geom::bboxForEach<BBox>
    (objects.begin(), objects.end());
  std::vector<Object> relevant;
  if (useBBox) {
    relevant = stlib::sfc::gatherOverlappingBBox(objectBoxes, objects, comm);
  }
  else {
    relevant = stlib::sfc::gatherOverlapping(objectBoxes, objects, comm);
  }
  record("Number of relevant objects", relevant.size());

  {
    // Barrier to make timings more consistent.
    stlib::mpi::barrier();
    // Determine the balls that overlap any other (including itself). This is 
    // a useless calculation. I just do it to assess the performance of 
    // doOverlap().
    Scope _("Do overlap tests");
    std::vector<bool> const doOverlap =
      stlib::sfc::doOverlap(stlib::geom::bboxForEach<BBox>
                            (relevant.begin(), relevant.end()), objectBoxes);
    record("Number of overlapping objects",
           std::count(doOverlap.begin(), doOverlap.end(), true));
  }

  {
    // Barrier to make timings more consistent.
    stlib::mpi::barrier();
    // Determine the balls that overlap.
    Scope _("Overlap queries");
    stlib::container::PackedArrayOfArrays<std::size_t> overlapping =
      stlib::sfc::overlapQuery(stlib::geom::bboxForEach<BBox>
                               (relevant.begin(), relevant.end()), objectBoxes);
  }

  stlib::performance::print();

  MPI_Finalize();
  return 0;
}
