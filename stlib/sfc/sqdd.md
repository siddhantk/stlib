# Spatial Queries with Distributed Data

# Introduction

## Spatial Query Examples

Spatial queries are common in scientific simulations. First we list a
few examples so that we understand the kinds of objects and queries
that may be involved. In particle simulations with short range
interactions, one must determine the neighboring particles within the
interaction distance, *r*, for each of the particles. That is, for
each particle, one stores the list of particles that are contained
within the ball of radius *r*, centered at the particle position. This
is typically accomplished by performing orthogonal range queries with
axis-aligned bounding boxes for the interaction balls and then
computing distance to determine which of the recorded particles are
within the interaction distance.

Some simulations involve overset meshes that overlap background
meshes. Here one must calculate the intersection between the two. If
a background cell is completely covered by the overset mesh, the cell
is inactivated. If it is partially covered, its volume must be
adjusted in order to not double count the overlap region. Ultimately,
one must calculate intersection volumes between pairs of cells, but
before that one performs spatial queries to determine the pairs that
may overlap. Typically one constructs axis-aligned bounding boxes for
both the background and overset cells. For each background bounding
box, one determines the overlapping overset bounding boxes. After a
spatial query has identified the relevant overset cells for a
particular background cell, then one calculates the intersection
volume for each pair. There are many ways to determine the overlapping
bounding boxes. One could store the overset cells in a bounding box
tree and perform a query for each background box.

For a third example of spatial queries in scientific simulations,
consider a finite volume method with an unstructured mesh. Certain
turbulence models require the distance to the wall (no-slip)
boundaries. Here, one must compute the distance to the wall
boundaries, which may be represented with a triangle mesh, from the
centroids of the volume mesh centroids. This problem may be solved by
storing the wall boundary in a bounding box tree and performing a
distance query for each centroid.

In each of the above problems we can denote *source* data and *target*
data. The target data is used to build a spatial query data structure
(often a tree). We perform queries for each of the source objects
(these may or may not be stored in a data structure that facilitates
the queries. For the overlap problem, the overset cells are the target data
and the background cells are the source data. For computing distance to
the wall boundary, the triangle mesh is the target data while the cell
centroids are the source objects. For the particle simulation, the
particle positions are both source objects and target objects.

## Distributed Data

Consider the problem of performing spatial queries with distributed
data. That is, both the source objects and the target objects are
distributed. In order to perform spatial queries for the source
objects on a given MPI process, we must gather the relevant target
objects to that process. Here *relevant* means that the target object
is required to correctly calculate the spatial query. In order to
*efficiently* perform the spatial queries, redistributing the source
objects in order to load-balance the cost of the queries may be
required. However, we will postpone addressing this problem. For the
moment we will assume that the distribution of the source objects is
fixed. We consider how to determine which of the distributed target
objects are relevant for each process.

Consider the overlap problem. The spatial query for a given (source)
background mesh cell returns the (target) overset cells whose bounding
boxes overlap the bounding box for the background cell. The simplest
approach for gathering the relevant target objects is to gather *all*
of them to each process. However, this is clearly not an efficient
approach for a large number of processes. Firstly, this results in a
large storage overhead. Also, performing queries is less efficient
when all of the distributed target objects are stored in the spatial
query data structures. At the other end of the spectrum, it would not
be efficient to try to gather *just* the relevant target objects to
each process. In order to do that, we would need to know the result of
all of the spatial queries, which have not yet been
computed. Reasonable approaches for gathering the relevant objects
must use approximate descriptions of the source and target geometries
to determine the relevant objects. Such approaches gather *at least*
all of the relevant objects.

The simplest non-trivial method for gathering the relevant target
objects starts with calculating two axis-aligned bounding boxes in
each process, one contains the source objects, the other contains the
target objects. Next we all-gather the source bounding boxes. We
calculate which of the gathered source bounding boxes overlap the
local target bounding box. If there is an overlap for the *n*th box,
we send all of the target objects to that process. As an alternative,
we could reduce the total number of target objects sent by first
checking if the target objects overlap the given source bounding box.

The primary issue with the above family of methods is that a single
bounding box is a very coarse description of the object geometries. In
particular, the bounding box is a poor description when the principal
axis of the distribution is not axis-aligned. They also suffer when
the local objects in a process are composed of multiple clumps. In
both of these cases, the bounding boxes are mostly empty.



