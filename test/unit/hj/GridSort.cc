// -*- C++ -*-

#include "stlib/hj/GridSort.h"

#include "stlib/hj/DistanceAdj1st.h"
#include "stlib/hj/DiffSchemeAdj.h"

#include <iostream>

#include <cassert>

using namespace stlib;

int
main()
{

  //
  // 2-D
  //
  {
    typedef container::MultiArray<double, 2> MultiArray;
    typedef MultiArray::SizeList SizeList;
    typedef MultiArray::IndexList IndexList;

    const SizeList extents = {{10, 10}};
    const IndexList bases = {{0, 0}};
    MultiArray solution(extents + 2, bases - 1);
    const double dx = 1;
    typedef hj::DistanceAdj1st<2, double> Equation;
    typedef hj::DiffSchemeAdj<2, double, Equation> DifferenceScheme;
    hj::GridSort<2, double, DifferenceScheme> x(solution, dx);
  }
  //
  // 3-D
  //
  {
    typedef container::MultiArray<double, 3> MultiArray;
    typedef MultiArray::SizeList SizeList;
    typedef MultiArray::IndexList IndexList;

    const SizeList extents = {{10, 10, 10}};
    const IndexList bases = {{0, 0, 0}};
    MultiArray solution(extents + 2, bases - 1);
    const double dx = 1;
    typedef hj::DistanceAdj1st<3, double> Equation;
    typedef hj::DiffSchemeAdj<3, double, Equation> DifferenceScheme;
    hj::GridSort<3, double, DifferenceScheme> x(solution, dx);
  }

  return 0;
}
