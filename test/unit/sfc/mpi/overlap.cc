// -*- C++ -*-

#include "stlib/sfc/overlapMpi.h"
#include "stlib/geom/kernel/Ball.h"

#include <random>

int
main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);

  MPI_Comm const comm = MPI_COMM_WORLD;
  int const commSize = stlib::mpi::commSize();
  int const commRank = stlib::mpi::commRank();

  using stlib::sfc::gatherOverlapping;
  using stlib::sfc::gatherOverlappingBBox;
  using stlib::sfc::gatherOverlappingBrute;

  {
    typedef float Float;
    std::size_t constexpr Dimension = 1;
    typedef std::array<Float, Dimension> Point;
    typedef stlib::geom::BBox<Float, Dimension> BBox;
    typedef stlib::geom::Ball<Float, Dimension> Object;

    // No query windows. No objects.
    {
      std::vector<BBox> queryWindows;
      std::vector<Object> objects;
      assert(gatherOverlapping(queryWindows, objects, comm).empty());
      assert(gatherOverlappingBBox(queryWindows, objects, comm).empty());
    }

    // No query windows.
    {
      std::vector<BBox> queryWindows;
      std::vector<Object> objects = {{{{0}}, 1}};
      assert(gatherOverlapping(queryWindows, objects, comm).empty());
      assert(gatherOverlappingBBox(queryWindows, objects, comm).empty());
    }

    // No objects.
    {
      std::vector<BBox> queryWindows = {{{{0}}, {{1}}}};
      std::vector<Object> objects;
      assert(gatherOverlapping(queryWindows, objects, comm).empty());
      assert(gatherOverlappingBBox(queryWindows, objects, comm).empty());
    }

    // Only local objects are relevant. One object per process.
    {
      Point const center = {{Float(commRank)}};
      std::vector<BBox> queryWindows =
        {{center - Float(0.5), center + Float(0.5)}};
      std::vector<Object> objects = {{center, Float(0.25)}};
      // The maximum objects per cell will be 1, However, because
      // there is only one object per process, in
      // buildAdaptiveBlocksFromDistributedObjects(), the highest
      // required level will be zero. Thus, the global description
      // of the object geometry will have only one cell.
      assert(gatherOverlapping(queryWindows, objects, comm).size() ==
             std::size_t(commSize));
      assert(gatherOverlappingBBox(queryWindows, objects, comm).size() == 1);
    }

    // Only local objects are relevant. Two objects per process.
    {
      Point const center = {{Float(commRank)}};
      std::vector<BBox> queryWindows =
        {{center - Float(0.5), center + Float(0.5)}};
      std::vector<Object> objects = {{center, Float(0.25)},
                                     {center, Float(0.25)}};
      assert(gatherOverlapping(queryWindows, objects, comm).size() == 2);
      assert(gatherOverlappingBBox(queryWindows, objects, comm).size() == 2);
    }

    // Neighbors are relevant as well.
    {
      Point const center = {{Float(commRank)}};
      std::vector<BBox> queryWindows =
        {{center - Float(0.5), center + Float(0.5)}};
      std::vector<Object> objects = {{center, Float(0.5)},
                                     {center, Float(0.5)}};
      std::vector<Object> relevantObjects =
        gatherOverlapping(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 4);
      }
      else {
        assert(relevantObjects.size() == 6);
      }
      relevantObjects =
        gatherOverlappingBBox(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 4);
      }
      else {
        assert(relevantObjects.size() == 6);
      }
    }

    // Objects at lower and upper corners of local domain.
    // Single query window.
    {
      Point const center = {{Float(commRank)}};
      std::vector<BBox> queryWindows =
        {{center - Float(0.5), center + Float(0.5)}};
      std::vector<Object> objects = {{center - Float(0.5), Float(0.5)},
                                     {center + Float(0.5), Float(0.5)}};
      std::vector<Object> relevantObjects =
        gatherOverlapping(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 3);
      }
      else {
        assert(relevantObjects.size() == 4);
      }
      relevantObjects =
        gatherOverlappingBBox(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 3);
      }
      else {
        assert(relevantObjects.size() == 4);
      }
    }

    // Objects at lower and upper corners of local domain.
    // Two query windows.
    {
      Point const center = {{Float(commRank)}};
      std::vector<BBox> queryWindows =
        {{center - Float(0.5), center}, {center, center + Float(0.5)}};
      std::vector<Object> objects = {{center - Float(0.5), Float(0.5)},
                                     {center + Float(0.5), Float(0.5)}};
      std::vector<Object> relevantObjects =
        gatherOverlapping(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 3);
      }
      else {
        assert(relevantObjects.size() == 4);
      }
      relevantObjects =
        gatherOverlappingBBox(queryWindows, objects, comm);
      if (commRank == 0 || commRank == commSize - 1) {
        assert(relevantObjects.size() == 3);
      }
      else {
        assert(relevantObjects.size() == 4);
      }
    }
  }
  
  {
    typedef float Float;
    std::size_t constexpr Dimension = 3;
    typedef stlib::geom::BBox<Float, Dimension> BBox;
    typedef stlib::geom::Ball<Float, Dimension> Object;

    std::mt19937_64 engine(commRank);
    std::uniform_real_distribution<Float> distribution(0, 1);

    // Random query windows and objects.
    {
      // Randow query windows with side lengths of 0.1.
      std::vector<BBox> queryWindows(10);
      for (auto& window: queryWindows) {
        for (std::size_t i = 0; i != Dimension; ++i) {
          window.lower[i] = distribution(engine);
        }
        window.upper = window.lower + Float(0.1);
      }
      // Random balls with radius 0.1.
      std::vector<Object> objects(100);
      for (auto& object: objects) {
        for (std::size_t i = 0; i != Dimension; ++i) {
          object.center[i] = distribution(engine);
        }
        object.radius = 0.1;
      }

      // Use the brute force method to get the exact set of relevant objects.
      std::vector<Object> exactRelevantObjects =
        gatherOverlappingBrute(queryWindows, objects, comm);
      // Verify that all relevant objects are gathered with the SFC method.
      std::vector<Object> relevantObjects =
        gatherOverlapping(queryWindows, objects, comm);
      for (auto const& object: exactRelevantObjects) {
        assert(std::count(relevantObjects.begin(), relevantObjects.end(),
                          object) == 1);
      }
      // Verify that all relevant objects are gathered with the BBox method.
      relevantObjects =
        gatherOverlappingBBox(queryWindows, objects, comm);
      for (auto const& object: exactRelevantObjects) {
        assert(std::count(relevantObjects.begin(), relevantObjects.end(),
                          object) == 1);
      }
    }
  }

  MPI_Finalize();
  return 0;
}
