# -*- Makefile -*-
RM = rm -rf


.PHONY: test unit performance

# By default, build the test code.
test: unit performance

unit:
	cd test/unit; scons

performance:
	cd test/performance; scons


# Package the source files for StarCCM+.
STAR_TARGET := ~/stlib.tar.gz
star: $(STAR_TARGET)

$(STAR_TARGET):
	find stlib -name \*~ | xargs $(RM)
	$(RM) /tmp/stlib
	mkdir /tmp/stlib
	cp -R stlib/ads stlib/container stlib/ext stlib/geom stlib/lorg stlib/mpi stlib/mst stlib/numerical stlib/performance stlib/sfc stlib/simd /tmp/stlib
	cd /tmp; tar czf stlib.tar.gz stlib
	$(RM) /tmp/stlib
	mv /tmp/stlib.tar.gz $(STAR_TARGET)


# Format the code with astyle.
SRC_DIRS := ${shell find stlib -type d -not -path "*svn*" -not -path "*third*" -not -path "*loki*" -print} ${shell find examples performance test -type d -not -path "*svn*" -print}
format:
	$(foreach DIR, $(SRC_DIRS), astyle --options=doc/astyle.txt $(DIR)/*.h $(DIR)/*.ipp $(DIR)/*.cc;)

# Prune directories that I do not distribute. 
# Use SWIG to make wrappers for the libraries.
distribution:
	$(RM) prototypes utilities
	$(RM) examples/mesh/optimization examples/mesh/experimental
# PSAAP
	$(RM) stlib/contact test/unit/contact examples/contact
# Smallest enclosing ball.
	$(RM) stlib/geom/kernel/SmallestEnclosingBall.h test/unit/geom/kernel/SmallestEnclosingBall.cc test/performance/geom/kernel/SmallestEnclosingBall.cc
#$(MAKE) -C lib wrappers
	$(RM) rsyncTriad.py

# Remove the documentation.
nodoc: distribution
	$(RM) doc doxygen


# Prepare STLib for distribution with the VTF.
vtf: distribution vtf-src vtf-examples vtf-test vtf-documentation
	$(RM) applications data lib performance prototypes \
results utilities 

# Prepare the src for distribution with the VTF.
vtf-src:
	$(RM) stlib/hj stlib/hj.h \
stlib/mst stlib/mst.h \
stlib/shortest_paths stlib/shortest_paths.h \
stlib/stochastic stlib/stochastic.h 
	$(RM) stlib/geom/orq stlib/geom/orq.h \
stlib/geom/orq3d stlib/geom/orq3d.h \
stlib/geom/spatialIndexing stlib/geom/spatialIndexing.h 
	$(RM) stlib/third-party/jama stlib/third-party/tnt

vtf-examples:
	$(RM) examples/geom examples/hj examples/mst examples/stochastic

vtf-test:
	$(RM) test/unit/C++ test/unit/compilers test/unit/geom test/unit/hj test/unit/mst test/unit/numerical test/unit/shortest_paths test/unit/stochastic test/unit/third-party

# Prepare the documentation for distribution with the VTF.
vtf-documentation:
	$(RM) doxygen/geom doxygen/hj doxygen/mst doxygen/numerical \
doxygen/shortest_paths doxygen/stochastic
