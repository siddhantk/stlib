// -*- C++ -*-

#include "stlib/sfc/DoOverlap.h"

int
main()
{
  typedef float Float;
  std::size_t constexpr Dimension = 1;
  typedef stlib::geom::BBox<Float, Dimension> BBox;
  typedef stlib::sfc::Traits<Dimension, Float> Traits;
  typedef stlib::sfc::DoOverlap<Traits> DoOverlap;
  typedef stlib::sfc::LinearOrthantTrie<Traits, BBox, false> LinearOrthantTrie;
  typedef stlib::sfc::LinearOrthantTrieTopology<Dimension> Topology;
  using stlib::geom::bbox;

  // doOverlap() for leaf BBox overlap.
  std::vector<std::size_t> stack;
  // Nothing overlaps an empty trie.
  {
    LinearOrthantTrie const trie(BBox{{{0}}, {{1}}}, 0);
    Topology const topology(trie);
    assert(! doOverlap(trie, topology, &stack, BBox{{{0}}, {{1}}}));
  }
  {
    DoOverlap doOverlap(std::vector<BBox>{});
    assert(! doOverlap(BBox{{{0}}, {{1}}}, &stack));
  }
  // One object.
  {
    LinearOrthantTrie trie(BBox{{{0}}, {{1}}}, 0);
    std::vector<BBox> objectBoxes = {BBox{{{0}}, {{1}}}};
    trie.buildCells(&objectBoxes);
    Topology const topology(trie);
    assert(doOverlap(trie, topology, &stack, BBox{{{0}}, {{1}}}));
    assert(doOverlap(trie, topology, &stack, BBox{{{1}}, {{2}}}));
    assert(! doOverlap(trie, topology, &stack, BBox{{{2}}, {{3}}}));
    assert(! doOverlap(trie, topology, &stack, bbox<BBox>()));
  }
  {
    DoOverlap doOverlap(std::vector<BBox>{BBox{{{0}}, {{1}}}});
    assert(doOverlap(BBox{{{0}}, {{1}}}, &stack));
    assert(doOverlap(BBox{{{1}}, {{2}}}, &stack));
    assert(! doOverlap(BBox{{{2}}, {{3}}}, &stack));
    assert(! doOverlap(bbox<BBox>(), &stack));
  }
  // Multiple objects.
  {
    std::size_t constexpr Size = 100;
    LinearOrthantTrie trie(BBox{{{0}}, {{Float(Size)}}}, 0);
    std::vector<BBox> objectBoxes(Size);
    for (std::size_t i = 0; i != objectBoxes.size(); ++i) {
      objectBoxes[i] = BBox{{{Float(i)}}, {{Float(i + 1)}}};
    }
    trie.buildCells(&objectBoxes);
    Topology const topology(trie);
    assert(doOverlap(trie, topology, &stack, BBox{{{-1}}, {{0}}}));
    assert(doOverlap(trie, topology, &stack, BBox{{{0}}, {{1}}}));
    assert(doOverlap(trie, topology, &stack, BBox{{{1}}, {{2}}}));
    assert(! doOverlap(trie, topology, &stack, BBox{{{-2}}, {{-1}}}));
    assert(! doOverlap(trie, topology, &stack, bbox<BBox>()));
  }
  {
    std::size_t constexpr Size = 100;
    std::vector<BBox> objectBoxes(Size);
    for (std::size_t i = 0; i != objectBoxes.size(); ++i) {
      objectBoxes[i] = BBox{{{Float(i)}}, {{Float(i + 1)}}};
    }
    DoOverlap doOverlap(std::move(objectBoxes));
    assert(doOverlap(BBox{{{-1}}, {{0}}}, &stack));
    assert(doOverlap(BBox{{{0}}, {{1}}}, &stack));
    assert(doOverlap(BBox{{{1}}, {{2}}}, &stack));
    assert(! doOverlap(BBox{{{-2}}, {{-1}}}, &stack));
    assert(! doOverlap(bbox<BBox>(), &stack));
  }

  // doOverlap() for object BBox overlap.
  {
    using stlib::sfc::doOverlap;
    {
       assert(doOverlap(std::vector<BBox>{}, std::vector<BBox>{}).empty());
    }
    {
      assert(doOverlap(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                       std::vector<BBox>{}).empty());
    }
    {
      assert(doOverlap(std::vector<BBox>{},
                       std::vector<BBox>{BBox{{{0}}, {{1}}}}) ==
             std::vector<bool>{false});
    }
    {
      assert(doOverlap(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                       std::vector<BBox>{BBox{{{0}}, {{1}}}}) ==
             std::vector<bool>{true});
    }
    {
      ;
      assert(doOverlap(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                       std::vector<BBox>{BBox{{{1}}, {{2}}}}) ==
             std::vector<bool>{true});
    }
    {
      assert(doOverlap(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                       std::vector<BBox>{BBox{{{2}}, {{3}}}}) ==
             std::vector<bool>{false});
    }
  }

  // doOverlapBrute()
  {
    using stlib::sfc::doOverlapBrute;
    assert(doOverlapBrute(std::vector<BBox>{}, std::vector<BBox>{}).empty());
    assert(doOverlapBrute(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                          std::vector<BBox>{}).empty());
    assert(doOverlapBrute(std::vector<BBox>{},
                          std::vector<BBox>{BBox{{{0}}, {{1}}}}) ==
           std::vector<bool>{false});
    assert(doOverlapBrute(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                          std::vector<BBox>{BBox{{{0}}, {{1}}}}) ==
           std::vector<bool>{true});
    assert(doOverlapBrute(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                          std::vector<BBox>{BBox{{{1}}, {{2}}}}) ==
           std::vector<bool>{true});
    assert(doOverlapBrute(std::vector<BBox>{BBox{{{0}}, {{1}}}},
                          std::vector<BBox>{BBox{{{2}}, {{3}}}}) ==
           std::vector<bool>{false});
  }

  return 0;
}
