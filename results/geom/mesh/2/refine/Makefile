# -*- Makefile -*-

# Refinement according to maximum edge length function.

DATA_PATH = ../../../../../data/geom/mesh
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = \
  square.eps square_refined.eps \
  square.vtu square_refined.vtu 

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.gnu *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	python.exe gp_file.py $(basename $<)
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

#
# Square
#

# The initial mesh.
square.txt:
	cp $(DATA_PATH)/22/square1.txt $@

# Refine
square_refined.txt: square.txt
	$(BIN_PATH)/refine22.exe -m square_mesh -f square_function $< $@

# End of file.