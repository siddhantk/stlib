// -*- C++ -*-

#include "stlib/hj/DiffSchemeAdjDiag.h"

#include "stlib/hj/DistanceAdj1st.h"

#include <iostream>

#include <cassert>

using namespace stlib;

int
main()
{

  //
  // 2-D
  //
  {
    typedef container::MultiArray<double, 2> MultiArray;
    typedef MultiArray::SizeList SizeList;
    typedef MultiArray::IndexList IndexList;
    typedef MultiArray::Range Range;

    const SizeList extents = {{10, 10}};
    const IndexList bases = {{0, 0}};
    const Range range(extents, bases);
    MultiArray solution(extents + 2, bases - 1);
    const double dx = 1;
    const bool is_concurrent = false;
    typedef hj::DistanceAdj1st<2, double> Equation;
    hj::DiffSchemeAdjDiag<2, double, Equation>
    x(range, solution, dx, is_concurrent);
  }
  //
  // 3-D
  //
  {
    typedef container::MultiArray<double, 3> MultiArray;
    typedef MultiArray::SizeList SizeList;
    typedef MultiArray::IndexList IndexList;
    typedef MultiArray::Range Range;

    const SizeList extents = {{10, 10, 10}};
    const IndexList bases = {{0, 0, 0}};
    const Range range(extents, bases);
    MultiArray solution(extents + 2, bases - 1);
    const double dx = 1;
    const bool is_concurrent = false;
    typedef hj::DistanceAdj1st<3, double> Equation;
    hj::DiffSchemeAdjDiag<3, double, Equation>
    x(range, solution, dx, is_concurrent);
  }

  return 0;
}
