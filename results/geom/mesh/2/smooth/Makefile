# -*- Makefile -*-

# Geometric optimization, interior vertices.

DATA_PATH = ../../../../../data/geom/mesh/22
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = initial.vtu mesh_0.vtu mesh_1.vtu mesh_2.vtu mesh_3.vtu mesh_4.vtu \
  initial.eps mesh_0.eps mesh_1.eps mesh_2.eps mesh_3.eps mesh_4.eps

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

# The initial mesh.
initial.txt:
	cp $(DATA_PATH)/triangle4.txt initial.txt

# Distort the initial mesh.
mesh_0.txt: initial.txt
	$(BIN_PATH)/randomize2.exe 0.0625 initial.txt mesh_0.txt

# Smooth 1 step.
mesh_1.txt: mesh_0.txt
	$(BIN_PATH)/smooth2.exe -n 1 -o mesh_1.txt mesh_0.txt

# Smooth 2 steps.
mesh_2.txt: mesh_0.txt
	$(BIN_PATH)/smooth2.exe -n 2 -o mesh_2.txt mesh_0.txt

# Smooth 3 steps.
mesh_3.txt: mesh_0.txt
	$(BIN_PATH)/smooth2.exe -n 3 -o mesh_3.txt mesh_0.txt

# Smooth 4 step.
mesh_4.txt: mesh_0.txt
	$(BIN_PATH)/smooth2.exe -n 4 -o mesh_4.txt mesh_0.txt

# End of file.