# -*- Makefile -*-

DATA_PATH = ../../../../../data/geom/mesh/21
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = boundary.vtu mesh.vtu mesh_r.vtu mesh_rfs.vtu \
  boundary.eps mesh.eps mesh_r.eps mesh_rfs.eps \

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

# The boundary mesh is the outline of the letter 'A'.
boundary.txt:
	cp $(DATA_PATH)/a.txt boundary.txt

# Convert the ascii mesh file to a VTK XML file.
boundary.vtu: boundary.txt
	$(BIN_PATH)/iss2vtk21.exe boundary.txt boundary.vtu

# We need an explicit rule for the 1-D manifold.
boundary.dat: boundary.txt
	python.exe $(PY_PATH)/txt2gp21.py $< $@

# Mesh the boundary using the tile algorithm.  The edge length is 0.04.
mesh.txt: boundary.txt
	$(BIN_PATH)/tile2.exe -s -m -l 0.04 boundary.txt mesh.txt

# Refine the boundary.
mesh_r.txt: mesh.txt
	$(BIN_PATH)/refine_boundary2.exe -a 0.1 -l 0.01 boundary.txt mesh.txt mesh_r.txt

# Refine the boundary with flipping and smoothing at each sweep.
mesh_rfs.txt: mesh.txt
	$(BIN_PATH)/refine_boundary2.exe -a 0.1 -l 0.01 -f -s boundary.txt mesh.txt mesh_rfs.txt

# End of file.