// -*- C++ -*-

#include "stlib/geom/kernel/Plane.h"

#include "stlib/geom/kernel/content.h"

#include <iostream>

#include <cassert>

using namespace stlib;
using namespace geom;

int
main()
{
  typedef Plane<double> Plane;
  typedef Plane::Point Point;

  {
    // Default constructor
    Plane p;
    std::cout << "Plane<double>() = " << p << '\n';
  }
  {
    // point and normal constructor
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    assert(p.isValid());
    std::cout << "Plane(Point{{2,3,5}},Point{{1,2,3}}) = "
              << p << '\n';
    assert((p.getPointOn() == Point{{2., 3., 5.}}));
    assert(p.getNormal() == nm);
  }
  {
    // Equality
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p1(Point{{2., 3., 5.}}, nm);
    Plane p2(Point{{2., 3., 5.}}, nm);
    assert(p1 == p2);
  }
  {
    // Inequality
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p1(Point{{2., 3., 5.}}, nm);
    Plane p2(Point{{2., 3., 6.}}, nm);
    assert(p1 != p2);
  }
  {
    // Inequality
    Point nm1 = {{1, 2, 3}};
    normalize(&nm1);
    Plane p1(Point{{2., 3., 5.}}, nm1);
    Point nm2 = {{1, 2, 4}};
    normalize(&nm2);
    Plane p2(Point{{2., 3., 5.}}, nm2);
    assert(p1 != p2);
  }
  {
    // three point constructor
    Plane p(Point{{1., 0., 1.}}, Point{{1., 1., 1.}},
            Point{{0., 1., 1.}});
    assert(p.isValid());
    assert(p == Plane(Point{{1., 1., 1.}}, Point{{0., 0., 1.}}));

    // copy constructor
    assert(p == Plane(p));

    // assignment operator
    Plane p2 = p;
    assert(p2 == p);

    // accessors
    assert((p.getPointOn() == Point{{1., 1., 1.}}) &&
           (p.getNormal() == Point{{0., 0., 1.}}));
  }
  {
    // +=
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    p += Point{{1., 1., 2.}};
    assert(p.isValid());
    assert((p.getPointOn() == Point{{2., 3., 5.}} + Point{{1., 1.,
            2.}}));
    assert(p.getNormal() == nm);
  }
  {
    // -=
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    p -= Point{{1., 1., 2.}};
    assert(p.isValid());
    assert((p.getPointOn() == Point{{2., 3., 5.}} - Point{{1., 1.,
            2.}}));
    assert(p.getNormal() == nm);
  }
  {
    // distance
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    Point pt = {{4, 5, 6}};
    double d = p.computeSignedDistance(pt);
    assert(std::abs(d - dot(pt - p.getPointOn(), nm)) < 1e-6);
  }
  {
    // closest point
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    assert(p.isValid());
    Point pt = {{4, 5, 6}}, cpt;
    double d = p.computeSignedDistanceAndClosestPoint(pt, &cpt);
    assert(std::abs(d - dot(pt - p.getPointOn(), nm)) < 1e-6);
    Point project = pt - d * nm;
    assert(euclideanDistance(cpt, project) < 1e-6);
  }
  {
    // unary +
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    assert(p.isValid());
    assert(p.getPointOn() == (+p).getPointOn());
    assert(p.getNormal() == (+p).getNormal());
  }
  {
    // unary -
    Point nm = {{1, 2, 3}};
    normalize(&nm);
    Plane p(Point{{2., 3., 5.}}, nm);
    Plane q(Point{{2., 3., 5.}}, -nm);
    assert(p.getPointOn() == (-q).getPointOn());
    assert(p.getNormal() == (-q).getNormal());
  }

  return 0;
}

