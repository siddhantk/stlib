# -*- Makefile -*-

# Geometric optimization with a boundary constraint.

DATA_PATH = ../../../../../data/geom/mesh
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = initial.vtu refined.vtu distorted.vtu optimized1.vtu optimized10.vtu
#  initial.eps 

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.gnu *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	python.exe gp_file.py $(basename $<)
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

# The initial mesh of a square.
initial.txt:
	cp $(DATA_PATH)/22/square1.txt $@

# The boundary of a square.
boundary.txt:
	cp $(DATA_PATH)/21/square.txt $@

# Refine the initial mesh so no edge is longer than 0.1.
refined.txt: initial.txt
	$(BIN_PATH)/refine22.exe -length=0.1 $< $@

# Distort the refined mesh.
distorted.txt: refined.txt
	python.exe $(PY_PATH)/vertices_map.py distortion $< $@ 

# Apply geometric optimization.
optimized1.txt: distorted.txt boundary.txt
	$(BIN_PATH)/geomOptBoundaryCondition2.exe -sweeps=1 -angle=0.1 boundary.txt $< $@

optimized10.txt: distorted.txt boundary.txt
	$(BIN_PATH)/geomOptBoundaryCondition2.exe -sweeps=10 -angle=0.1 boundary.txt $< $@


# The boundary of the unit square.
#boundary.txt:
#	cp $(DATA_PATH)/21/square.txt boundary.txt

# We need an explicit rule for the 1-D manifold.
#boundary.vtu: boundary.txt
#	$(BIN_PATH)/iss2vtk21.exe $< $@

# We need an explicit rule for the 1-D manifold.
#boundary.dat: boundary.txt
#	python.exe $(PY_PATH)/txt2gp21.py $< $@

# Move the boundary vertices to the boundary surface to obtain the mesh.
mesh.txt: boundary.txt refined.txt
	$(BIN_PATH)/move_boundary2.exe boundary.txt refined.txt mesh.txt

# Geometric optimization, unconstrained optimization in initially
# moving the boundary vertices, one step.
mesh_1.txt: mesh.txt
	$(BIN_PATH)/smooth_bc2.exe boundary.txt $< $@

# Geometric optimization, unconstrained optimization in initially
# moving the boundary vertices, 2 steps.
mesh_2.txt: mesh.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt $< $@

# Geometric optimization, unconstrained optimization in initially
# moving the boundary vertices, 10 steps.
mesh_10.txt: mesh.txt
	$(BIN_PATH)/smooth_bc2.exe -n 10 boundary.txt $< $@

mesh_gfg.txt: mesh.txt
	$(BIN_PATH)/flip2.exe mesh_10.txt mesh_gf.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt mesh_gf.txt $@

mesh_gfgc.txt: mesh_gfg.txt
	$(BIN_PATH)/refine2.exe -l 0.1 mesh_gfg.txt mesh_gfgc.txt
	$(BIN_PATH)/move_boundary2.exe boundary.txt mesh_gfgc.txt mesh_gfgc.txt
	$(BIN_PATH)/flip2.exe mesh_gfgc.txt mesh_gfgc.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt mesh_gfgc.txt $@

mesh_gfgcc.txt: mesh_gfgc.txt
	$(BIN_PATH)/refine2.exe -l 0.1 mesh_gfgc.txt mesh_gfgcc.txt
	$(BIN_PATH)/move_boundary2.exe boundary.txt mesh_gfgcc.txt mesh_gfgcc.txt
	$(BIN_PATH)/flip2.exe mesh_gfgcc.txt mesh_gfgcc.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt mesh_gfgcc.txt $@

# $Log$
# Revision 1.1  2006/01/06 22:34:48  sean
# Original.
#
# Revision 1.1.1.1  2005/09/15 20:00:31  sean
# Original
#
# Revision 1.4  2005/06/11 01:00:10  parasim
# Updated the order of command line options.
#
# Revision 1.3  2005/04/02 01:17:56  parasim
# Updated for new graphics.
#
# Revision 1.2  2005/03/25 02:02:45  parasim
# Changed the example.
#
# Revision 1.1  2005/03/24 00:09:46  parasim
# Original.
#

# End of file.