// -*- C++ -*-

#if !defined(__sfc_overlapMpi_h__)
#define __sfc_overlapMpi_h__

/*!
  \file
  \brief Gather the potentially overlapping objects to each process.
*/

#include "stlib/sfc/gatherRelevant.h"
#include "stlib/sfc/LinearOrthantTrieTopology.h"

#include <memory>

namespace stlib
{
namespace sfc
{


/// Gather the objects that overlap the query windows.
/**
   \param queryWindows We pass a pointer to the vector of query windows 
   so that it may be used as scratch data. Specifically, the elements of
   the vector will be reordered.
   \param objects The vector of local objects.
   \param comm The MPI communicator.
   \param queryLeafSize The maximum number of query windows in a cell for
   the data structure that organizes them.
   \return The vector of objects that potentially overlap the local query
   windows.

   Gather the objects that may intersect the query windows. First we 
   build a coarse description of the query windows. We do this because
   performing a query for every window could be expensive. Note that 
   you can control the accuracy of the description with the queryLeafSize
   parameter. This gives us a sequence of cell query windows.
   Next we build a coarse description of the geometry of the
   distributed objects. From this we build a linear orthant trie that
   we will use of queries. We do an overlap test for each of the cell
   query windows, recording the overlapping cells for the distributed objects.
   Now that we know the relevant object cells, we gather the relevant
   objects to each process.
*/
template<typename _Float, std::size_t _Dimension, typename _Object>
std::vector<_Object>
gatherOverlapping(std::vector<geom::BBox<_Float, _Dimension> >* queryWindows,
                  std::vector<_Object>* objects,
                  MPI_Comm comm,
                  std::size_t maxQueryWindowsPerCell = 32);


/// Gather the objects that overlap the query windows.
/**
   \param queryWindows The vector of query windows.
   \param objects The vector of local objects.
   \param comm The MPI communicator.
   \param queryLeafSize The maximum number of query windows in a cell for
   the data structure that organizes them.
   \return The vector of objects that potentially overlap the local query
   windows.

   Gather the objects that may intersect the query windows. First we 
   build a coarse description of the query windows. We do this because
   performing a query for every window could be expensive. Note that 
   you can control the accuracy of the description with the queryLeafSize
   parameter. This gives us a sequence of cell query windows.
   Next we build a coarse description of the geometry of the
   distributed objects. From this we build a linear orthant trie that
   we will use of queries. We do an overlap test for each of the cell
   query windows, recording the overlapping cells for the distributed objects.
   Now that we know the relevant object cells, we gather the relevant
   objects to each process.
*/
template<typename _Float, std::size_t _Dimension, typename _Object>
inline
std::vector<_Object>
gatherOverlapping
(std::vector<geom::BBox<_Float, _Dimension> > const& queryWindows,
 std::vector<_Object> const& objects,
 MPI_Comm const comm,
 std::size_t const maxQueryWindowsPerCell = 32)
{
  // Copy the query windows and objects. Then pass them as scratch data.
  std::vector<geom::BBox<_Float, _Dimension> > queryWindowsCopy = queryWindows;
  std::vector<_Object> objectsCopy = objects;
  return gatherOverlapping(&queryWindowsCopy, &objectsCopy, comm,
                           maxQueryWindowsPerCell);
}


/// Gather the objects that overlap the query windows.
/**
   \param queryWindows The vector of query windows.
   \param objects The vector of local objects.
   \param comm The MPI communicator.
   \return The vector of objects that potentially overlap the local query
   windows.

   Gather the objects that may intersect the query windows. For each
   process, consider the tight bounding box for the query
   windows. Each process will gather the distributed objects whose
   bounding boxes overlap this query window bounding box.
*/
template<typename _Float, std::size_t _Dimension, typename _Object>
std::vector<_Object>
gatherOverlappingBBox(
  std::vector<geom::BBox<_Float, _Dimension> > const& queryWindows,
  std::vector<_Object> const& objects,
  MPI_Comm comm);


} // namespace sfc
} // namespace stlib

#define __sfc_overlapMpi_tcc__
#include "stlib/sfc/overlapMpi.tcc"
#undef __sfc_overlapMpi_tcc__

#endif
