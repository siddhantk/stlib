// -*- C++ -*-

#include "stlib/geom/kernel/Segment.h"

#include <iostream>

#include <cassert>

using namespace stlib;

int
main()
{
  typedef geom::Segment<3> Segment;
  typedef Segment::Point Point;
  {
    // Default constructor
    Segment s;
    std::cout << "Segment() = " << s << "\n";
  }
  {
    // Point constructor
    Point p = {{1, 2, 3}}, q = {{2, 3, 5}};
    Segment s(p, q);
    std::cout << "Segment((1,2,3),(2,3,5)) = " << s << "\n";

    // copy constructor
    Segment t(s);
    std::cout << "copy = " << t << "\n";
    // assignment operator
    Segment u = s;
    std::cout << "assignment = " << u << "\n";

    // accessors
    std::cout << "Accessors: " << "\n"
              << "source = " << s.getSource() << "\n"
              << "target = " << s.getTarget() << "\n";
  }
  {
    // += operator
    Point p = {{1, 2, 3}}, q = {{2, 3, 5}};
    Segment s(p, q);
    s += p;
    Segment sp(Point{{2., 4., 6.}}, Point{{3., 5., 8.}});
    assert(s == sp);
  }
  {
    // -= operator
    Point p = {{1, 2, 3}}, q = {{2, 3, 5}};
    Segment s(p, q);
    s -= p;
    Segment sp(Point{{0., 0., 0.}}, Point{{1., 1., 2.}});
    assert(s == sp);
  }
  // == operator
  {
    Segment a(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    Segment b(Point{{2., 3., 5.}}, Point{{7., 11., 13.}});
    assert(!(a == b));
  }
  {
    Segment a(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    Segment b(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    assert(a == b);
  }
  // != operator
  {
    Segment a(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    Segment b(Point{{2., 3., 5.}}, Point{{7., 11., 13.}});
    assert(a != b);
  }
  {
    Segment a(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    Segment b(Point{{1., 2., 3.}}, Point{{4., 5., 6.}});
    assert(!(a != b));
  }
  {
    // unary + operator
    Segment s(Point{{1., 2., 3.}}, Point{{2., 3., 5.}});
    assert(+s == s);
  }
  {
    // unary - operator
    Segment s(Point{{1., 2., 3.}}, Point{{2., 3., 5.}});
    Segment ms(Point{{2., 3., 5.}}, Point{{1., 2., 3.}});
    assert(ms == -s);
  }

  return 0;
}
