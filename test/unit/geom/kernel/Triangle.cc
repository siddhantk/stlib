// -*- C++ -*-

#include "stlib/geom/kernel/Triangle.h"

#include <iostream>

#include <cassert>

using namespace stlib;

int
main()
{
  typedef geom::Triangle<3> Triangle;
  typedef Triangle::Point Point;

  {
    // default constructor
    std::cout << "Triangle<3>() = " << Triangle()
              << '\n';
  }
  {
    // Point constructor
    Triangle t(Point{{0., 0., 0.}},
               Point{{1., 0., 0.}},
               Point{{1., 1., 1.}});
    std::cout << "Triangle((0,0,0),(1,0,0),(1,1,1)) = " << t
              << '\n';
    Triangle tc(t);
    std::cout << "copy = " << tc << '\n';
    Triangle ta = t;
    std::cout << "assignment = " << ta << '\n';

    // accessors
    assert((t.getVertex(0) == Point{{0., 0., 0.}}));
    assert((t.getVertex(1) == Point{{1., 0., 0.}}));
    assert((t.getVertex(2) == Point{{1., 1., 1.}}));
  }
  {
    // +=
    Triangle t(Point{{0., 0., 0.}}, Point{{1., 0., 0.}},
               Point{{1., 1., 1.}});
    t += Point{{1., 2., 3.}};
    assert((t.getVertex(0) == Point{{1., 2., 3.}}));
    assert((t.getVertex(1) == Point{{2., 2., 3.}}));
    assert((t.getVertex(2) == Point{{2., 3., 4.}}));
  }
  {
    // -=
    Triangle t(Point{{0., 0., 0.}}, Point{{1., 0., 0.}},
               Point{{1., 1., 1.}});
    t -= Point{{1., 2., 3.}};
    assert((t.getVertex(0) == Point{{-1., -2., -3.}}));
    assert((t.getVertex(1) == Point{{0., -2., -3.}}));
    assert((t.getVertex(2) == Point{{0., -1., -2.}}));
  }
  {
    // supporting_plane
    Triangle t(Point{{0., 0., 0.}}, Point{{1., 0., 0.}},
               Point{{1., 1., 1.}});
    std::cout << "supporting plane = " << buildSupportingPlane(t) << '\n';
  }

  return 0;
}
