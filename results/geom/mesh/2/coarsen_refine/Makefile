# -*- Makefile -*-

# Coarsening and refining with incidence optimization and Laplacian smoothing.

DATA_PATH = ../../../../../data/geom/mesh
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = square4.eps sin.eps sin_l.eps sin_il.eps \
  sin_t.eps sin_tg.eps sin_tgt.eps sin_tgtg.eps \
  sin-c.eps sin-f1.eps sin-f.eps \
  square4.vtu sin.vtu sin_l.vtu sin_il.vtu \
  sin_t.vtu sin_tg.vtu sin_tgt.vtu sin_tgtg.vtu \
  sin-c.vtu sin-f1.vtu sin-f.vtu \
  maze_boundary.vtu maze.vtu \
  maze-1.vtu maze-2.vtu maze-3.vtu maze-4.vtu maze-5.vtu maze-10.vtu maze-15.vtu \
  maze-coarse.vtu maze-fine.vtu \
  maze_boundary.eps maze.eps \
  maze-1.eps maze-2.eps maze-3.eps maze-4.eps maze-5.eps maze-10.eps maze-15.eps \
  maze-coarse.eps maze-fine.eps \
  cutout_boundary.vtu cutout_initial.vtu cutout_coarse.vtu cutout_medium.vtu cutout_fine.vtu \
  cutout_boundary.eps cutout_initial.eps cutout_coarse.eps cutout_medium.eps cutout_fine.eps


GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.gnu *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	python.exe gp_file.py $(basename $<)
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

#
# The cutout shape.
#

# The boundary of the mesh.
cutout_boundary.txt:
	cp $(DATA_PATH)/21/square_cutouts.txt cutout_boundary.txt

# We need an explicit rule for the 1-D manifold.
cutout_boundary.vtu: cutout_boundary.txt
	$(BIN_PATH)/iss2vtk21.exe $< $@

# We need an explicit rule for the 1-D manifold.
cutout_boundary.dat: cutout_boundary.txt
	python.exe $(PY_PATH)/txt2gp21.py $< $@

# Use center point meshing to generate a mesh with poor quality.
cutout_initial.txt: cutout_boundary.txt
	$(BIN_PATH)/center_point_mesh2.exe $< $@

# Make a coarse mesh.
cutout_coarse.txt: cutout_initial.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.1 -u 0.2 -a 2.36 -n 40 $< $@

# Make a medium mesh.
cutout_medium.txt: cutout_initial.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 40 $< $@

# Make a fine mesh.
cutout_fine.txt: cutout_initial.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.025 -u 0.05 -a 2.36 -n 40 $< $@


#
# The sin.
#

square4.txt:
	cp $(DATA_PATH)/22/square4.txt .

sin.txt:
	python.exe $(PY_PATH)/vertices_map.py sin_function square4.txt $@ 

sin-c.txt: sin.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.1 -u 0.25 -a 2.36 -n 2 $< $@

sin-f1.txt: sin.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.125 -a 2.36 -n 1 $< $@

sin-f.txt: sin.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.125 -a 2.36 -n 5 $< $@


sin_l.txt: sin.txt
	$(BIN_PATH)/laplacianSolve2.exe $< $@

sin_il.txt: sin.txt
	$(BIN_PATH)/inc_opt22.exe $< $@
	$(BIN_PATH)/laplacianSolve2.exe $@ $@

sin_t.txt: sin.txt
	$(BIN_PATH)/flip22.exe $< $@

sin_tg.txt: sin_t.txt
	$(BIN_PATH)/boundary22.exe sin.txt boundary.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt $< $@

sin_tgt.txt: sin_tg.txt
	$(BIN_PATH)/flip22.exe $< $@

sin_tgtg.txt: sin_tgt.txt
	$(BIN_PATH)/smooth_bc2.exe -n 2 boundary.txt $< $@


#
# The Maze.
#

# The boundary of the maze.
maze_boundary.txt:
	cp $(DATA_PATH)/21/maze.txt maze_boundary.txt

# We need an explicit rule for the 1-D manifold.
maze_boundary.vtu: maze_boundary.txt
	$(BIN_PATH)/iss2vtk21.exe $< $@

# We need an explicit rule for the 1-D manifold.
maze_boundary.dat: maze_boundary.txt
	python.exe $(PY_PATH)/txt2gp21.py $< $@

# Use center point meshing to generate a mesh with poor quality.
maze.txt: maze_boundary.txt
	$(BIN_PATH)/center_point_mesh2.exe maze_boundary.txt maze.txt

# 1 sweep
maze-1.txt: maze.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 1 $< $@

# 2 sweeps
maze-2.txt: maze-1.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 1 $< $@

# 3 sweeps
maze-3.txt: maze-2.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 1 $< $@

# 4 sweeps
maze-4.txt: maze-3.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 1 $< $@

# 5 sweeps
maze-5.txt: maze-4.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 1 $< $@

# 8 sweeps
maze-8.txt: maze-5.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 3 $< $@

# 10 sweeps
maze-10.txt: maze-8.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 2 $< $@

# 15 sweeps
maze-15.txt: maze-10.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.05 -u 0.1 -a 2.36 -n 5 $< $@

# Coarse mesh of the maze.
maze-coarse.txt: maze.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.1 -u 0.25 -a 2.36 -n 6 $< $@

# Fine mesh of the maze.
maze-fine.txt: maze-coarse.txt
	$(BIN_PATH)/coarsen_refine22.exe -l 0.02 -u 0.05 -a 2.36 -n 10 $< $@
