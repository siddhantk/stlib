# Overlap Queries for Bounding Boxes

# Introduction

Consider a set of objects, *A*. For an object *b*, we may want to determine 
which of the objects in *A* overlap it. Since overlap tests for
bounding boxes are relatively cheap, the way to solve this problem
efficiently is to first determine which of the objects' bounding boxes
overlap the bounding box for *b*. Given the set of potential
overlapping objects, one may then perform overlap tests with the
objects themselves.

# Algorithm

To efficiently perform the overlap queries, we store the object bounding boxes
in a linear orthant trie. The cell type is a bounding box. Thus, we
can find the object boxes that overlap the query box by performing a depth-first
search of the trie. We initialize the stack with the root cell. 
When we process a cell, we first check its bounding 
box. We proceed only if it overlaps the query box. If the cell is internal,
we then add each of the children to the stack. For leaf cells, we perform
an overlap test with each of the object boxes.

The above serial algorithm is simple to implement. There are two opportunities
for using intra-register vectorization to optimize it. Firstly, for each 
leaf cell, we can store the object bounding boxes in a 
structure-of-arrays format that permits efficient overlap tests with
SIMD instructions. Obviously, the data for each leaf cell must be 
appropriately aligned. Consider using AVX with single-precision 
floating-point numbers, for example. In that case, one can perform overlap tests
with eight objects bounding boxes at a time.

The second opportunity for vectorization is in the calculations performed
on internal cells. In the serial algorithm, if the cell bounding box
overlaps the query box we add all of the children to the stack. Instead
of adding them to the stack to process later, we can perform overlap tests
for all the children of a cell. Then we only add children whose boxes overlap
the query box. For each internal cell, we store the child bounding boxes
in aligned, shuffled storage that permits efficient SIMD operations.

# Usage

There are a number of functions for performing 
\ref sfcOverlapQuery "overlap queries".
The simplest interface is the function
stlib::sfc::overlapQuery(std::vector<geom::BBox<_T, _D> >&& objectBoxes, std::vector<geom::BBox<_T, _D> > const& queryBoxes). For each query box, it 
records the indices of the overlapping object boxes.
It returns the result in a packed array of arrays.
Below is an example in which the objects are balls.

\code
constexpr std::size_t Dimension = 3;
typedef float Float;
typedef stlib::geom::Ball<Float, Dimension> Object;
typedef stlib::geom::BBox<Float, Dimension> BBox;
typedef stlib::container::PackedArrayOfArrays<std::size_t> Packed;

std::vector<Object> objects;
std::vector<BBox> queryBoxes;
// Initialize the objects and query boxes.
...

Packed const overlapping =
  stlib::sfc::overlapQuery(stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end()), queryBoxes);
\endcode

There are also interfaces that may be useful when you have
already built a linear orthant trie for the objects. You might use these
interfaces if you are performing multiple kinds of queries with the objects.

To perform overlap tests one at a time, use the stlib::sfc::OverlapQuery
functor. Below we show how to use this alternative.

\code
typedef stlib::sfc::Traits<Dimension, Float> Traits;

// Construct the functor.
stlib::sfc::OverlapQuery<Traits> overlapQuery(stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end()));
// We need a vector of std::size_t for scratch data.
std::vector<std::size_t> stack;
// For each query box, get the overlapping object boxes.
std::vector<std::size_t> overlapping;
auto output = std::back_inserter(overlapping);
for (BBox const& queryBox: queryBoxes) {
  overlapping.clear();
  overlapQuery(queryBox, output, &stack);
  for (std::size_t objectIndex: overlapping) {
    ...
  }
}
\endcode

When you specify the object bounding boxes, they must be passed as an 
rvalue reference. This is because the caller should think about reusing
the storage for them. Most often, the object boxes will be built when
one wants to perform the overlap query and not used afterwards. It would
be wasteful to copy the vector of object bounding boxes in this case.
It is better to pass an rvalue and use them as a pure input. If one needs
the vector of object bounding boxes after performing the overlap queries,
then one must explicitly copy it. Below we construct the vector of object
bounding boxes before calling \c stlib::sfc::overlapQuery(). Thus we must
use \c std::move() to obtain an rvalue reference for them.

\code
std::vector<BBox> objectBoxes = stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end());
Packed const overlapping = stlib::sfc::overlapQuery(std::move(objectBoxes), queryBoxes);
\endcode

# Performance

To measure the performance of the overlap tests we use the ball intersection
example in \c test/performance/sfc/mpi/ballIntersection.cc. We run the 
test on an Intel(R) Xeon(R) CPU E5-2687W v2 @ 3.40GHz, using 16 MPI processes.
We run all of the tests with the rotated geometry (\c -t option). For
this test, processes have between 1.19 million and 1.26 million object
boxes. The number of query boxes is 1.05 million. The total time for 
performing the overlap queries is 3.90 seconds. Note that the query boxes
are just bounding boxes for the balls. The balls are generated by randomly
distributing them in a unit cube. Thus, there is no correlation between
the query box index and its (centroid) position. If there were a correlation,
the performance of the overlap tests would be improved due to better 
cache utilization. Specifically, there would be fewer cache misses when 
accessing the bounding boxes in the linear orthant trie. If we sort the
query boxes by their z coordinate, then the cost of performing the overlap
tests is reduced to 2.49 seconds. Since sorting the query boxes takes 
0.09 seconds, the total cost for the queries is 2.58 seconds. If we 
sort the query boxes using a space-filling curve, then the overlap tests
take 2.06 seconds. Since sorting with the SFC takes 0.16 seconds, the 
total time for the overlap tests is 2.22 seconds.

From this example we see that locallity of reference plays a fairly 
important role in the performance of the overlap tests. Going from queries 
that have no correlation between order and location to queries that are highly 
correlated reduced the overlap query time by a factor of two. However, 
one must also consider the cost the queries. If the queries have some 
correlation between order and location, then it is probably best to use
the given order. If the queries have no such correlation, then it is 
probably best to order them with an SFC before conducting the overlap 
tests. Doing so will modestly improve the performance.
