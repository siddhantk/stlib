// -*- C++ -*-

#include "stlib/ext/array.h"

#include <sstream>

using namespace stlib;

int
main()
{
  //---------------------------------------------------------------------------
  // Size.
  {
    std::cout << "Size of <int,1> = " << sizeof(std::array<int, 1>)
              << '\n'
              << "Size of <int,2> = " << sizeof(std::array<int, 2>)
              << '\n';
  }
  //---------------------------------------------------------------------------
  // Aggregate initializer.
  {
    std::array<int, 3> a = {{}};
    assert(a[0] == 0 && a[1] == 0 && a[2] == 0);
  }
  {
    std::array<int, 3> a = {{0}};
    assert(a[0] == 0 && a[1] == 0 && a[2] == 0);
  }
  {
    std::array<int, 3> a = {{2}};
    assert(a[0] == 2 && a[1] == 0 && a[2] == 0);
  }
  {
    int a[3] = {2};
    assert(a[0] == 2 && a[1] == 0 && a[2] == 0);
  }
  //---------------------------------------------------------------------------
  // Array Assignment Operators with a Scalar Operand.
  {
    std::array<int, 3> a = {{2, 3, 5}};
    // +=
    {
      std::array<int, 3> b = a;
      b += 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] + 1);
      }
    }
    // -=
    {
      std::array<int, 3> b = a;
      b -= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] - 1);
      }
    }
    // *=
    {
      std::array<int, 3> b = a;
      b *= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] * 2);
      }
    }
    // /=
    {
      std::array<int, 3> b = a;
      b /= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] / 2);
      }
    }
    // %=
    {
      std::array<int, 3> b = a;
      b %= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] % 2);
      }
    }
    // <<=
    {
      std::array<int, 3> b = a;
      b <<= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] << 1);
      }
    }
    // >>=
    {
      std::array<int, 3> b = a;
      b >>= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] >> 1);
      }
    }
  }

  //---------------------------------------------------------------------------
  // Array Assignment Operators with a Array Operand.
  {
    std::array<int, 3> a = {{2, 3, 5}};
    std::array<int, 3> b = {{1, 2, 3}};
    // +=
    {
      std::array<int, 3> c = a;
      c += b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] + b[n]);
      }
    }
    // -=
    {
      std::array<int, 3> c = a;
      c -= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] - b[n]);
      }
    }
    // *=
    {
      std::array<int, 3> c = a;
      c *= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] * b[n]);
      }
    }
    // /=
    {
      std::array<int, 3> c = a;
      c /= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] / b[n]);
      }
    }
    // %=
    {
      std::array<int, 3> c = a;
      c %= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] % b[n]);
      }
    }
    // <<=
    {
      std::array<int, 3> c = a;
      c <<= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] << b[n]);
      }
    }
    // >>=
    {
      std::array<int, 3> c = a;
      c >>= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] >> b[n]);
      }
    }
  }

  //---------------------------------------------------------------------------
  // Unary Operators
  {
    std::array<double, 3> a = {{1, 2, 3}};
    assert(a == +a);
    assert(a == -(-a));
    std::array<double, 3> b;
    b = -a;
    for (std::size_t i = 0; i != b.size(); ++i) {
      assert(b[i] == -a[i]);
    }
  }
  {
    std::array<std::array<double, 3>, 3> a = {{{{1, 2, 3}},
        {{4, 5, 6}},
        {{7, 8, 9}}
      }
    };
    assert(a == +a);
    assert(a == -(-a));
    std::array<std::array<double, 3>, 3> b;
    b = -a;
    for (std::size_t i = 0; i != b.size(); ++i) {
      assert(b[i] == -a[i]);
    }
  }

  //---------------------------------------------------------------------------
  // Binary Operators
  {
    // Array-scalar.
    {
      // int, unsigned
      const std::array<int, 1> a = {{1}};
      const unsigned b = 2;
      std::array<unsigned, 1> c;
      c = a + b;
      assert(c[0] == a[0] + b);
      c = a - b;
      assert(c[0] == a[0] - b);
      c = a * b;
      assert(c[0] == a[0] * b);
      c = a / b;
      assert(c[0] == a[0] / b);
      c = a % b;
      assert(c[0] == a[0] % b);
    }
    {
      // int, unsigned
      const std::array<int, 1> a = {{1}};
      const unsigned b = 2;
      std::array<int, 1> c;
      c = ext::convert_array<int>(a + b);
      assert(c[0] == int(a[0] + b));
      c = ext::convert_array<int>(a - b);
      assert(c[0] == int(a[0] - b));
      c = ext::convert_array<int>(a * b);
      assert(c[0] == int(a[0] * b));
      c = ext::convert_array<int>(a / b);
      assert(c[0] == int(a[0] / b));
      c = ext::convert_array<int>(a % b);
      assert(c[0] == int(a[0] % b));
    }
    {
      // int, double
      const std::array<int, 1> a = {{1}};
      const double b = 2;
      std::array<double, 1> c;
      c = a + b;
      assert(c[0] == a[0] + b);
      c = a - b;
      assert(c[0] == a[0] - b);
      c = a * b;
      assert(c[0] == a[0] * b);
      c = a / b;
      assert(c[0] == a[0] / b);
    }
    {
      // double, int
      const std::array<double, 1> a = {{1}};
      const int b = 2;
      std::array<double, 1> c;
      c = a + b;
      assert(c[0] == a[0] + b);
      c = a - b;
      assert(c[0] == a[0] - b);
      c = a * b;
      assert(c[0] == a[0] * b);
      c = a / b;
      assert(c[0] == a[0] / b);
    }
    {
      // double, bool
      const std::array<double, 1> a = {{2}};
      const bool b = true;
      std::array<double, 1> c;
      c = a + b;
      assert(c[0] == a[0] + b);
      c = a - b;
      assert(c[0] == a[0] - b);
      c = a * b;
      assert(c[0] == a[0] * b);
      c = a / b;
      assert(c[0] == a[0] / b);
    }
    // Scalar-array.
    {
      // int, unsigned
      const int a = 2;
      const std::array<unsigned, 1> b = {{1}};
      std::array<unsigned, 1> c;
      c = a + b;
      assert(c[0] == a + b[0]);
      c = a - b;
      assert(c[0] == a - b[0]);
      c = a * b;
      assert(c[0] == a * b[0]);
      c = a / b;
      assert(c[0] == a / b[0]);
      c = a % b;
      assert(c[0] == a % b[0]);
    }
    // Array-array.
    {
      // int, unsigned
      const std::array<int, 1> a = {{2}};
      const std::array<unsigned, 1> b = {{1}};
      std::array<unsigned, 1> c;
      c = a + b;
      assert(c[0] == a[0] + b[0]);
      c = a - b;
      assert(c[0] == a[0] - b[0]);
      c = a * b;
      assert(c[0] == a[0] * b[0]);
      c = a / b;
      assert(c[0] == a[0] / b[0]);
      c = a % b;
      assert(c[0] == a[0] % b[0]);
    }

#if 0
    char c;
    unsigned char h;
    signed char a;
    int i;
    unsigned j;
    long l;
    unsigned long m;
    std::cout
        << "char = " << typeid(char).name() << '\n'
        << "signed char = " << typeid(signed char).name() << '\n'
        << "unsigned char = " << typeid(unsigned char).name() << '\n'
        << "short = " << typeid(short).name() << '\n'
        << "unsigned short = " << typeid(unsigned short).name() << '\n'
        << "int = " << typeid(int).name() << '\n'
        << "unsigned int = " << typeid(unsigned int).name() << '\n'
        << "long = " << typeid(long).name() << '\n'
        << "unsigned long = " << typeid(unsigned long).name() << '\n'
        << "float = " << typeid(float).name() << '\n'
        << "double = " << typeid(double).name() << '\n' << '\n'

        << typeid(c).name() << "+" << typeid(h).name() << "="
        << typeid(c + h).name() << '\n'
        << typeid(c).name() << "+" << typeid(a).name() << "="
        << typeid(c + a).name() << '\n'
        << typeid(h).name() << "+" << typeid(a).name() << "="
        << typeid(h + a).name() << '\n'

        << typeid(i).name() << "+" << typeid(i).name() << "="
        << typeid(i + i).name() << '\n'
        << typeid(i).name() << "+" << typeid(j).name() << "="
        << typeid(i + j).name() << '\n'
        << typeid(i).name() << "+" << typeid(l).name() << "="
        << typeid(i + l).name() << '\n'
        << typeid(i).name() << "+" << typeid(m).name() << "="
        << typeid(i + m).name() << '\n';
#endif
  }

  //---------------------------------------------------------------------------
  // File I/O.
  {
    std::array<int, 3> a = {{2, 3, 5}};
    std::ostringstream out;
    out << a;
    std::array<int, 3> b;
    std::istringstream in(out.str());
    in >> b;
    assert(a == b);
  }
  {
    std::array<int, 3> a = {{2, 3, 5}};
    std::ostringstream out;
    write(a, out);
    std::array<int, 3> b;
    std::istringstream in(out.str());
    read(&b, in);
    assert(a == b);
  }

  //---------------------------------------------------------------------------
  // Make an array.
  {
    // ConvertArray
    {
      std::array<char, 1> x = {{2}};
      std::array<int, 1> y = {{2}};
      assert(ext::ConvertArray<int>::convert(x) == y);
    }
    {
      std::array<char, 1> x = {{2}};
      std::array<char, 1> y = {{2}};
      assert(ext::ConvertArray<char>::convert(x) == y);
    }

    // convert_array
    {
      std::array<char, 1> x = {{2}};
      std::array<int, 1> y = {{2}};
      assert(ext::convert_array<int>(x) == y);
    }
    {
      std::array<char, 1> x = {{2}};
      std::array<char, 1> y = {{2}};
      assert(ext::convert_array<char>(x) == y);
    }

    // filled_array
    {
      std::array<int, 1> x = {{2}};
      assert((ext::filled_array<std::array<int, 1> >(2) == x));
    }
    {
      std::array<int, 2> x = {{2, 2}};
      assert((ext::filled_array<std::array<int, 2> >(2) == x));
    }
    {
      std::array<int, 3> x = {{2, 2, 2}};
      assert((ext::filled_array<std::array<int, 3> >(2) == x));
    }

    // copy_array
    {
      int data[] = {2};
      std::array<int, 1> x = {{2}};
      assert((ext::copy_array<std::array<int, 1> >(data) == x));
    }
    {
      int data[] = {2, 3};
      std::array<int, 2> x = {{2, 3}};
      assert((ext::copy_array<std::array<int, 2> >(data) == x));
    }
    {
      int data[] = {2, 3, 5};
      std::array<int, 3> x = {{2, 3, 5}};
      assert((ext::copy_array<std::array<int, 3> >(data) == x));
    }
  }

  //---------------------------------------------------------------------------
  // Array Mathematical Functions
  {
    const std::array<int, 3> a = {{2, 3, 5}};
    assert(sum(a) == 10);
    assert(product(a) == 30);
    assert(min(a) == 2);
    assert(max(a) == 5);
    const std::array<int, 3> b = {{7, 11 , 13}};
    assert(min(a, b) == a);
    assert(max(a, b) == b);
    assert(dot(a, b) == 112);
    {
      const std::array<int, 3> r = {{ -16, 9, 1}};
      assert(cross(a, b) == r);
    }
    {
      std::array<int, 3> c;
      cross(a, b, &c);
      const std::array<int, 3> r = {{ -16, 9, 1}};
      assert(c == r);
    }
    const std::array<int, 3> c = {{17, 19, 23}};
    assert(tripleProduct(a, b, c) == -78);
  }
  {
    const std::array<int, 2> a = {{2, 3}};
    const std::array<int, 2> b = {{5, 7}};
    assert(discriminant(a, b) == 2 * 7 - 3 * 5);
  }
  {
    std::array<double, 2> a = {{3, 4}};
    assert(squaredMagnitude(a) == 25.);
    assert(std::abs(magnitude(a) - 5.) <
           5. * std::numeric_limits<double>::epsilon());
    normalize(&a);
    assert(std::abs(magnitude(a) - 1.) <
           std::numeric_limits<double>::epsilon());
  }
  {
    std::array<double, 2> a = {{1., -1.}};
    negateElements(&a);
    assert(a == (std::array<double, 2>{{-1., 1.}}));
  }
  {
    std::array<bool, 2> a = {{false, true}};
    negateElements(&a);
    assert(a == (std::array<bool, 2>{{true, false}}));
  }
  {
    const std::array<double, 2> a = {{1, 2}};
    const std::array<double, 2> b = {{4, 6}};
    assert(squaredDistance(a, b) == 25.);
    assert(std::abs(euclideanDistance(a, b) - 5.) <
           5. * std::numeric_limits<double>::epsilon());
  }
  //---------------------------------------------------------------------------
  // SIMD implementations.
  {
    assert(dot(std::array<float, 3>{{1, 2, 3}},
               std::array<float, 3>{{2, 3, 5}}) == 23);
    assert(dot(std::array<float, 4>{{1, 2, 3, 4}},
               std::array<float, 4>{{2, 3, 5, 7}}) == 51);
    assert(dot(std::array<double, 4>{{1, 2, 3, 4}},
               std::array<double, 4>{{2, 3, 5, 7}}) == 51);
    assert(squaredDistance(std::array<float, 3>{{1, 2, 3}},
                           std::array<float, 3>{{2, 3, 5}}) == 6);
    assert(squaredDistance(std::array<float, 4>{{1, 2, 3, 4}},
                           std::array<float, 4>{{2, 3, 5, 7}}) == 15);
    assert(squaredDistance(std::array<double, 4>{{1, 2, 3, 4}},
                           std::array<double, 4>{{2, 3, 5, 7}}) == 15);
  }

  return 0;
}
