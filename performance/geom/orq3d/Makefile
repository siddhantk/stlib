# Makefile

CXX = g++           # use GNU C++ compiler 
CXXINCLUDE  = -I.. -I../../..

CXXFLAGS    = -O3 -Wall $(CXXINCLUDE)
ifeq ($(CXX),g++)
CXXFLAGS = -O3 -Wall -ansi -pedantic $(CXXINCLUDE) 
endif
ifeq ($(CXX),icc)
# Display errors and warning but not remarks.
CXXFLAGS = -O3 -strict_ansi $(CXXINCLUDE) 
endif

SOURCES = $(wildcard *.cc)
DEPENDENCIES = $(SOURCES:.cc=.d)
TARGETS = $(SOURCES:.cc=.exe)

.SUFFIXES:                   # get rid of predefined rules 
.SUFFIXES: .cc .d .o .exe

all: $(TARGETS)

clean: 
	$(RM) *.o *~ core* *.stackdump 

distclean: 
	$(MAKE) clean 
	$(RM) $(TARGETS) $(DEPENDENCIES)

again: 
	$(MAKE) distclean 
	$(MAKE) 

run: $(TESTS)

# Implicit rules.

.cc.d: 
	$(CXX) -MM $(CXXINCLUDE) $< > $@.$$$$; \
  sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
  $(RM) $@.$$$$

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include $(DEPENDENCIES)
endif
endif

.cc.o: 
	$(CXX) $(CXXFLAGS) -c $< 

.o.exe:
	$(CXX) $(LDFLAGS) -o $@ $<

#	$Log$
#	Revision 1.2  2006/07/07 23:08:58  sean
#	Improved portability.
#
#	Revision 1.1  2006/06/28 22:42:24  sean
#	Fixed compilation issues with GCC 4.0.
#	
#	Revision 1.1.1.1  2005/09/15 20:00:30  sean
#	Original
#	
#	Revision 1.1.1.1  2005/02/01 23:48:48  sean
#	Original import.
#	
#	Revision 1.3  2004/07/30 22:14:28  sean
#	Updated.  Now the sources are determined with a wildcard and there is
#	automatic dependency generation.
#	
#	Revision 1.2  2004/02/06 00:39:54  sean
#	Use new build procedure macro.
#	
#	Revision 1.1  2003/07/29 20:16:54  sean
#	Changed to geom namespace.
#	
#	Revision 1.2  2003/04/11 22:28:23  sean
#	Included directory to find the ads library.
#	
#	Revision 1.1  2003/03/26 00:05:57  sean
#	Updated for compgeom library.
#	
#	Revision 1.11  2002/06/17 20:19:24  sean
#	Added the .orq targets.
#	
#	Revision 1.10  2002/06/10 18:48:57  sean
#	Added CellArray.orq.
#	
#	Revision 1.9  2002/06/01 21:53:00  sean
#	Added SequentialScan.
#	
#	Revision 1.8  2002/05/14 22:59:19  sean
#	Updated for new classes.
#	
#	Revision 1.7  2002/05/13 04:20:59  sean
#	Added  KDTree.domain.mesh.
#	
#	Revision 1.6  2002/05/10 21:03:46  sean
#	Added CellXYBinarySearchZ, CellXYForwardSearchKeyZ, Placebo and
#	PlaceboCheck.
#	
#	Revision 1.5  2002/05/09 21:45:37  sean
#	Added CellXYForwardSearchZ.
#	
#	Revision 1.4  2002/05/08 21:15:13  sean
#	Added targets.  Added SortRankProject.
#	
#	Revision 1.3  2002/05/07 23:59:30  sean
#	Added CellXYForwardSearchZArray and SortProject.
#	
#	Revision 1.2  2002/05/06 04:37:49  sean
#	Added CellForwardSearch.
#	
#	Revision 1.1  2002/05/04 17:21:30  sean
#	Moved timing code to time directory.
#	
#	Revision 1.2  2002/05/03 23:41:16  sean
#	Cleaned up.
#	
#	Revision 1.1  2002/05/01 05:44:46  sean
#	Original source.
#	
