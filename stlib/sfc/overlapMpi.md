# Gather Overlapping Objects

# Introduction

CONTINUE HERE

Axis-aligned geometry.

\image html overlap_ballInt_time.png "Total time to gather the overlapping objects for ball intersection (Axis-aligned)."
\image latex overlap_ballInt_time.png "Total time to gather the overlapping objects for ball intersection (Axis-aligned)."

\image html overlap_ballInt_comm.png "Storage for communication buffers (Axis-aligned)."
\image latex overlap_ballInt_comm.png "Storage for communication buffers (Axis-aligned)."

\image html overlap_ballInt_relevant.png "Storage for relevant objects (Axis-aligned)."
\image latex overlap_ballInt_relevant.png "Storage for relevant objects (Axis-aligned)."

Rotated geometry.


\image html overlap_ballInt-t_time.png "Total time to gather the overlapping objects for ball intersection (Rotated)."
\image latex overlap_ballInt-t_time.png "Total time to gather the overlapping objects for ball intersection (Rotated)."

\image html overlap_ballInt-t_comm.png "Storage for communication buffers (Rotated)."
\image latex overlap_ballInt-t_comm.png "Storage for communication buffers (Rotated)."

\image html overlap_ballInt-t_relevant.png "Storage for relevant objects (Rotated)."
\image latex overlap_ballInt-t_relevant.png "Storage for relevant objects (Rotated)."


Rotated geometry that has been redistributed using a space-filling curve.

\image html overlap_ballInt-t-d_time.png "Total time to gather the overlapping objects for ball intersection (Rotated and distributed with an SFC)."
\image latex overlap_ballInt-t-d_time.png "Total time to gather the overlapping objects for ball intersection (Rotated and distributed with an SFC)."

\image html overlap_ballInt-t-d_comm.png "Storage for communication buffers (Rotated and distributed with an SFC)."
\image latex overlap_ballInt-t-d_comm.png "Storage for communication buffers (Rotated and distributed with an SFC)."

\image html overlap_ballInt-t-d_relevant.png "Storage for relevant objects (Rotated and distributed with an SFC)."
\image latex overlap_ballInt-t-d_relevant.png "Storage for relevant objects (Rotated and distributed with an SFC)."

