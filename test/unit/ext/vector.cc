// -*- C++ -*-

#include "stlib/ext/vector.h"

#include <sstream>
#include <limits>

int
main()
{
  //----------------------------------------------------------------------------
  // Vector Assignment Operators with a Scalar Operand.
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    // +=
    {
      std::vector<int> b = a;
      b += 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] + 1);
      }
    }
    // +=
    {
      std::vector<int> b = a;
      b += char(1);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] + char(1));
      }
    }
    // -=
    {
      std::vector<int> b = a;
      b -= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] - 1);
      }
    }
    // *=
    {
      std::vector<int> b = a;
      b *= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] * 2);
      }
    }
    // /=
    {
      std::vector<int> b = a;
      b /= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] / 2);
      }
    }
    // %=
    {
      std::vector<int> b = a;
      b %= 2;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] % 2);
      }
    }
    // <<=
    {
      std::vector<int> b = a;
      b <<= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] << 1);
      }
    }
    // >>=
    {
      std::vector<int> b = a;
      b >>= 1;
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == a[n] >> 1);
      }
    }
  }

  //---------------------------------------------------------------------------
  // Vector Assignment Operators with a Vector Operand.
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    std::vector<int> b;
    b.push_back(1);
    b.push_back(2);
    b.push_back(3);
    // +=
    {
      std::vector<int> c = a;
      c += b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] + b[n]);
      }
    }
    // -=
    {
      std::vector<int> c = a;
      c -= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] - b[n]);
      }
    }
    // *=
    {
      std::vector<int> c = a;
      c *= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] * b[n]);
      }
    }
    // /=
    {
      std::vector<int> c = a;
      c /= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] / b[n]);
      }
    }
    // %=
    {
      std::vector<int> c = a;
      c %= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] % b[n]);
      }
    }
    // <<=
    {
      std::vector<int> c = a;
      c <<= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] << b[n]);
      }
    }
    // >>=
    {
      std::vector<int> c = a;
      c >>= b;
      for (std::size_t n = 0; n != c.size(); ++n) {
        assert(c[n] == a[n] >> b[n]);
      }
    }
  }

  //---------------------------------------------------------------------------
  // File I/O.
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    std::cout << a << '\n';
    std::ostringstream out;
    out << a;
    std::vector<int> b;
    std::istringstream in(out.str());
    in >> b;
    assert(a == b);
  }
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    std::ostringstream out;
    writeElements(out, a);
    std::vector<int> b;
    std::istringstream in(out.str());
    readElements(in, &b);
    assert(a == b);
  }
  {
    std::vector<double> a = {2., 3., 5., std::numeric_limits<double>::max(),
                             std::numeric_limits<double>::infinity()};
    std::cout << a << '\n';
    std::ostringstream out;
    write(out, a);
    std::vector<double> b;
    std::istringstream in(out.str());
    read(in, &b);
    assert(a == b);
  }
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    std::vector<unsigned char> buffer(serializedSize(a));
    write(&buffer.front(), a);
    std::vector<int> b;
    read(&buffer.front(), &b);
    assert(a == b);
  }
  {
    std::vector<int> const a = {2, 3, 5};
    std::vector<int> const b = {1, 1, 2};
    std::vector<unsigned char> buffer;
    write(&buffer, a);
    write(&buffer, b);
    std::vector<int> x, y;
    std::size_t const pos = read(buffer, &x);
    read(buffer, &y, pos);
    assert(x == a);
    assert(y == b);
  }

  //---------------------------------------------------------------------------
  // Vector Mathematical Functions
  {
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(5);
    assert(sum(a) == 10);
    assert(product(a) == 30);
    assert(min(a) == 2);
    assert(max(a) == 5);
    assert(dot(a, a) == 38);
    assert(sum(a) == 10);
  }

  //---------------------------------------------------------------------------
  // Apply the Standard Math Functions.
  {
    const double Eps = 10 * std::numeric_limits<double>::epsilon();
    std::vector<double> a;
    a.push_back(0.1);
    a.push_back(0.2);
    a.push_back(0.3);
    // abs
    {
      std::vector<double> b = a;
      applyAbs(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::abs(a[n])) < Eps);
      }
    }
    // acos
    {
      std::vector<double> b = a;
      applyAcos(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::acos(a[n])) < Eps);
      }
    }
    // asin
    {
      std::vector<double> b = a;
      applyAsin(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::asin(a[n])) < Eps);
      }
    }
    // atan
    {
      std::vector<double> b = a;
      applyAtan(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::atan(a[n])) < Eps);
      }
    }
    // ceil
    {
      std::vector<double> b = a;
      applyCeil(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == std::ceil(a[n]));
      }
    }
    // cos
    {
      std::vector<double> b = a;
      applyCos(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::cos(a[n])) < Eps);
      }
    }
    // cosh
    {
      std::vector<double> b = a;
      applyCosh(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::cosh(a[n])) < Eps);
      }
    }
    // exp
    {
      std::vector<double> b = a;
      applyExp(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::exp(a[n])) < Eps);
      }
    }
    // floor
    {
      std::vector<double> b = a;
      applyFloor(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(b[n] == std::floor(a[n]));
      }
    }
    // log
    {
      std::vector<double> b = a;
      applyLog(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::log(a[n])) < Eps);
      }
    }
    // log10
    {
      std::vector<double> b = a;
      applyLog10(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        // CONTINUE: The function works but the test does not. Very strange.
        //std::cout << a[n] << ' ' << b[n] << ' ' << std::log10(a[n]) << '\n';
        assert(std::abs(b[n] - std::log10(a[n])) < Eps);
      }
    }
    // sin
    {
      std::vector<double> b = a;
      applySin(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::sin(a[n])) < Eps);
      }
    }
    // sinh
    {
      std::vector<double> b = a;
      applySinh(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::sinh(a[n])) < Eps);
      }
    }
    // sqrt
    {
      std::vector<double> b = a;
      applySqrt(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::sqrt(a[n])) < Eps);
      }
    }
    // tan
    {
      std::vector<double> b = a;
      applyTan(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::tan(a[n])) < Eps);
      }
    }
    // tanh
    {
      std::vector<double> b = a;
      applyTanh(&b);
      for (std::size_t n = 0; n != b.size(); ++n) {
        assert(std::abs(b[n] - std::tanh(a[n])) < Eps);
      }
    }
  }

  return 0;
}
