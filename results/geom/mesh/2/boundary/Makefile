# -*- Makefile -*-

# Extract the boundary of a mesh.

DATA_PATH = ../../../../../data/geom/mesh/22
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = mesh.vtu boundary.vtu \
  mesh.eps boundary.eps

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

# The mesh of the letter 'A'.
mesh.txt:
	cp $(DATA_PATH)/a_23.txt mesh.txt

# We need an explicit rule for the 1-D manifold.
boundary.vtu: boundary.txt
	$(BIN_PATH)/iss2vtk21.exe $< $@


# Extract the boundary.
boundary.txt: mesh.txt
	$(BIN_PATH)/boundary22.exe mesh.txt boundary.txt 

# We need an explicit rule for the 1-D manifold.
boundary.dat: boundary.txt
	python.exe $(PY_PATH)/txt2gp21.py $< $@

# End of file.