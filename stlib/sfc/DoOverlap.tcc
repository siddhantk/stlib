// -*- C++ -*-

#if !defined(__sfc_DoOverlap_tcc__)
#error This file is an implementation detail of DoOverlap.
#endif

namespace stlib
{
namespace sfc
{


template<typename _Traits>
inline
DoOverlap<_Traits>::
DoOverlap(std::vector<BBox>&& objectBoxes) :
  _objectBoxes(std::move(objectBoxes)),
  // Build the linear orthant trie and organize the object bounding boxes.
  _trie(linearOrthantTrie<Trie>(&_objectBoxes)),
  // Cache topological information for the trie.
  _topology(_trie)
{
}


template<typename _Traits>
inline
bool
DoOverlap<_Traits>::
operator()(BBox const& queryBox, std::vector<std::size_t>* stack) const
{
  return doOverlap(_trie, _topology, _objectBoxes, stack, queryBox);
}


template<typename _Traits, bool _StoreDel>
inline
bool
doOverlap
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, _StoreDel> const&
 trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& box)
{
  typedef Siblings<_Traits::Dimension> Siblings;

  // Initialize the stack of cells.
  stack->clear();
  if (! trie.empty()) {
    stack->push_back(0);
  }
  // Traverse the trie.
  while (! stack->empty()) {
    // Get the current cell.
    std::size_t const cell = stack->back();
    stack->pop_back();
    // If the bounding box for this cell overlaps the box.
    if (doOverlap(trie[cell], box)) {
      if (topology.isInternal(cell)) {
        // Add the children to the stack.
        Siblings const& children = topology.children(cell);
        stack->insert(stack->end(), children.rbegin(), children.rend());
      }
      else {
        // The box overlaps this leaf's bounding box.
        return true;
      }
    }
  }
  return false;
}


template<typename _Traits>
inline
bool
doOverlap
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, true> const& trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<geom::BBox<typename _Traits::Float, _Traits::Dimension> > const&
 objectBoxes,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& queryBox)
{
  typedef Siblings<_Traits::Dimension> Siblings;

  // Initialize the stack of cells.
  stack->clear();
  if (! trie.empty()) {
    stack->push_back(0);
  }
  // Traverse the trie.
  while (! stack->empty()) {
    // Get the current cell.
    std::size_t const cell = stack->back();
    stack->pop_back();
    // If the bounding box for this cell overlaps the query box.
    if (doOverlap(trie[cell], queryBox)) {
      if (topology.isInternal(cell)) {
        // Add the children to the stack.
        Siblings const& children = topology.children(cell);
        stack->insert(stack->end(), children.rbegin(), children.rend());
      }
      else {
        // Check the objects in this leaf.
        std::size_t const end = trie.delimiter(trie.next(cell));
        for (std::size_t i = trie.delimiter(cell); i != end; ++i) {
          if (doOverlap(objectBoxes[i], queryBox)) {
            return true;
          }
        }
      }
    }
  }
  return false;
}


template<typename _T, std::size_t _D>
inline
std::vector<bool>
doOverlap(std::vector<geom::BBox<_T, _D> >&& objectBoxes,
          std::vector<geom::BBox<_T, _D> > const& queryBoxes)
{
  typedef Traits<_D, _T> Traits;
  typedef geom::BBox<_T, _D> BBox;
  typedef LinearOrthantTrie<Traits, BBox, true> Trie;
  using performance::start;
  using performance::stop;
  using performance::record;

  performance::Scope _("sfc::doOverlap()");
  record("Number of object boxes", objectBoxes.size());
  record("Number of query boxes", queryBoxes.size());
  start("Build the trie");
  // Build the linear orthant trie and organize the object bounding boxes.
  Trie trie = linearOrthantTrie<Trie>(&objectBoxes);
  stop();
  start("Cache topological information");
  // Cache topological information for the trie.
  LinearOrthantTrieTopology<Traits::Dimension> topology(trie);
  stop();
  start("Overlap queries");
  // For each query box, determine if it overlaps any object box.
  std::vector<bool> result(queryBoxes.size());
  std::vector<std::size_t> stack;
  for (std::size_t i = 0; i != result.size(); ++i) {
    result[i] = doOverlap(trie, topology, objectBoxes, &stack, queryBoxes[i]);
  }
  stop();
  return result;
}


template<typename _T, std::size_t _D>
inline
bool
doOverlapBrute(std::vector<geom::BBox<_T, _D> > const& objectBoxes,
               geom::BBox<_T, _D> const& queryBox)
{
  for (auto const& objectBox: objectBoxes) {
    if (doOverlap(objectBox, queryBox)) {
      return true;
    }
  }
  return false;
}


template<typename _T, std::size_t _D>
inline
std::vector<bool>
doOverlapBrute(std::vector<geom::BBox<_T, _D> > const& objectBoxes,
               std::vector<geom::BBox<_T, _D> > const& queryBoxes)
{
  std::vector<bool> result(queryBoxes.size());
  for (std::size_t i = 0; i != result.size(); ++i) {
    result[i] = doOverlapBrute(objectBoxes, queryBoxes[i]);
  }
  return result;
}


} // namespace sfc
} // namespace stlib
