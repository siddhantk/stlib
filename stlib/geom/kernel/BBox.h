// -*- C++ -*-

/**
  \file
  \brief Implements a class for an axis-aligned bounding box in N dimensions.
*/

#if !defined(__geom_BBox_h__)
#define __geom_BBox_h__

#include "stlib/ext/array.h"

#include <boost/config.hpp>

#include <vector>

#include <cmath>

namespace stlib
{
namespace geom
{

/// An axis-oriented bounding box in the specified dimension.
/**
  \param _T is the number type.
  \param _D is the dimension.

  A bounding box is used to provide a coarse description of the geometry
  of an object, or set of objects. Thus, the most important operation 
  is building - one can build a bounding box that contains an object or
  a range of objects. Also, one can add objects to an existing bounding box.
  We have implemented building and adding to bounding boxes in a general
  and extensible way. That is, there is built-in support for some 
  common objects, like points, simplices, and bounding balls, and there
  is a method for adding support for user-defined objects, like a polyhedron.

  This class is a POD type. Thus it has no user-defined constructors.
  The box is defined by its lower and upper corners, which have the
  data members \c lower and \c upper.
  To construct a bounding box, use an initializer list. Below we construct
  a bounding box that is the unit cube with lower corner at the origin.
  \code
  stlib::geom::BBox<float, 3> box = {{{0, 0, 0}}, {{1, 1, 1}}};
  \endcode

  We use the bbox() function to build bounding boxes. The function may
  take 0, 1, or 2 arguments. Calling this function with no arguments
  produces an empty bounding box. All of the functions are templated
  on the bounding box type, and this type must be specified explicitly.
  Below we build an empty bounding box and verify that it is indeed empty.
  \code
  typedef stlib::geom::BBox<float, 3> BBox;
  BBox box = stlib::geom::bbox<BBox>();
  assert(isEmpty(box));
  \endcode

  To bound an object, use bbox(_Object const& object). Bounding the following 
  objects is automatically supported.
  - A Cartesian point, i.e. \c std::array<T, D>.
  - A bounding box. Note that the number types need not be the same.
  - An array of points.
  - Ball, a bounding ball.
  .
  Below are examples of bounding various objects.
  \code
  using stlib::geom::bbox;
  typedef stlib::geom::BBox<float, 2> BBox;
  // Bound a point.
  BBox pointBBox = bbox<BBox>(std::array<float, 2>{{2, 3}});
  // Bound a bounding box with a different number type.
  stlib::geom::BBox<double, 2> square = {{{0, 0}}, {{1, 1}}};
  BBox boxBBox = bbox<BBox>(square);
  // Bound a triangle.
  typedef std::array<std::array<float, 2>, 3> Triangle;
  Triangle triangle = {{{{0, 0}}, {{1, 0}}, {{0, 1}}}};
  BBox triangleBBox = bbox<BBox>(triangle);
  // Bound a bounding ball.
  stlib::geom::Ball<float, 2> ball = {{{0, 0}}, 1};
  BBox ballBBox = bbox<BBox>(ball);
  \endcode
  You can add support for more object types by specializing the BBoxForObject
  struct. The static \c create() function specifies how to build the 
  bounding box. Note that for the objects with built-in support, we 
  allow conversions between number types. That is, we can build a 
  single-precision bounding box for an object that uses double-precision
  floating-point numbers, and vice versa.

  To bound a range of objects, use
  bbox(_InputIterator begin, _InputIterator end).
  The value type for the iterators may be any supported object. Below we bound
  a vector of tetrahedra (which has built-in support).
  \code
  using stlib::geom::bbox;
  typedef stlib::geom::BBox<double, 3> BBox;
  typedef std::array<std::array<double, 3>, 4> Tetrahedron;
  std::vector<Tetrahedron> tetrahedra;
  // Add tetrahedra.
  ...
  // Bound the tetrahedra.
  BBox domain = bbox<BBox>(tetrahedra.begin(), tetrahedra.end());
  \endcode

  Most of the time one will know the type of the bounding box that one 
  wants to build. (That is, the floating-point number type and space dimension
  are specified.) However, sometimes it is convenient to deduce an appropriate
  bounding box type from the objects being bounded. For this we use the
  defaultBBox() functions. Whereas the bbox() functions are templated
  on the bounding box type, these are templated on the object type.
  Note that specialization of the BBoxForObject struct must define the
  \c DefaultBBox type, which, as the name suggests, defines a default
  bounding box type for the object.

  Below we build an empty bounding box that is suitable for bounding 
  3-D points using double-precision floating-point numbers. Note that we
  need to explicitly specify the template parameter because there are
  no function arguments.
  \code
  typedef std::array<double, 3> Object;
  auto box = stlib::geom::defaultBBox<Object>();
  assert(isEmpty(box));
  \endcode

  To build a default bounding box for an object, use
  defaultBBox(_Object const& object). Below are examples of bounding
  various objects.
  \code
  using stlib::geom::defaultBBox;
  // Bound a point.
  auto pointBBox = defaultBBox(std::array<float, 2>{{2, 3}});
  // Bound a bounding box.
  stlib::geom::BBox<double, 2> square = {{{0, 0}}, {{1, 1}}};
  auto boxBBox = defaultBBox(square);
  // Bound a triangle.
  typedef std::array<std::array<float, 2>, 3> Triangle;
  Triangle triangle = {{{{0, 0}}, {{1, 0}}, {{0, 1}}}};
  auto triangleBBox = defaultBBox(triangle);
  // Bound a bounding ball.
  stlib::geom::Ball<float, 2> ball = {{{0, 0}}, 1};
  auto ballBBox = defaultBBox(ball);
  \endcode

  To build a default bounding box for a range of objects, use
  defaultBBox(_InputIterator begin, _InputIterator end).
  The value type for the iterators may be any supported object. Below we bound
  a vector of tetrahedra (which has built-in support).
  \code
  using stlib::geom::defaultBBox;
  typedef std::array<std::array<double, 3>, 4> Tetrahedron;
  std::vector<Tetrahedron> tetrahedra;
  // Add tetrahedra.
  ...
  // Bound the tetrahedra.
  auto domain = defaultBBox(tetrahedra.begin(), tetrahedra.end());
  \endcode

  After building, the next most common thing that one does with bounding 
  boxes is to add objects to them. That is, make the bounding box expand to 
  contain the object. Objects are added to a bounding box with the += operator.
  Below we start with an empty bounding box and add various objects to it.
  \code
  typedef stlib::geom::BBox<float, 2> BBox;
  // Start with an empty bounding box.
  BBox box = bbox<BBox>();
  // Add a point.
  box += std::array<float, 2>{{2, 3}};
  // Add a bounding box with a different number type.
  box += stlib::geom::BBox<double, 2>{{{0, 0}}, {{1, 1}}};
  // Add a triangle.
  typedef std::array<std::array<float, 2>, 3> Triangle;
  box += Triangle{{{{0, 0}}, {{1, 0}}, {{0, 1}}}};
  // Add a ball.
  box += stlib::geom::Ball<float, 2>{{{0, 0}}, 1};
  \endcode

  You can check bounding boxes for equality and inequality. Note that 
  any two empty bounding boxes are equal, regardless of the specific values
  of the coordinates. In the following example, the lower and upper 
  coordinates for the first bounding box are infinity and negative infinity,
  respectively. The second bounding box has different coordinate values, 
  but is also empty, so the two are equal.
  \code
  typedef stlib::geom::BBox<float, 2> BBox;
  assert(stlib::geom::bbox<BBox>() == (BBox{{{1, 1}}, {{0, 0}}}));
  \endcode

  You can read and write bounding boxes in ascii format. The representation of
  the bounding box is just the lower and upper corners.
  \code
  typedef stlib::geom::BBox<float, 2> BBox;
  std::cout << "Enter a bounding box for the domain.\n";
  std::cin >> domain;
  std::cout << "The domain is " << domain << ".\n";
  \endcode

  You can calculate the centroid of a bounding box with
  centroid(BBox<_T, _D> const&).
  \code
  typedef stlib::geom::BBox<float, 2> BBox;
  assert(centroid(BBox{{{1, 2}}, {{5, 7}}}) == (std::array<float, 2>{{3, 4.5}}));
  \endcode

  Offsetting, expanding or contracting, is a common operation with bounding
  boxes. Often one expands a bounding box to account for truncation errors
  or to account for some interaction distance. In the following example
  we expand the bounding box by the machine epsilon times the maximum edge 
  length.
  \code
  typedef stlib::geom::BBox<double, 3> BBox;
  BBox domain = ...;
  // Account for truncation errors that max occur in some subsequent calculations.
  offset(&domain, std::numeric_limits<double>::epsilon() * max(domain.upper - domain.lower));
  \endcode

  Use isInside() to test whether an object is inside a bounding box.
  Below are some examples.
  \code
  using stlib::geom::bbox;
  typedef stlib::geom::BBox<double, 2> BBox;
  // No object is inside an empty bounding box.
  assert(! isInside(bbox<BBox>(), std::array<double, 2>{{0, 0}}));
  assert(! isInside(bbox<BBox>(), bbox<BBox>()));
  // An empty bounding box is inside any non-empty bounding box.
  assert(isInside(BBox{{{0, 0}}, {{0, 0}}}, bbox<BBox>()));
  // The specified triangle in inside the unit square.
  typedef std::array<std::array<double, 2>, 3> Triangle;
  Triangle triangle const = {{{{0, 0}}, {{1, 0}}, {{0, 1}}}};
  assert(isInside(BBox{{{0, 0}}, {{1, 1}}}, triangle));
  \endcode

  Use content(BBox<_T, _D> const&) to calculate the content
  (length, area, volume, etc.) of a bounding box.
  \code
  typedef stlib::geom::BBox<double, 2> BBox;
  // The content of on empty bounding box is zero.
  assert(content(stlib::geom::bbox<BBox>()) == 0);
  // Check the area of a bounding box.
  assert(content(BBox{{{0, 0}}, {{1, 2}}}) == 2);
  \endcode

  Use doOverlap(BBox<_T, _D> const&, BBox<_T, _D> const&) to check if two
  bounding boxes overlap. Note that nothing overlaps an empty bounding box.
  \code
  using stlib::geom::bbox;
  typedef stlib::geom::BBox<float, 1> BBox;
  assert(! doOverlap(bbox<BBox>(), bbox<BBox>()));
  assert(! doOverlap(BBox{{{0}}, {{1}}}, bbox<BBox>()));
  assert(doOverlap(BBox{{{0}}, {{1}}}, BBox{{{1}}, {{2}}}));
  \endcode

  To calculate the intersection of bounding boxes, use
  intersection(BBox<_T, _D> const&, BBox<_T, _D> const&).
  \code
  using stlib::geom::bbox;
  typedef stlib::geom::BBox<float, 1> BBox;
  assert(intersection(bbox<BBox>(), bbox<BBox>()) == bbox<BBox>());
  assert(intersection(BBox{{{0}}, {{1}}}, BBox{{{1}}, {{2}}}) == (BBox{{{1}}, {{1}}}));
  \endcode
*/
template<typename _T, std::size_t _D>
struct BBox {
  // Types.

  /// The number type.
  typedef _T Number;
  /// The point type.
  typedef std::array<_T, _D> Point;

  // Constants.

  /// The space dimension.
  BOOST_STATIC_CONSTEXPR std::size_t Dimension = _D;

  // Member data

  /// The lower corner.
  Point lower;
  /// The upper corner.
  Point upper;
};


/// Create an empty bounding box.
/** \relates BBox
    If any of the lower coordinates exceed the corresponding upper coordinate,
    then the bounding box is empty. Thus, for example, setting the lower 
    coordinates to 1 and the upper coordinates to 0 would yield an empty
    bounding box. However, in this function we go one step further. We set
    the coordinates to values that would likely cause an error if they were
    inadvertantly used. If the number type has a representation of infinity, 
    then the lower coordinates are set to positive infinity, while the 
    upper coordinates are set to negavite infinity. For other number types,
    we use use the \c max() and \c min() functions in \c std::numeric_limits
    to set coordinates values.
*/
template<typename _BBox>
_BBox
bbox();


/// Create a tight axis-aligned bounding box for the object.
/** Specialize this struct for every object that you want to bound
    with bbox(). The specialization must define the \c DefaultBBox
    type. It must also have a static create() function that takes a
    const reference to the object as an argument and returns the
    bounding box. */
template<typename _Float, typename _Object>
struct BBoxForObject;


/// Create a tight axis-aligned bounding box for the object.
/** \relates BBox */
template<typename _BBox, typename _Object>
inline
_BBox
bbox(_Object const& object)
{
  return BBoxForObject<typename _BBox::Number, _Object>::create(object);
}


/// Create a tight axis-aligned bounding box for the range of objects.
/** \relates BBox */
template<typename _BBox, typename _InputIterator>
_BBox
bbox(_InputIterator begin, _InputIterator end);


/// Create a tight axis-aligned bounding box for each object in the range.
/** \relates BBox */
template<typename _BBox, typename _ForwardIterator>
std::vector<_BBox>
bboxForEach(_ForwardIterator begin, _ForwardIterator end);


/// Create an empty bounding box.
/** \relates BBox
    If any of the lower coordinates exceed the corresponding upper coordinate,
    then the bounding box is empty. Thus, for example, setting the lower 
    coordinates to 1 and the upper coordinates to 0 would yield an empty
    bounding box. However, in this function we go one step further. We set
    the coordinates to values that would likely cause an error if they were
    inadvertantly used. If the number type has a representation of infinity, 
    then the lower coordinates are set to positive infinity, while the 
    upper coordinates are set to negavite infinity. For other number types,
    we use use the \c max() and \c min() functions in \c std::numeric_limits
    to set coordinates values.
*/
template<typename _Object>
inline
typename BBoxForObject<void, _Object>::DefaultBBox
defaultBBox()
{
  return bbox<typename BBoxForObject<void, _Object>::DefaultBBox>();
}


/// Create a tight axis-aligned bounding box for the object.
/** \relates BBox */
template<typename _Object>
inline
typename BBoxForObject<void, _Object>::DefaultBBox
defaultBBox(_Object const& object)
{
  return bbox<typename BBoxForObject<void, _Object>::DefaultBBox>(object);
}


/// Create a tight axis-aligned bounding box for the range of objects.
/** \relates BBox */
template<typename _InputIterator>
inline
typename BBoxForObject<void, typename std::iterator_traits<_InputIterator>::value_type>::DefaultBBox
defaultBBox(_InputIterator begin, _InputIterator end)
{
  typedef typename BBoxForObject<void, 
  typename std::iterator_traits<_InputIterator>::value_type>::DefaultBBox BBox;
  return bbox<BBox>(begin, end);
}


/// Create a tight axis-aligned bounding box for each object in the range.
/** \relates BBox */
template<typename _ForwardIterator>
inline
std::vector<typename BBoxForObject<void, typename std::iterator_traits<_ForwardIterator>::value_type>::DefaultBBox>
defaultBBoxForEach(_ForwardIterator begin, _ForwardIterator end)
{
  typedef typename BBoxForObject<void, 
  typename std::iterator_traits<_ForwardIterator>::value_type>::DefaultBBox
    BBox;
  return bboxForEach<BBox>(begin, end);
}


//
// Equality Operators.
//


/// Equality.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
bool
operator==(BBox<_T, _D> const& a, BBox<_T, _D> const& b)
{
  return (isEmpty(a) && isEmpty(b)) ||
    (a.lower == b.lower && a.upper == b.upper);
}


/// Inequality.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
bool
operator!=(BBox<_T, _D> const& a, BBox<_T, _D> const& b)
{
  return !(a == b);
}


//
// File I/O.
//


/// Read the bounding box.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
std::istream&
operator>>(std::istream& in, BBox<_T, _D>& x)
{
  return in >> x.lower >> x.upper;
}


/// Write the bounding box.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
std::ostream&
operator<<(std::ostream& out, BBox<_T, _D> const& x)
{
  return out << x.lower << ' ' << x.upper;
}


//
// Mathematical Functions.
//


/// Return true if the BBox is empty.
template<typename _T, std::size_t _D>
bool
isEmpty(BBox<_T, _D> const& box);


/// Return the centroid of the bounding box.
/** \note We do not check if the bounding box is empty. Empty bounding boxes
    do not have a centroid. Thus, the result is undefined. */
template<typename _T, std::size_t _D>
inline
std::array<_T, _D>
centroid(BBox<_T, _D> const& box)
{
  return _T(0.5) * (box.lower + box.upper);
}


/// Offset (expand or contract) by the specified amount.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
void
offset(BBox<_T, _D>* box, _T const value)
{
  box->lower -= value;
  box->upper += value;
}


/// Make the bounding box expand to contain the object.
/** \relates BBox */
template<typename _T, std::size_t _D, typename _Object>
inline
BBox<_T, _D>&
operator+=(BBox<_T, _D>& box, _Object const& object)
{
  return box += bbox<BBox<_T, _D> >(object);
}


/// Make the bounding box expand to contain the point.
/** \relates BBox */
template<typename _T, std::size_t _D>
BBox<_T, _D>&
operator+=(BBox<_T, _D>& box, std::array<_T, _D> const& p);


/// Make the bounding box expand to contain the box.
/** \relates BBox */
template<typename _T, std::size_t _D>
BBox<_T, _D>&
operator+=(BBox<_T, _D>& box, BBox<_T, _D> const& rhs);


/// Return true if the object is contained in the bounding box.
/** \relates BBox */
template<typename _T, std::size_t _D, typename _Object>
bool
isInside(BBox<_T, _D> const& box, _Object const& x);


/// Return true if the point is in the bounding box.
/** \relates BBox */
template<typename _T, std::size_t _D>
bool
isInside(BBox<_T, _D> const& box, std::array<_T, _D> const& p);


/// Return true if the second bounding box is in the first bounding box.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
bool
isInside(BBox<_T, _D> const& box, BBox<_T, _D> const& x)
{
  // Nothing can be inside an empty bounding box.
  if (isEmpty(box)) {
    return false;
  }
  // Barring that, an empty bounding is inside any other bounding box.
  if (isEmpty(x)) {
    return true;
  }
  // If neither is empty, we compare coordinates.
  return isInside(box, x.lower) && isInside(box, x.upper);
}


/// Return the content (length, area, volume, etc.) of the box.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
_T
content(BBox<_T, _D> const& box)
{
  if (isEmpty(box)) {
    return 0;
  }
  _T x = 1;
  for (std::size_t n = 0; n != _D; ++n) {
    x *= box.upper[n] - box.lower[n];
  }
  return x;
}


/// Return the squared distance between two 1-D intervals.
template<typename _T>
_T
squaredDistanceBetweenIntervals(_T lower1, _T upper1,
                                _T lower2, _T upper2);


/// Return the squared distance between two bounding boxes.
template<typename _T, std::size_t _D>
_T
squaredDistance(BBox<_T, _D> const& x, BBox<_T, _D> const& y);


/// Return true if the bounding boxes overlap.
/** \relates BBox */
template<typename _T, std::size_t _D>
bool
doOverlap(BBox<_T, _D> const& a, BBox<_T, _D> const& b);


/// Return the intersection of the bounding boxes.
/** \relates BBox */
template<typename _T, std::size_t _D>
inline
BBox<_T, _D>
intersection(BBox<_T, _D> const& a, BBox<_T, _D> const& b)
{
  return BBox<_T, _D>{max(a.lower, b.lower), min(a.upper, b.upper)};
}




/// Scan convert the index bounding box.
/**
  \relates BBox

  \param indices is an output iterator for multi-indices. The value type
  must be std::array<Index,3> assignable where Index is the integer
  index type.
  \param box describes the range of indices. It is a bounding box of
  some floating point number type. This box will be converted to an
  integer bounding box. Then the below scan conversion function is used.
*/
template<typename Index, typename MultiIndexOutputIterator>
void
scanConvert(MultiIndexOutputIterator indices, BBox<double, 3> const& box);


/// Scan convert the index bounding box.
/**
  \relates BBox

  \param indices is an output iterator for multi-indices. The value type
  must be std::array<Index,3> assignable where Index is the integer
  index type.
  \param box describes the range of indices.
*/
template<typename MultiIndexOutputIterator, typename Index>
void
scanConvertIndex(MultiIndexOutputIterator indices, BBox<Index, 3> const& box);


/// Scan convert the index bounding box on the specified index domain.
/**
  \relates BBox

  \param indices is an output iterator for multi-indices. The value type
  must be std::array<Index,3> assignable where Index is the integer
  index type.
  \param box describes the range of indices. It is a bounding box of
  some floating point number type. This box will be converted to an
  integer bounding box. Then the below scan conversion function is used.
  \param domain is the closed range of indices on which to perform the
  scan conversion.
*/
template<typename MultiIndexOutputIterator, typename Index>
void
scanConvert(MultiIndexOutputIterator indices, BBox<double, 3> const& box,
            BBox<Index, 3> const& domain);


/// Scan convert the index bounding box on the specified index domain.
/**
  \relates BBox

  \param indices is an output iterator for multi-indices. The value type
  must be std::array<Index,3> assignable where Index is the integer
  index type.
  \param box is the closed range of indices.
  \param domain is the closed range of indices on which to perform the
  scan conversion.
*/
template<typename MultiIndexOutputIterator, typename Index>
inline
void
scanConvert(MultiIndexOutputIterator indices, BBox<Index, 3> const& box,
            BBox<Index, 3> const& domain);

} // namespace geom
} // namespace stlib

#define __geom_BBox_ipp__
#include "stlib/geom/kernel/BBox.ipp"
#undef __geom_BBox_ipp__

#endif
