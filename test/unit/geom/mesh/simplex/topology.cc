// -*- C++ -*-

#include "stlib/geom/mesh/simplex/topology.h"

#include <cassert>

using namespace stlib;

int
main()
{
  std::size_t a, b;
  geom::computeOtherIndices(0, 1, &a, &b);
  assert(a == 2 && b == 3);
  geom::computeOtherIndices(1, 0, &a, &b);
  assert(a == 2 && b == 3);
  geom::computeOtherIndices(0, 2, &a, &b);
  assert(a == 1 && b == 3);
  geom::computeOtherIndices(0, 3, &a, &b);
  assert(a == 1 && b == 2);
  geom::computeOtherIndices(1, 2, &a, &b);
  assert(a == 0 && b == 3);
  geom::computeOtherIndices(1, 3, &a, &b);
  assert(a == 0 && b == 2);
  geom::computeOtherIndices(2, 3, &a, &b);
  assert(a == 0 && b == 1);

  {
    // A 3-simplex.
    std::array < int, 3 + 1 > s = {{0, 1, 2, 3}};

    // hasElement()
    assert(hasElement(s, 0) &&
           hasElement(s, 1) &&
           hasElement(s, 2) &&
           hasElement(s, 3) &&
           ! hasElement(s, 4));

    {
      // hasFace()
      for (int i = 0; i != 4; ++i) {
        assert(geom::hasFace(s, std::array<int, 1>{{i}}));
      }
      assert(! geom::hasFace(s, std::array<int, 1>{{4}}));

      for (int i = 0; i != 4; ++i) {
        for (int j = 0; j != 4; ++j) {
          if (i != j) {
            assert(geom::hasFace(s, std::array<int, 2>{{i, j}}));
          }
        }
      }
      assert(! geom::hasFace(s, std::array<int, 2>{{0, 4}}));

      for (int i = 0; i != 4; ++i) {
        for (int j = 0; j != 4; ++j) {
          if (i != j) {
            for (int k = 0; k != 4; ++k) {
              if (k != i && k != j) {
                assert(geom::hasFace(s, std::array<int, 3>{{i, j, k}}));
              }
            }
          }
        }
      }
      assert(! geom::hasFace(s, std::array<int, 3>{{0, 1, 4}}));

      assert(geom::hasFace(s, std::array<int, 4>{{0, 1, 2, 3}}));
      assert(! geom::hasFace(s, std::array<int, 4>{{0, 1, 2, 4}}));
    }

    // index
    for (int i = 0; i != 4; ++i) {
      assert(index(s, i) == std::size_t(i));
    }

    // getFace
    for (std::size_t i = 0; i != 4; ++i) {
      assert(geom::hasFace(s, geom::getFace(s, i)));
    }

  }

  return 0;
}
