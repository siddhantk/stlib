Note that "Storage for local objects" is not correct.
:
MPI processes = 1
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects = 1.04858e+06
Total time = 0.672462

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows = 144384
Maximum objects per cell = 128
Make a vector of coarsened query windows = 0.125082
Build the description of the distributed objects = 0.264073
Calculate relevant cells = 0.150601
Gather the relevant objects = 0.0591207
Total time = 0.599145

sfc::gatherRelevantRing():
Number of distributed cells = 32768
Distributed cell storage = 1.31076e+06
Number of local objects = 1.04858e+06
Number of relevant cells = 32768
Number of object cells = 32768
Number of object cells that are relevant for others = 0
Storage for local objects = 0
Storage for relevant objects = 1.67772e+07
Storage for send buffers = 32
Storage for receive buffers = 32
Count the number of relevant objects = 0.00391779
Build object cells = 0.0513549
Record the relevant objects. = 0.00307228
Calculate cells that are relevant for others = 0.000694735
All gather the vector sizes. = 2.214e-05
Reserve memory. = 2.432e-06
Total time = 0.0591162

:
MPI processes = 2
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 2.1639e+06, mean = 1.08195e+06, min = 1.08189e+06, max = 1.08201e+06
Total time
  sum = 1.3806, mean = 0.690298, min = 0.690244, max = 0.690352

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 288922, mean = 144461, min = 144384, max = 144538
Maximum objects per cell = 128
Make a vector of coarsened query windows
  sum = 0.260909, mean = 0.130454, min = 0.130186, max = 0.130722
Build the description of the distributed objects
  sum = 0.551536, mean = 0.275768, min = 0.274746, max = 0.27679
Calculate relevant cells
  sum = 0.29328, mean = 0.14664, min = 0.146191, max = 0.147089
Gather the relevant objects
  sum = 0.123842, mean = 0.0619211, min = 0.0616497, max = 0.0621924
Total time
  sum = 1.23035, mean = 0.615173, min = 0.615135, max = 0.615211

sfc::gatherRelevantRing():
Number of distributed cells = 65536
Distributed cell storage = 2.62148e+06
Number of local objects = 1.04858e+06
Number of relevant cells = 33792
Number of object cells = 32768
Number of object cells that are relevant for others = 1024
Storage for local objects
  sum = 1.06797e+06, mean = 533984, min = 532976, max = 534992
Storage for relevant objects
  sum = 3.46224e+07, mean = 1.73112e+07, min = 1.73102e+07, max = 1.73122e+07
Storage for send buffers = 551408
Storage for receive buffers = 551408
Count the number of relevant objects
  sum = 0.00868788, mean = 0.00434394, min = 0.00415703, max = 0.00453085
Build object cells
  sum = 0.103211, mean = 0.0516054, min = 0.0515847, max = 0.051626
Record the relevant objects.
  sum = 0.00699264, mean = 0.00349632, min = 0.00348139, max = 0.00351124
Calculate cells that are relevant for others
  sum = 0.00354692, mean = 0.00177346, min = 0.00130677, max = 0.00224015
All gather the vector sizes.
  sum = 6.9718e-05, mean = 3.4859e-05, min = 2.3588e-05, max = 4.613e-05
Reserve memory.
  sum = 0.000248609, mean = 0.000124305, min = 6.891e-06, max = 0.000241718
Initiate communication.
  sum = 0.00032716, mean = 0.00016358, min = 0.000162396, max = 0.000164764
Wait and then unserialize the cells.
  sum = 0.000351365, mean = 0.000175683, min = 6.0126e-05, max = 0.000291239
Wait and then swap buffers.
  sum = 0.000212589, mean = 0.000106295, min = 0.000106247, max = 0.000106342
Total time
  sum = 0.123832, mean = 0.0619159, min = 0.0616454, max = 0.0621864

:
MPI processes = 4
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 4.46405e+06, mean = 1.11601e+06, min = 1.11584e+06, max = 1.11613e+06
Total time
  sum = 2.94261, mean = 0.735653, min = 0.735579, max = 0.735844

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 577505, mean = 144376, min = 143925, max = 144658
Maximum objects per cell = 128
Make a vector of coarsened query windows
  sum = 0.522098, mean = 0.130524, min = 0.130134, max = 0.13083
Build the description of the distributed objects
  sum = 1.21435, mean = 0.303588, min = 0.300681, max = 0.30464
Calculate relevant cells
  sum = 0.640765, mean = 0.160191, min = 0.159654, max = 0.160795
Gather the relevant objects
  sum = 0.261064, mean = 0.065266, min = 0.0636458, max = 0.0675639
Total time
  sum = 2.64074, mean = 0.660185, min = 0.660083, max = 0.660423

sfc::gatherRelevantRing():
Number of distributed cells = 131072
Distributed cell storage = 5.24292e+06
Number of local objects = 1.04858e+06
Number of relevant cells = 34848
Number of object cells
  sum = 131073, mean = 32768.2, min = 32768, max = 32769
Number of object cells that are relevant for others
  sum = 8065, mean = 2016.25, min = 2016, max = 2017
Storage for local objects
  sum = 4.18398e+06, mean = 1.046e+06, min = 1.04202e+06, max = 1.0509e+06
Storage for relevant objects
  sum = 7.14248e+07, mean = 1.78562e+07, min = 1.78534e+07, max = 1.78581e+07
Storage for send buffers = 1.0832e+06
Storage for receive buffers = 1.0832e+06
Count the number of relevant objects
  sum = 0.0182606, mean = 0.00456516, min = 0.00428254, max = 0.00481653
Build object cells
  sum = 0.203452, mean = 0.0508629, min = 0.0507151, max = 0.0510484
Record the relevant objects.
  sum = 0.0140355, mean = 0.00350888, min = 0.00336988, max = 0.00364472
Calculate cells that are relevant for others
  sum = 0.0155496, mean = 0.00388739, min = 0.00197228, max = 0.0066069
All gather the vector sizes.
  sum = 0.000299885, mean = 7.49713e-05, min = 3.1388e-05, max = 0.000143511
Reserve memory.
  sum = 0.00153688, mean = 0.000384221, min = 1.1926e-05, max = 0.000537105
Initiate communication.
  sum = 0.00152195, mean = 0.000380489, min = 0.000337333, max = 0.000440261
Wait and then unserialize the cells.
  sum = 0.00158853, mean = 0.000397132, min = 0.000192471, max = 0.000698502
Wait and then swap buffers.
  sum = 0.00435677, mean = 0.00108919, min = 0.000967794, max = 0.00122495
Total time
  sum = 0.261042, mean = 0.0652604, min = 0.0636411, max = 0.0675576

:
MPI processes = 8
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 9.21054e+06, mean = 1.15132e+06, min = 1.15088e+06, max = 1.15166e+06
Total time
  sum = 6.98347, mean = 0.872933, min = 0.872801, max = 0.873238

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 1.15617e+06, mean = 144521, min = 143925, max = 144890
Maximum objects per cell = 128
Make a vector of coarsened query windows
  sum = 1.1196, mean = 0.139951, min = 0.139359, max = 0.142286
Build the description of the distributed objects
  sum = 3.00155, mean = 0.375194, min = 0.365516, max = 0.377057
Calculate relevant cells
  sum = 1.61747, mean = 0.202184, min = 0.198165, max = 0.204168
Gather the relevant objects
  sum = 0.598499, mean = 0.0748123, min = 0.0720062, max = 0.0862493
Total time
  sum = 6.34403, mean = 0.793004, min = 0.792761, max = 0.7933

sfc::gatherRelevantRing():
Number of distributed cells = 262144
Distributed cell storage = 1.04858e+07
Number of local objects = 1.04858e+06
Number of relevant cells = 35937
Number of object cells
  sum = 262145, mean = 32768.1, min = 32768, max = 32769
Number of object cells that are relevant for others
  sum = 23817, mean = 2977.12, min = 2977, max = 2978
Storage for local objects
  sum = 1.23397e+07, mean = 1.54247e+06, min = 1.53827e+06, max = 1.54795e+06
Storage for relevant objects
  sum = 1.47369e+08, mean = 1.84211e+07, min = 1.84141e+07, max = 1.84265e+07
Storage for send buffers = 1.59563e+06
Storage for receive buffers = 1.59563e+06
Count the number of relevant objects
  sum = 0.0386877, mean = 0.00483596, min = 0.0044779, max = 0.00534853
Build object cells
  sum = 0.414009, mean = 0.0517512, min = 0.0511323, max = 0.0522918
Record the relevant objects.
  sum = 0.0363638, mean = 0.00454547, min = 0.00383578, max = 0.00552791
Calculate cells that are relevant for others
  sum = 0.0515212, mean = 0.00644015, min = 0.00305393, max = 0.0197594
All gather the vector sizes.
  sum = 0.00080335, mean = 0.000100419, min = 4.0928e-05, max = 0.00018738
Reserve memory.
  sum = 0.00398697, mean = 0.000498371, min = 1.0433e-05, max = 0.00084976
Initiate communication.
  sum = 0.00438051, mean = 0.000547564, min = 0.000232708, max = 0.000638569
Wait and then unserialize the cells.
  sum = 0.01107, mean = 0.00138376, min = 0.000821723, max = 0.00254187
Wait and then swap buffers.
  sum = 0.0364368, mean = 0.0045546, min = 0.00404649, max = 0.00495543
Total time
  sum = 0.598456, mean = 0.074807, min = 0.0720009, max = 0.0862432

:
MPI processes = 16
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 1.95781e+07, mean = 1.22363e+06, min = 1.19074e+06, max = 1.25887e+06
Total time
  sum = 17.9354, mean = 1.12096, min = 1.1142, max = 1.12454

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 2.31284e+06, mean = 144552, min = 143925, max = 145284
Maximum objects per cell = 256
Make a vector of coarsened query windows
  sum = 2.83932, mean = 0.177458, min = 0.173896, max = 0.179329
Build the description of the distributed objects
  sum = 7.66745, mean = 0.479215, min = 0.465252, max = 0.485095
Calculate relevant cells
  sum = 3.5198, mean = 0.219987, min = 0.216805, max = 0.22273
Gather the relevant objects
  sum = 2.10611, mean = 0.131632, min = 0.123959, max = 0.147525
Total time
  sum = 16.1453, mean = 1.00908, min = 1.00445, max = 1.01245

sfc::gatherRelevantRing():
Number of distributed cells = 300491
Distributed cell storage = 1.20197e+07
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 339505, mean = 21219.1, min = 20214, max = 22291
Number of object cells
  sum = 305852, mean = 19115.8, min = 18794, max = 19538
Number of object cells that are relevant for others
  sum = 41171, mean = 2573.19, min = 2470, max = 2646
Storage for local objects
  sum = 4.00822e+07, mean = 2.50514e+06, min = 2.22563e+06, max = 2.7912e+06
Storage for relevant objects
  sum = 3.13249e+08, mean = 1.95781e+07, min = 1.90518e+07, max = 2.01419e+07
Storage for send buffers = 2.83357e+06
Storage for receive buffers = 2.83357e+06
Count the number of relevant objects
  sum = 0.0928723, mean = 0.00580452, min = 0.00486401, max = 0.00626816
Build object cells
  sum = 0.857509, mean = 0.0535943, min = 0.0519066, max = 0.0655699
Record the relevant objects.
  sum = 0.10892, mean = 0.00680749, min = 0.00484632, max = 0.00910559
Calculate cells that are relevant for others
  sum = 0.250357, mean = 0.0156473, min = 0.00454401, max = 0.0278599
All gather the vector sizes.
  sum = 0.00397543, mean = 0.000248464, min = 4.9438e-05, max = 0.000424514
Reserve memory.
  sum = 0.0262247, mean = 0.00163904, min = 2.3293e-05, max = 0.00196097
Initiate communication.
  sum = 0.0375374, mean = 0.00234609, min = 0.00183186, max = 0.00274701
Wait and then unserialize the cells.
  sum = 0.211621, mean = 0.0132263, min = 0.00214652, max = 0.0302994
Wait and then swap buffers.
  sum = 0.510679, mean = 0.0319174, min = 0.0152786, max = 0.0450927
Total time
  sum = 2.106, mean = 0.131625, min = 0.123949, max = 0.147518

:
MPI processes = 32
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 4.2701e+07, mean = 1.33441e+06, min = 1.24549e+06, max = 1.42596e+06
Total time
  sum = 34.5002, mean = 1.07813, min = 1.07253, max = 1.08099

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 4.62741e+06, mean = 144607, min = 143925, max = 145284
Maximum objects per cell = 512
Make a vector of coarsened query windows
  sum = 5.51032, mean = 0.172197, min = 0.169146, max = 0.175901
Build the description of the distributed objects
  sum = 13.4341, mean = 0.419815, min = 0.408047, max = 0.433677
Calculate relevant cells
  sum = 3.40563, mean = 0.106426, min = 0.102509, max = 0.108161
Gather the relevant objects
  sum = 7.28218, mean = 0.227568, min = 0.223313, max = 0.231829
Total time
  sum = 29.6593, mean = 0.926854, min = 0.920981, max = 0.935835

sfc::gatherRelevantRing():
Number of distributed cells = 131072
Distributed cell storage = 5.24292e+06
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 166600, mean = 5206.25, min = 4913, max = 5508
Number of object cells
  sum = 139378, mean = 4355.56, min = 4096, max = 4625
Number of object cells that are relevant for others
  sum = 38458, mean = 1201.81, min = 1156, max = 1250
Storage for local objects
  sum = 1.24095e+08, mean = 3.87798e+06, min = 3.11229e+06, max = 4.6248e+06
Storage for relevant objects
  sum = 6.83216e+08, mean = 2.13505e+07, min = 1.99279e+07, max = 2.28154e+07
Storage for send buffers = 4.64483e+06
Storage for receive buffers = 4.64483e+06
Count the number of relevant objects
  sum = 0.220555, mean = 0.00689234, min = 0.00479131, max = 0.00793914
Build object cells
  sum = 1.66226, mean = 0.0519455, min = 0.0512551, max = 0.0525033
Record the relevant objects.
  sum = 0.280323, mean = 0.00876009, min = 0.00520831, max = 0.0104573
Calculate cells that are relevant for others
  sum = 0.502627, mean = 0.0157071, min = 0.0117848, max = 0.0275871
All gather the vector sizes.
  sum = 0.02389, mean = 0.000746564, min = 5.1736e-05, max = 0.00131211
Reserve memory.
  sum = 0.0945755, mean = 0.00295548, min = 1.0772e-05, max = 0.00372125
Initiate communication.
  sum = 0.197638, mean = 0.00617619, min = 0.00470093, max = 0.0112815
Wait and then unserialize the cells.
  sum = 0.630392, mean = 0.0196997, min = 0.00530035, max = 0.0615463
Wait and then swap buffers.
  sum = 3.65238, mean = 0.114137, min = 0.0657253, max = 0.131012
Total time
  sum = 7.28197, mean = 0.227562, min = 0.223304, max = 0.231822

:
MPI processes = 64
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 8.79317e+07, mean = 1.37393e+06, min = 1.23929e+06, max = 1.51826e+06
Total time
  sum = 92.0251, mean = 1.43789, min = 1.42866, max = 1.44958

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 9.2541e+06, mean = 144595, min = 143925, max = 145284
Maximum objects per cell = 1024
Make a vector of coarsened query windows
  sum = 10.9318, mean = 0.17081, min = 0.161176, max = 0.178173
Build the description of the distributed objects
  sum = 33.1965, mean = 0.518695, min = 0.4936, max = 0.575299
Calculate relevant cells
  sum = 10.0638, mean = 0.157247, min = 0.151618, max = 0.162318
Gather the relevant objects
  sum = 26.7164, mean = 0.417444, min = 0.408401, max = 0.431275
Total time
  sum = 80.9779, mean = 1.26528, min = 1.25304, max = 1.31329

sfc::gatherRelevantRing():
Number of distributed cells = 262144
Distributed cell storage = 1.04858e+07
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 343000, mean = 5359.38, min = 4913, max = 5832
Number of object cells
  sum = 287392, mean = 4490.5, min = 4096, max = 4910
Number of object cells that are relevant for others
  sum = 92280, mean = 1441.88, min = 1352, max = 1535
Storage for local objects
  sum = 2.75925e+08, mean = 4.31133e+06, min = 3.19715e+06, max = 5.36773e+06
Storage for relevant objects
  sum = 1.40691e+09, mean = 2.19829e+07, min = 1.98286e+07, max = 2.42921e+07
Storage for send buffers = 5.39232e+06
Storage for receive buffers = 5.39232e+06
Count the number of relevant objects
  sum = 0.435892, mean = 0.00681081, min = 0.00470717, max = 0.00829189
Build object cells
  sum = 3.33471, mean = 0.0521048, min = 0.0510464, max = 0.0527583
Record the relevant objects.
  sum = 0.592727, mean = 0.00926136, min = 0.00558192, max = 0.011776
Calculate cells that are relevant for others
  sum = 1.78218, mean = 0.0278466, min = 0.0213794, max = 0.0497769
All gather the vector sizes.
  sum = 0.0432258, mean = 0.000675403, min = 5.7382e-05, max = 0.00139753
Reserve memory.
  sum = 0.254534, mean = 0.00397709, min = 1.0528e-05, max = 0.00455689
Initiate communication.
  sum = 0.730457, mean = 0.0114134, min = 0.00907875, max = 0.0174486
Wait and then unserialize the cells.
  sum = 2.86626, mean = 0.0447854, min = 0.00610174, max = 0.157258
Wait and then swap buffers.
  sum = 16.6204, mean = 0.259693, min = 0.145676, max = 0.296948
Total time
  sum = 26.716, mean = 0.417437, min = 0.40839, max = 0.431266

:
MPI processes = 128
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 1.99151e+08, mean = 1.55587e+06, min = 1.33368e+06, max = 1.7174e+06
Total time
  sum = 279.579, mean = 2.18421, min = 2.15939, max = 2.21922

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 1.85021e+07, mean = 144548, min = 143634, max = 145297
Maximum objects per cell = 2048
Make a vector of coarsened query windows
  sum = 21.4622, mean = 0.167674, min = 0.152876, max = 0.176285
Build the description of the distributed objects
  sum = 82.4308, mean = 0.643991, min = 0.61256, max = 0.706309
Calculate relevant cells
  sum = 23.4328, mean = 0.183069, min = 0.173911, max = 0.188163
Gather the relevant objects
  sum = 125.129, mean = 0.977574, min = 0.961412, max = 1.00481
Total time
  sum = 252.589, mean = 1.97335, min = 1.9455, max = 2.0275

sfc::gatherRelevantRing():
Number of distributed cells = 319986
Distributed cell storage = 1.27995e+07
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 440233, mean = 3439.32, min = 2488, max = 4262
Number of object cells
  sum = 360582, mean = 2817.05, min = 2475, max = 3134
Number of object cells that are relevant for others
  sum = 136029, mean = 1062.73, min = 906, max = 1219
Storage for local objects
  sum = 7.69869e+08, mean = 6.0146e+06, min = 4.2911e+06, max = 6.99798e+06
Storage for relevant objects
  sum = 3.18642e+09, mean = 2.48939e+07, min = 2.13389e+07, max = 2.74784e+07
Storage for send buffers = 7.01752e+06
Storage for receive buffers = 7.01752e+06
Count the number of relevant objects
  sum = 0.985603, mean = 0.00770002, min = 0.00543302, max = 0.0089081
Build object cells
  sum = 6.67378, mean = 0.0521389, min = 0.0507771, max = 0.0532228
Record the relevant objects.
  sum = 1.33881, mean = 0.0104595, min = 0.00641529, max = 0.0128597
Calculate cells that are relevant for others
  sum = 2.80979, mean = 0.0219515, min = 0.014079, max = 0.0521046
All gather the vector sizes.
  sum = 0.126314, mean = 0.000986828, min = 6.9394e-05, max = 0.00214588
Reserve memory.
  sum = 0.701739, mean = 0.00548234, min = 1.0964e-05, max = 0.00625825
Initiate communication.
  sum = 1.96919, mean = 0.0153843, min = 0.0125474, max = 0.0331084
Wait and then unserialize the cells.
  sum = 15.0728, mean = 0.117756, min = 0.00367338, max = 0.450873
Wait and then swap buffers.
  sum = 95.2506, mean = 0.744145, min = 0.414179, max = 0.856982
Total time
  sum = 125.129, mean = 0.977567, min = 0.961405, max = 1.0048

:
MPI processes = 256
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 4.74182e+08, mean = 1.85227e+06, min = 1.46735e+06, max = 2.06806e+06
Total time
  sum = 976.488, mean = 3.8144, min = 3.72806, max = 3.92857

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 3.70056e+07, mean = 144553, min = 143641, max = 145503
Maximum objects per cell = 4096
Make a vector of coarsened query windows
  sum = 43.0349, mean = 0.168105, min = 0.157634, max = 0.176378
Build the description of the distributed objects
  sum = 171.326, mean = 0.669241, min = 0.637959, max = 0.719998
Calculate relevant cells
  sum = 22.5597, mean = 0.0881238, min = 0.0830595, max = 0.0943772
Gather the relevant objects
  sum = 662.672, mean = 2.58856, min = 2.5499, max = 2.60801
Total time
  sum = 899.729, mean = 3.51457, min = 3.45271, max = 3.56182

sfc::gatherRelevantRing():
Number of distributed cells = 131072
Distributed cell storage = 5.24292e+06
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 231190, mean = 903.086, min = 729, max = 1000
Number of object cells
  sum = 166209, mean = 649.254, min = 512, max = 729
Number of object cells that are relevant for others
  sum = 101209, mean = 395.348, min = 296, max = 475
Storage for local objects
  sum = 2.17001e+09, mean = 8.47661e+06, min = 5.79312e+06, max = 9.6588e+06
Storage for relevant objects
  sum = 7.58691e+09, mean = 2.96364e+07, min = 2.34776e+07, max = 3.3089e+07
Storage for send buffers = 9.66643e+06
Storage for receive buffers = 9.66643e+06
Count the number of relevant objects
  sum = 2.54796, mean = 0.00995298, min = 0.00603785, max = 0.0116016
Build object cells
  sum = 13.3669, mean = 0.0522144, min = 0.050809, max = 0.0535919
Record the relevant objects.
  sum = 3.51434, mean = 0.0137279, min = 0.00715099, max = 0.0172871
Calculate cells that are relevant for others
  sum = 11.2376, mean = 0.0438967, min = 0.0379169, max = 0.0587686
All gather the vector sizes.
  sum = 0.91127, mean = 0.00355965, min = 8.2324e-05, max = 0.00785655
Reserve memory.
  sum = 1.90302, mean = 0.00743367, min = 9.869e-06, max = 0.00851082
Initiate communication.
  sum = 9.57691, mean = 0.0374098, min = 0.0309708, max = 0.0599893
Wait and then unserialize the cells.
  sum = 75.6209, mean = 0.295394, min = 0.00987263, max = 1.61505
Wait and then swap buffers.
  sum = 543.121, mean = 2.12156, min = 0.814657, max = 2.41783
Total time
  sum = 662.671, mean = 2.58856, min = 2.54989, max = 2.608

:
MPI processes = 512
Ball radius = 0.01
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 9.73363e+08, mean = 1.9011e+06, min = 1.45995e+06, max = 2.06807e+06
Total time
  sum = 3556.84, mean = 6.94696, min = 6.86189, max = 7.08226

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 7.40183e+07, mean = 144567, min = 143334, max = 145503
Maximum objects per cell = 8192
Make a vector of coarsened query windows
  sum = 88.7558, mean = 0.173351, min = 0.166083, max = 0.180394
Build the description of the distributed objects
  sum = 531.067, mean = 1.03724, min = 1.00777, max = 1.06999
Calculate relevant cells
  sum = 72.3292, mean = 0.141268, min = 0.131918, max = 0.145957
Gather the relevant objects
  sum = 2688.98, mean = 5.25192, min = 5.19367, max = 5.30101
Total time
  sum = 3381.52, mean = 6.60453, min = 6.53217, max = 6.6594

sfc::gatherRelevantRing():
Number of distributed cells = 262144
Distributed cell storage = 1.04858e+07
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 474551, mean = 926.857, min = 729, max = 1000
Number of object cells
  sum = 342330, mean = 668.613, min = 512, max = 729
Number of object cells that are relevant for others
  sum = 217330, mean = 424.473, min = 296, max = 512
Storage for local objects
  sum = 4.50382e+09, mean = 8.79652e+06, min = 5.86328e+06, max = 9.66147e+06
Storage for relevant objects
  sum = 1.55738e+10, mean = 3.04176e+07, min = 2.33592e+07, max = 3.30891e+07
Storage for send buffers = 9.6697e+06
Storage for receive buffers = 9.6697e+06
Count the number of relevant objects
  sum = 5.12395, mean = 0.0100077, min = 0.00580373, max = 0.0116429
Build object cells
  sum = 26.7653, mean = 0.052276, min = 0.0507467, max = 0.0540062
Record the relevant objects.
  sum = 7.33629, mean = 0.0143287, min = 0.00786482, max = 0.0193461
Calculate cells that are relevant for others
  sum = 27.2206, mean = 0.0531652, min = 0.037641, max = 0.0832098
All gather the vector sizes.
  sum = 4.20419, mean = 0.0082113, min = 7.5296e-05, max = 0.01646
Reserve memory.
  sum = 3.75237, mean = 0.00732884, min = 1.0861e-05, max = 0.00814678
Initiate communication.
  sum = 36.5099, mean = 0.0713083, min = 0.0590416, max = 0.107454
Wait and then unserialize the cells.
  sum = 342.701, mean = 0.669338, min = 0.0062336, max = 3.48682
Wait and then swap buffers.
  sum = 2231.68, mean = 4.35875, min = 1.56481, max = 5.02248
Total time
  sum = 2688.98, mean = 5.25191, min = 5.19367, max = 5.301
