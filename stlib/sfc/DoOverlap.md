# Overlap Tests for Bounding Boxes

# Introduction

Consider a set of objects, *A*. For an object *b*, we may want to determine 
if any of the objects in *A* overlaps it. Depending on the geometry of the 
objects, this test may be expensive or difficult to implement. However, 
there are efficient methods for solving a related problem. If we replace 
all of the objects with their tight, axis-aligned bounding boxes, then 
testing for overlaps is fairly easy. Also, working with boxes provides 
a partial solution to the original problem, namely, a bounding box overlap is
necessary, but not sufficient, for an object overlap.

There are many applications in which overlap tests with bounding boxes provide
useful information. Consider a simulation which has a background mesh and
an overset mesh. Assume that both are polyhedral meshes. 
These meshes may overlap. Where they do, we must subtract
the intersection volume from the background mesh cells so that we do not
perform double-counting with volume integrals. Compared to testing for
bounding box overlaps, calculating the intersection volume between 
cells is very expensive. Thus, it is best to first identify the cells that
may overlap, and then perform the intersection calculations with these
subsets. Overlap tests with bounding boxes are exactly what we need for this.
We first identify the background cells that may overlap overset cells.
For the purposes of the intersection calculation, we then discard
those that do not. Next we identify the overset sells that potentially
overlap the remaining background cells. Again we would discard those that
do not. Now we have the subsets of the background cells and the overset cells
that may overlap.

# Algorithm

To efficiently perform the overlap test, we store the object bounding boxes
in a linear orthant trie. The cell type is a bounding box. Thus, we can
determine if a query box overlaps any object box by performing a depth-first
search of the trie. We initialize the stack with the root cell. 
When we process a cell, we first check its bounding 
box. We proceed only if it overlaps the query box. If the cell is internal,
we then add each of the children to the stack. For leaf cells, we perform
an overlap test with each of the object boxes. We stop the search as soon as
we find an overlap. If we complete the DFS, then there is no overlap.

The above serial algorithm is simple to implement. There are two opportunities
for using intra-register vectorization to optimize it. Firstly, for each 
leaf cell, we can store the object bounding boxes in a 
structure-of-arrays format that permits efficient overlap tests with
SIMD instructions. Obviously, the data for each leaf cell must be 
appropriately aligned. Consider using AVX with single-precision 
floating-point numbers, for example. In that case, one can perform overlap tests
with eight objects bounding boxes at a time.

The second opportunity for vectorization is in the calculations performed
on internal cells. In the serial algorithm, if the cell bounding box
overlaps the query box we add all of the children to the stack. Instead
of adding them to the stack to process later, we can perform overlap tests
for all the children of a cell. Then we only add children whose boxes overlap
the query box. For each internal cell, we store the child bounding boxes
in aligned, shuffled storage that permits efficient SIMD operations.

# Usage

There are a number of functions for performing 
\ref sfcDoOverlap "overlap tests".
The simplest interface for performing overlap tests is with the function
stlib::sfc::doOverlap(std::vector<geom::BBox<_T, _D> >&& objectBoxes, std::vector<geom::BBox<_T, _D> > const& queryBoxes). For each query box, it performs an
overlap test with the object bounding boxes. It returns the result in a 
\c std::vector<bool>. Below is an example in which the objects are balls.

\code
constexpr std::size_t Dimension = 3;
typedef float Float;
typedef stlib::geom::Ball<Float, Dimension> Object;
typedef stlib::geom::BBox<Float, Dimension> BBox;

std::vector<Object> objects;
std::vector<BBox> queryBoxes;
// Initialize the objects and query boxes.
...

std::vector<bool> const doOverlap =
  stlib::sfc::doOverlap(stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end()), queryBoxes);
\endcode

There are also interfaces that may be useful when you have
already built a linear orthant trie for the objects. You might use these
interfaces if you are performing multiple kinds of queries with the objects.

To perform overlap tests one at a time, use the stlib::sfc::DoOverlap functor.
Below we show how to use this alternative.

\code
typedef stlib::sfc::Traits<Dimension, Float> Traits;

// Construct the functor.
stlib::sfc::DoOverlap<Traits> doOverlap(stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end()));
// We need a vector of std::size_t for scratch data.
std::vector<std::size_t> stack;
// For each query box, check if it overlaps the object bounding boxes.
for (BBox const& queryBox: queryBoxes) {
  if (doOverlap(queryBox, &stack)) {
    ...
  }
}
\endcode

When you specify the object bounding boxes, they must be passed as an 
rvalue reference. This is because the caller should think about reusing
the storage for them. Most often, the object boxes will be built when
one wants to perform the overlap query and not used afterwards. It would
be wasteful to copy the vector of object bounding boxes in this case.
It is better to pass an rvalue and use them as a pure input. If one needs
the vector of object bounding boxes after performing the overlap queries,
then one must explicitly copy it. Below we construct the vector of object
bounding boxes before calling \c stlib::sfc::doOverlap(). Thus we must
use \c std::move() to obtain an rvalue reference for them.

\code
std::vector<BBox> objectBoxes = stlib::geom::bboxForEach<BBox>(objects.begin(), objects.end());
std::vector<bool> const doOverlap = stlib::sfc::doOverlap(std::move(objectBoxes), queryBoxes);
\endcode

# Performance

To measure the performance of the overlap tests we use the ball intersection
example in \c test/performance/sfc/mpi/ballIntersection.cc. We run the 
test on an Intel(R) Xeon(R) CPU E5-2687W v2 @ 3.40GHz, using 16 MPI processes.
We run all of the tests with the rotated geometry (\c -t option). For
this test, processes have between 1.19 million and 1.26 million object
boxes. The number of query boxes is 1.05 million. The total time for 
performing the overlap tests is 1.07 seconds. Note that the query boxes
are just bounding boxes for the balls. The balls are generated by randomly
distributing them in a unit cube. Thus, there is no correlation between
the query box index and its (centroid) position. If there were a correlation,
the performance of the overlap tests would be improved due to better 
cache utilization. Specifically, there would be fewer cache misses when 
accessing the bounding boxes in the linear orthant trie. If we sort the
query boxes by their z coordinate, then the cost of performing the overlap
tests is reduced to 0.63 seconds. Since sorting the query boxes takes 
0.09 seconds, the total cost for the queries is 0.72 seconds. If we 
sort the query boxes using a space-filling curve, then the overlap tests
take 0.51 seconds. Since sorting with the SFC takes 0.16 seconds, the 
total time for the overlap tests is 0.67 seconds.

From this example we see that locallity of reference plays a fairly 
important role in the performance of the overlap tests. Going from queries 
that have no correlation between order and location to queries that are highly 
correlated reduced the overlap test time by a factor of two. However, 
one must also consider the cost the queries. If the queries have some 
correlation between order and location, then it is probably best to use
the given order. If the queries have no such correlation, then it is 
probably best to order them with an SFC before conducting the overlap 
tests. Doing so will modestly improve the performance.
