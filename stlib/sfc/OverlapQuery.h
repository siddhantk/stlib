// -*- C++ -*-

#if !defined(__sfc_OverlapQuery_h__)
#define __sfc_OverlapQuery_h__

/*!
  \file
  \brief Overlap queries for objects that are organized with a linear orthant trie.
*/

#include "stlib/container/PackedArrayOfArrays.h"
#include "stlib/sfc/LinearOrthantTrieTopology.h"
#include "stlib/performance/Performance.h"

namespace stlib
{
namespace sfc
{


/// Functor that performs an overlap query for a set of object bounding boxes.
template<typename _Traits>
class OverlapQuery
{
public:

  /// The space dimension.
  BOOST_STATIC_CONSTEXPR std::size_t Dimension = _Traits::Dimension;
  /// A bounding box.
  typedef typename _Traits::BBox BBox;
  
  /// Construct from the object boxes.
  OverlapQuery(std::vector<BBox>&& objectBoxes);

  /// Record the object box indices that overlap the query box.
  /**
     \param box The bounding box that we test for overlaps.
     \param output The indices of the overlapping boxes are recorded in this
     output iterator.
     \param stack The stack is used as scratch data. We pass this as a parameter
     for the sake of efficiency. Note that we wouldn't want the stack to be
     member data because then the function wouldn't be thread-safe.
  */
  template<typename _OutputIterator>
  void
  operator()(BBox const& queryBox, _OutputIterator output,
             std::vector<std::size_t>* stack) const;
  
private:

  typedef LinearOrthantTrie<_Traits, BBox, true> Trie;

  std::vector<BBox> _objectBoxes;
  Trie _trie;
  LinearOrthantTrieTopology<Dimension> _topology;
};


/** \defgroup sfcOverlapQuery Overlap Queries
    @{
*/

/// For each query box, record the object boxes that overlap it.
/**
   \param objectBoxes The vector of object boxes.
   \param queryBoxes The vector of query boxes.
   \return A packed array of arrays that record the overlapping object indices
   for each query box.
*/
template<typename _T, std::size_t _D>
container::PackedArrayOfArrays<std::size_t>
overlapQuery(std::vector<geom::BBox<_T, _D> >&& objectBoxes,
             std::vector<geom::BBox<_T, _D> > const& queryBoxes);


/// Record the object box indices that overlap the query box.
/**
   \param trie The linear orthant trie for the objects. The cells are 
   bounding boxes.
   \param topology Cached topology for the linear orthant trie.
   \param objectBoxes Bounding boxes for the objects. These have been organized
   by the linear orthant trie.
   \param stack The stack is used as scratch data. We pass this as a parameter
   for the sake of efficiency. Note that we wouldn't want the stack to be
   static data for the function because then the function wouldn't be 
   thread-safe.
   \param queryBox The bounding box that we test for overlaps.
   \param output The indices of the overlapping boxes are recorded in this
   output iterator.
*/
template<typename _Traits, typename _OutputIterator>
void
overlapQuery
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, true> const& trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<geom::BBox<typename _Traits::Float, _Traits::Dimension> > const&
 objectBoxes,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& queryBox,
 _OutputIterator output);


/** @} */ // End of sfcOverlapQuery group.


} // namespace sfc
} // namespace stlib

#define __sfc_OverlapQuery_tcc__
#include "stlib/sfc/OverlapQuery.tcc"
#undef __sfc_OverlapQuery_tcc__

#endif
