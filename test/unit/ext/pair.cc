// -*- C++ -*-

#include "stlib/ext/pair.h"

#include <sstream>

#include <cassert>

using namespace stlib;

int
main()
{
  {
    std::pair<int, double> a(-1, 3);
    std::ostringstream out;
    out << a;
    std::pair<int, double> b;
    std::istringstream in(out.str());
    in >> b;
    assert(a == b);
  }

  return 0;
}
