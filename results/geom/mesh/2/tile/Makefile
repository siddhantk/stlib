# -*- Makefile -*-

# Using tiling and cookie-cutter meshing to mesh a boundary.

DATA_PATH = ../../../../../data/geom/mesh/21
PY_PATH = ../../../../../data/geom/mesh/utilities
BIN_PATH = ../../../../../examples/geom/mesh

TARGETS = boundary.vtu mesh.vtu mesh_s.vtu mesh_sm.vtu \
  boundary.eps mesh.eps mesh_s.eps mesh_sm.eps

GNUPLOT = wgnuplot.exe

.SUFFIXES:
.SUFFIXES: .txt .vtu .dat .eps

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu *.dat *.eps

again: 
	$(MAKE) distclean 
	$(MAKE) 

#
# Implicit rules.
#

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk22.exe $< $@

# Convert the ascii mesh file to a gnuplot file.
.txt.dat:
	python.exe $(PY_PATH)/txt2gp2.py $< $@

# Make an eps from the gnuplot file.
.dat.eps:
	$(GNUPLOT) $(basename $<).gnu

#
# Explicit rules.
#

# The boundary of the letter 'A'.
boundary.txt:
	cp $(DATA_PATH)/a.txt boundary.txt

# We need an explicit rule for the 1-D manifold.
boundary.vtu: boundary.txt
	$(BIN_PATH)/iss2vtk21.exe $< $@

# We need an explicit rule for the 1-D manifold.
boundary.dat: boundary.txt
	python.exe $(PY_PATH)/txt2gp21.py $< $@

# Tile with an edge length of 0.04.  The centroid of each cell lies inside 
# the boundary.
mesh.txt: boundary.txt
	$(BIN_PATH)/tile2.exe -l 0.04 boundary.txt mesh.txt

# Tile and remove the cells with low adjacencies.
mesh_s.txt: boundary.txt
	$(BIN_PATH)/tile2.exe -s -l 0.04 boundary.txt mesh_s.txt

# Tile, remove the cells with low adjacencies, and move the boundary vertices
# to lie on the boundary mesh.
mesh_sm.txt: boundary.txt
	$(BIN_PATH)/tile2.exe -s -m -l 0.04 boundary.txt mesh_sm.txt

# End of file.