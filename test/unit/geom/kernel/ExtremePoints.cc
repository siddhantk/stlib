// -*- C++ -*-

#include "stlib/geom/kernel/ExtremePoints.h"

#include <vector>

using namespace stlib;

using namespace geom;

int
main()
{
  {
    const std::size_t D = 2;
    typedef double Float;
    typedef geom::ExtremePoints<Float, D> ExtremePoints;
    typedef ExtremePoints::Point Point;
    typedef geom::BBox<Float, D> BBox;
    {
      ExtremePoints x = ExtremePoints::makeEmpty();
      assert(x.isEmpty());
    }
    {
      const ExtremePoints x = {{{{{{{0, 0}}, {{1, 0}}}},
                                 {{{{0, 0}}, {{0, 1}}}}}}};
      BBox box;
      x.bbox(&box);
      assert(box == (BBox{{{0, 0}}, {{1, 1}}}));
    }
    {
      std::array<Point, 1> points = {{{{2, 3}}}};
      ExtremePoints x;
      x.bound(points.begin(), points.end());
      assert(x.points[0][0] == points[0]);
      assert(x.points[0][1] == points[0]);
      assert(x.points[1][0] == points[0]);
      assert(x.points[1][1] == points[0]);
    }
    {
      std::array<Point, 2> points = {{{{2, 3}}, {{5, 7}}}};
      ExtremePoints x;
      x.bound(points.begin(), points.end());
      assert(x.points[0][0] == points[0]);
      assert(x.points[0][1] == points[1]);
      assert(x.points[1][0] == points[0]);
      assert(x.points[1][1] == points[1]);
    }
    {
      std::array<Point, 2> points = {{{{5, 7}}, {{2, 3}}}};
      ExtremePoints x;
      x.bound(points.begin(), points.end());
      assert(x.points[0][0] == points[1]);
      assert(x.points[0][1] == points[0]);
      assert(x.points[1][0] == points[1]);
      assert(x.points[1][1] == points[0]);
    }
    {
      std::array<Point, 2> points = {{{{2, 7}}, {{5, 3}}}};
      ExtremePoints x;
      x.bound(points.begin(), points.end());
      assert(x.points[0][0] == points[0]);
      assert(x.points[0][1] == points[1]);
      assert(x.points[1][0] == points[1]);
      assert(x.points[1][1] == points[0]);
    }

    {
      std::array<Point, 1> px = {{{{2, 3}}}};
      ExtremePoints x;
      x.bound(px.begin(), px.end());
      std::array<Point, 1> py = {{{{5, 7}}}};
      ExtremePoints y;
      y.bound(py.begin(), py.end());
      assert(x == x);
      assert(x != y);
      x += y;
      assert(x.points[0][0] == px[0]);
      assert(x.points[0][1] == py[0]);
      assert(x.points[1][0] == px[0]);
      assert(x.points[1][1] == py[0]);
    }
  }

  return 0;
}
