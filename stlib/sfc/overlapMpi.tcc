// -*- C++ -*-

#if !defined(__sfc_overlapMpi_tcc__)
#error This file is an implementation detail of overlap.
#endif

namespace stlib
{
namespace sfc
{


template<typename _Traits, bool _StoreDel>
inline
void
markOverlappingLeaves
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, _StoreDel> const&
 trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& window,
 std::vector<bool>* areOverlapping)
{
  typedef Siblings<_Traits::Dimension> Siblings;

  // Initialize the stack of cells.
  stack->clear();
  if (! trie.empty()) {
    stack->push_back(0);
  }
  // Traverse the trie.
  while (! stack->empty()) {
    // Get the current cell.
    std::size_t const cell = stack->back();
    stack->pop_back();
    // If the bounding box for this cell overlaps the window.
    if (doOverlap(trie[cell], window)) {
      if (topology.isInternal(cell)) {
        // Add the children to the stack.
        Siblings const& children = topology.children(cell);
        stack->insert(stack->end(), children.rbegin(), children.rend());
      }
      else {
        // Mark the leaf as overlapping.
        (*areOverlapping)[topology.leafRank(cell)] = true;
      }
    }
  }
}


/**
   \return The cell indices that overlap the query windows.
*/
template<typename _Traits, bool _StoreDel>
inline
std::vector<std::size_t>
overlappingCells
(CellsMultiLevel<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, _StoreDel> const&
 cells,
 std::vector<geom::BBox<typename _Traits::Float, _Traits::Dimension> > const&
 queryWindows)
{
  typedef LinearOrthantTrie<_Traits,
    geom::BBox<typename _Traits::Float, _Traits::Dimension>, _StoreDel> Trie;

  // Make a linear orthant trie that we can search.
  Trie const trie(cells);
  LinearOrthantTrieTopology<_Traits::Dimension> const topology(trie);

  // Mark the cells that overlap the query windows.
  std::vector<std::size_t> stack;
  std::vector<bool> areOverlapping(cells.size(), false);
  for (auto const& window: queryWindows) {
    markOverlappingLeaves(trie, topology, &stack, window, &areOverlapping);
  }

  // Convert from a bit array to a vector of indices.
  return stlib::numerical::convertBitVectorToIndices<std::size_t>
    (areOverlapping);
}


template<typename _Float, std::size_t _Dimension, typename _Object>
inline
std::vector<_Object>
gatherOverlapping(
  std::vector<geom::BBox<_Float, _Dimension> >* queryWindows,
  std::vector<_Object>* objects,
  MPI_Comm const comm,
  std::size_t const maxQueryWindowsPerCell)
{
  typedef Traits<_Dimension, _Float> Traits;
  typedef typename Traits::BBox BBox;
  using performance::start;
  using performance::stop;
  using performance::record;

  performance::Scope _("sfc::gatherOverlapping()");
  record("Number of query windows", queryWindows->size());
  record("Number of objects", objects->size());
  record("maxQueryWindowsPerCell", maxQueryWindowsPerCell);

  start("Make a vector of coarsened query windows");
  // Make a vector of coarsened query windows. We do this because performing
  // a query for every window could be expensive. Additionally, the 
  // windows in the coarsened vector will be ordered with a SFC to improve
  // locality of reference when performing the queries.
  std::vector<BBox> cellQueryWindows;
  // Note that we check for the trivial case of no query windows.
  if (! queryWindows->empty()) {
    // Make an adaptive cell data structure for the query windows. We store
    // a bounding box for each cell. There is no need to store object 
    // delimiters.
    CellsMultiLevel<Traits, BBox, false>
      queryCells(geom::bbox<BBox>(queryWindows->begin(), queryWindows->end()));
    // Note that building the cells reorders the query windows.
    queryCells.buildCells(queryWindows, maxQueryWindowsPerCell);
    cellQueryWindows.resize(queryCells.size());
    for (std::size_t i = 0; i != cellQueryWindows.size(); ++i) {
      cellQueryWindows[i] = queryCells[i];
    }
  }
  stop();
  record("Number of cell query windows", cellQueryWindows.size());

  start("Build the description of the distributed objects");
  // Build the description of the distributed objects.
  typedef CellsMultiLevel<Traits, BBox, true> GlobalCells;
  // Bound the distributed objects.
  BBox const objectDomain =
    mpi::allReduce(geom::bbox<BBox>(objects->begin(), objects->end()), comm);
  // Handle the trivial case that there are no objects.
  if (isEmpty(objectDomain)) {
    stop();
    return std::vector<_Object>{};
  }
  // Define the virtual grid.
  typename GlobalCells::Grid const grid(objectDomain);
  // Determine an appropriate maximum number of objects per cell.
  // CONTINUE: Study the argument values for large process counts.
  std::size_t const objectsPerCell =
    maxObjectsPerCell<Traits>(objects->size(), comm);
  // Build the bounding box cell data structure. Note that this 
  // reorders the objects.
  GlobalCells const globalCells =
    cellsMultiLevel<GlobalCells>(grid, objects, objectsPerCell, comm);
  stop();
  record("Maximum objects per cell", objectsPerCell);

  start("Calculate relevant cells");
  // Determine the cell indices in globalCells that are relevant for 
  // this process.
  std::vector<std::size_t> const relevantCells =
    overlappingCells(globalCells, cellQueryWindows);
  stop();

  performance::Event event("Gather the relevant objects");
  // Gather the relevant objects.
  return gatherRelevant(objects, globalCells, relevantCells, comm);
}


template<typename _Float, std::size_t _Dimension, typename _Object>
inline
std::vector<_Object>
gatherOverlappingBBox(
  std::vector<geom::BBox<_Float, _Dimension> > const& queryWindows,
  std::vector<_Object> const& objects,
  MPI_Comm const comm)
{
  typedef Traits<_Dimension, _Float> Traits;
  typedef typename Traits::BBox BBox;
  using performance::start;
  using performance::stop;
  using performance::record;

  performance::Scope _("sfc::gatherOverlappingBBox()");
  record("Number of query windows", queryWindows.size());
  record("Number of objects", objects.size());

  int const commSize = mpi::commSize(comm);
  int const commRank = mpi::commRank(comm);

  start("Build bounding boxes");
  // Build bounding boxes around each of the objects. We do this for the sake
  // of efficiency. This way we only need to build them once.
  std::vector<BBox> const objectBoxes =
    geom::bboxForEach<BBox>(objects.begin(), objects.end());

  // Build bounding boxes around the query windows and objects in this process.
  BBox const queryWindowProcessBox =
    geom::bbox<BBox>(queryWindows.begin(), queryWindows.end());
  BBox const objectProcessBox =
    geom::bbox<BBox>(objectBoxes.begin(), objectBoxes.end());
  stop();
#ifdef STLIB_PERFORMANCE
  record("Process query window BBox volume", content(queryWindowProcessBox));
  record("Process object BBox volume", content(objectProcessBox));
#endif

  start("Record the local objects");
  // Record the local objects that overlap the query windows.
  std::vector<_Object> relevantObjects;
  // First use a simple check to see of any overlaps are possible.
  if (doOverlap(objectProcessBox, queryWindowProcessBox)) {
    for (std::size_t i = 0; i != objects.size(); ++i) {
      if (doOverlap(objectBoxes[i], queryWindowProcessBox)) {
        relevantObjects.push_back(objects[i]);
      }
    }
  }
  stop();

  start("All-gather the bounding box information");
  // All-gather the bounding box information. For the sake of efficiency,
  // use one all-gather operation.
  std::vector<BBox> queryWindowProcessBoxes(commSize);
  std::vector<BBox> objectProcessBoxes(commSize);
  {
    std::vector<std::pair<BBox, BBox> > boxes =
      mpi::allGather(std::pair<BBox, BBox>{queryWindowProcessBox,
            objectProcessBox}, comm);
    for (std::size_t i = 0; i != boxes.size(); ++i) {
      queryWindowProcessBoxes[i] = boxes[i].first;
      objectProcessBoxes[i] = boxes[i].second;
    }
  }
  // We have already recorded the local relevant objects. To simplify 
  // subsequent logic, we set the bounding boxes for this process to be empty.
  queryWindowProcessBoxes[commRank] = geom::bbox<BBox>();
  objectProcessBoxes[commRank] = geom::bbox<BBox>();
  stop();

  start("Initiate sends");
  // Send our objects to the appropriate processes.
  std::vector<MPI_Request> sendRequests;
  std::vector<std::unique_ptr<std::vector<_Object> > > sendBuffers;
  // We use this as a scratch buffer for both send and receiving objects.
  std::vector<_Object> buffer;
  for (int rank = 0; rank != commSize; ++rank) {
    // If the bounding box for the local objects intersects the bounding box
    // for the query windows from process i.
    if (doOverlap(objectProcessBox, queryWindowProcessBoxes[rank])) {
      buffer.clear();
      for (std::size_t i = 0; i != objects.size(); ++i) {
        if (doOverlap(objectBoxes[i], queryWindowProcessBoxes[rank])) {
          buffer.push_back(objects[i]);
        }
      }
      // Create a send buffer that holds the objects which overlap the
      // bounding box for the query windows on process rank.
      sendBuffers.push_back(std::unique_ptr<std::vector<_Object> >
                            (new std::vector<_Object>(buffer)));
      // Send the objects to process rank. Note that the sequence of objects
      // may be empty. In this case we still must send the empty container
      // because process rank will still expect a message.
      sendRequests.push_back(mpi::iSend(*sendBuffers.back(), rank, 0, comm));
    }
  }
  stop();
#ifdef STLIB_PERFORMANCE
  {
    std::size_t storage = 0;
    for (auto const& buffer: sendBuffers) {
      storage += buffer->size() * sizeof(_Object);
    }
    record("Storage for send buffers", storage);
  }
#endif

  start("Receive objects");
  // Determine how many messages we will receive.
  std::size_t numMessages = 0;
  for (int rank = 0; rank != commSize; ++rank) {
    if (doOverlap(objectProcessBoxes[rank], queryWindowProcessBox)) {
      ++numMessages;
    }
  }
#ifdef STLIB_PERFORMANCE
  std::size_t maxReceived = 0;
#endif
  // Receive objects from the appropriate processes.
  while (numMessages--) {
    // Receive objects from any source. Note that the buffer will be resized
    // as needed.
    mpi::recv(&buffer, MPI_ANY_SOURCE, 0, comm);
#ifdef STLIB_PERFORMANCE
    maxReceived = std::max(maxReceived, buffer.size());
#endif
    // Add these to the sequence of relevant objects.
    relevantObjects.insert(relevantObjects.end(), buffer.begin(), buffer.end());
  }
  stop();
#ifdef STLIB_PERFORMANCE
  record("Storage for receive buffers", maxReceived * sizeof(_Object));
#endif
  record("Storage for local objects", objects.size() * sizeof(_Object));
  record("Storage for relevant objects",
         relevantObjects.size() * sizeof(_Object));

  start("Wait for sends to complete");
  // Wait for all of the sends to complete.
  mpi::waitAll(&sendRequests);
  stop();

  // Return the relevant objects.
  return relevantObjects;
}


// Brute force for gathering relevant objects.
template<typename _Float, std::size_t _Dimension, typename _Object>
inline
std::vector<_Object>
gatherOverlappingBrute(
  std::vector<geom::BBox<_Float, _Dimension> >const& queryWindows,
  std::vector<_Object> const& objects,
  MPI_Comm const comm)
{
  typedef Traits<_Dimension, _Float> Traits;
  typedef typename Traits::BBox BBox;

  // Gather all of the objects to each process.
  std::vector<_Object> const allObjects = mpi::allGather(objects, comm);

  // Filter to collect the relevant objects.
  std::vector<_Object> relevantObjects;
  // Loop over the objects.
  for (auto const& object: allObjects) {
    // Construct a tight AABB around the object.
    BBox const box = geom::bbox<BBox>(object);
    // If the box overlaps any of the query windows, it is relevant.
    for (auto const& window: queryWindows) {
      if (doOverlap(box, window)) {
        relevantObjects.push_back(object);
        break;
      }
    }
  }

  return relevantObjects;
}


} // namespace sfc
} // namespace stlib
