// -*- C++ -*-

#if !defined(__geom_ExtremePoints_ipp__)
#error This file is an implementation detail of the class ExtremePoints.
#endif

namespace stlib
{
namespace geom
{


template<typename _T, std::size_t _D>
template<typename _Object>
inline
ExtremePoints<_T, _D>&
ExtremePoints<_T, _D>::
operator+=(const _Object& x)
{
  operator+=(extremePoints(x));
  return *this;
}


template<typename _T, std::size_t _D>
inline
ExtremePoints<_T, _D>&
ExtremePoints<_T, _D>::
operator+=(const Point& p)
{
  for (std::size_t i = 0; i != Dimension; ++i) {
    if (p[i] < points[i][0][i]) {
      points[i][0] = p;
    }
    if (p[i] > points[i][1][i]) {
      points[i][1] = p;
    }
  }
  return *this;
}


template<typename _T, std::size_t _D>
inline
ExtremePoints<_T, _D>&
ExtremePoints<_T, _D>::
operator+=(const ExtremePoints& other)
{
  for (std::size_t i = 0; i != Dimension; ++i) {
    if (other.points[i][0][i] < points[i][0][i]) {
      points[i][0] = other.points[i][0];
    }
    if (other.points[i][1][i] > points[i][1][i]) {
      points[i][1] = other.points[i][1];
    }
  }
  return *this;
}


template<typename _T, std::size_t _D>
template<typename _ForwardIterator>
inline
void
ExtremePoints<_T, _D>::
bound(_ForwardIterator begin, _ForwardIterator end)
{
  // Handle the trivial case.
  if (begin == end) {
    *this = makeEmpty();
    return;
  }
  // Bound the first object.
#if 0
  // For unknown reasons, the LLVM compiler generates seg faults when using
  // *begin++.
  *this = extremePoints(*begin++);
#else
  *this = extremePoints(*begin);
  ++begin;
#endif
  // Add the rest of the objects.
  while (begin != end) {
#if 0
    operator+=(*begin++);
#else
    operator+=(*begin);
    ++begin;
#endif
  }
}


//
// Builders
//


template<typename _T, std::size_t _D>
template<typename _T2, std::size_t _N>
inline
ExtremePoints<_T, _D>
ExtremePoints<_T, _D>::
extremePoints(std::array<std::array<_T2, _D>, _N> const& points)
{
  std::array<std::array<_T, _D>, _N> p;
  for (std::size_t i = 0; i != _N; ++i) {
    p[i] = ext::convert_array<_T>(points[i]);
  }
  return extremePoints(p);
}


template<typename _T, std::size_t _D>
template<std::size_t _N>
inline
ExtremePoints<_T, _D>
ExtremePoints<_T, _D>::
extremePoints(std::array<std::array<_T, _D>, _N> const& points)
{
  static_assert(_N != 0, "Error.");
  ExtremePoints<_T, _D> result = extremePoints(points[0]);
  for (std::size_t i = 1; i != points.size(); ++i) {
    result += points[i];
  }
  return result;
}


template<typename _T, std::size_t _D>
inline
ExtremePoints<_T, _D>
ExtremePoints<_T, _D>::
makeEmpty()
{
  ExtremePoints<_T, _D> x;
  for (std::size_t i = 0; i != Dimension; ++i) {
    for (std::size_t j = 0; j != Dimension; ++j) {
      x.points[i][0][j] = std::numeric_limits<Number>::quiet_NaN();
      x.points[i][1][j] = std::numeric_limits<Number>::quiet_NaN();
    }
  }
  return x;
}


template<typename _T, std::size_t _D>
inline
ExtremePoints<_T, _D>
ExtremePoints<_T, _D>::
_extremePoints(std::array<_T, _D> const& x)
{
  ExtremePoints<_T, _D> result;
  for (std::size_t i = 0; i != _D; ++i) {
    for (std::size_t j = 0; j != 2; ++j) {
      result.points[i][j] = x;
    }
  }
  return result;
}


} // namespace geom
} // namespace stlib
