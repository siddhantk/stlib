// -*- C++ -*-

#if !defined(__geom_BBox_ipp__)
#error This file is an implementation detail of the class BBox.
#endif

namespace stlib
{
namespace geom
{

//
// Member functions
//


template<typename _T, std::size_t _D>
inline
bool
isEmpty(BBox<_T, _D> const& box)
{
  for (std::size_t i = 0; i != _D; ++i) {
    if (box.lower[i] > box.upper[i]) {
      return true;
    }
  }
  return false;
}


template<typename _T, std::size_t _D>
inline
BBox<_T, _D>&
operator+=(BBox<_T, _D>& box, std::array<_T, _D> const& p)
{
  for (std::size_t i = 0; i != _D; ++i) {
    if (p[i] < box.lower[i]) {
      box.lower[i] = p[i];
    }
    if (p[i] > box.upper[i]) {
      box.upper[i] = p[i];
    }
  }
  return box;
}


template<typename _T, std::size_t _D>
inline
BBox<_T, _D>&
operator+=(BBox<_T, _D>& box, BBox<_T, _D> const& rhs)
{
  if (isEmpty(rhs)) {
    return box;
  }
  for (std::size_t i = 0; i != _D; ++i) {
    if (rhs.lower[i] < box.lower[i]) {
      box.lower[i] = rhs.lower[i];
    }
    if (rhs.upper[i] > box.upper[i]) {
      box.upper[i] = rhs.upper[i];
    }
  }
  return box;
}


template<typename _BBox>
inline
_BBox
_bbox(std::false_type /*hasInfinity*/)
{
  typedef typename _BBox::Point Point;
  typedef typename _BBox::Number Number;
  return _BBox{ext::filled_array<Point>
      (std::numeric_limits<Number>::max()),
      ext::filled_array<Point>
      (std::numeric_limits<Number>::min())};
}


template<typename _BBox>
inline
_BBox
_bbox(std::true_type /*hasInfinity*/)
{
  typedef typename _BBox::Point Point;
  typedef typename _BBox::Number Number;
  return _BBox{ext::filled_array<Point>
      (std::numeric_limits<Number>::infinity()),
      -ext::filled_array<Point>
      (std::numeric_limits<Number>::infinity())};
}


template<typename _BBox>
inline
_BBox
bbox()
{
  return _bbox<_BBox>(std::integral_constant<bool,
                      std::numeric_limits<typename _BBox::Number>::
                      has_infinity>{});
}


template<typename _BBox, typename _InputIterator>
inline
_BBox
bbox(_InputIterator begin, _InputIterator end)
{
  // If there are no objects, make the box empty.
  if (begin == end) {
    return bbox<_BBox>();
  }
  // Bound the first object.
  _BBox result = bbox<_BBox>(*begin++);
  // Add the rest of the objects.
  while (begin != end) {
    result += bbox<_BBox>(*begin++);
  }
  return result;
}


template<typename _BBox, typename _ForwardIterator>
inline
std::vector<_BBox>
bboxForEach(_ForwardIterator begin, _ForwardIterator end)
{
  std::vector<_BBox> result(std::distance(begin, end));
  for (std::size_t i = 0; i != result.size(); ++i) {
    result[i] = bbox<_BBox>(*begin++);
  }
  return result;
}


template<typename _Float, typename _Float2, std::size_t _D>
struct BBoxForObject<_Float, BBox<_Float2, _D> >
{
  typedef BBox<_Float2, _D> DefaultBBox;

  static
  BBox<_Float, _D>
  create(BBox<_Float2, _D> const& x)
  {
    return BBox<_Float, _D>{ext::ConvertArray<_Float>::convert(x.lower),
        ext::ConvertArray<_Float>::convert(x.upper)};
  }
};

/// Trivially convert a bounding box to one of the same type.
/** This function differs from the one in which a bounding box is converted
    to one with a different number type in that here we return a constant
    reference to the argument. Thus, we avoid constructing a bounding box. */
template<typename _Float, std::size_t _D>
struct BBoxForObject<_Float, BBox<_Float, _D> >
{
  typedef BBox<_Float, _D> DefaultBBox;

  static
  BBox<_Float, _D> const&
  create(BBox<_Float, _D> const& x)
  {
    return x;
  }
};

template<typename _Float, typename _Float2, std::size_t _D>
struct BBoxForObject<_Float, std::array<_Float2, _D> >
{
  typedef BBox<_Float2, _D> DefaultBBox;

  static
  BBox<_Float, _D>
  create(std::array<_Float2, _D> const& x)
  {
    return BBox<_Float, _D>{ext::ConvertArray<_Float>::convert(x),
        ext::ConvertArray<_Float>::convert(x)};
  }
};

template<typename _Float, typename _Float2, std::size_t _D, std::size_t _N>
struct BBoxForObject<_Float, std::array<std::array<_Float2, _D>, _N> >
{
  typedef BBox<_Float2, _D> DefaultBBox;

  static
  BBox<_Float, _D>
  create(std::array<std::array<_Float2, _D>, _N> const& x)
  {
    static_assert(_N != 0, "Error.");
    BBox<_Float, _D> box = {ext::ConvertArray<_Float>::convert(x[0]),
                            ext::ConvertArray<_Float>::convert(x[0])};
    for (std::size_t i = 1; i != _N; ++i) {
      box += ext::ConvertArray<_Float>::convert(x[i]);
    }
    return box;
  }
};


//
// Mathematical free functions
//


template<typename _T, std::size_t _D, typename _Object>
inline
bool
isInside(BBox<_T, _D> const& box, _Object const& x)
{
  // Check if the bounding box around the object is contained in this
  // bounding box.
  return isInside(box, bbox<BBox<_T, _D> >(x));
}


template<typename _T, std::size_t _D>
inline
bool
isInside(BBox<_T, _D> const& box, std::array<_T, _D> const& p)
{
  for (std::size_t i = 0; i != _D; ++i) {
    if (p[i] < box.lower[i] || box.upper[i] < p[i]) {
      return false;
    }
  }
  return true;
}


template<typename _T, std::size_t _D>
inline
bool
doOverlap(BBox<_T, _D> const& a, BBox<_T, _D> const& b)
{
  for (std::size_t i = 0; i != _D; ++i) {
    if (std::max(a.lower[i], b.lower[i]) >
        std::min(a.upper[i], b.upper[i])) {
      return false;
    }
  }
  return true;
}


// Return the squared distance between two 1-D intervals.
template<typename _T>
inline
_T
squaredDistanceBetweenIntervals(_T const lower1, _T const upper1,
                                _T const lower2, _T const upper2)
{
  // Consider the intervals [l1..u1] and [l2..u2].
  // l1 u1 l2 u2
  if (upper1 < lower2) {
    return (upper1 - lower2) * (upper1 - lower2);
  }
  // l2 u2 l1 u2
  if (upper2 < lower1) {
    return (upper2 - lower1) * (upper2 - lower1);
  }
  return 0;
}


// Return the squared distance between two bounding boxes.
template<typename _T, std::size_t _D>
inline
_T
squaredDistance(BBox<_T, _D> const& x, BBox<_T, _D> const& y)
{
  _T d2 = 0;
  for (std::size_t i = 0; i != _D; ++i) {
    d2 += squaredDistanceBetweenIntervals(x.lower[i], x.upper[i],
                                          y.lower[i], y.upper[i]);
  }
  return d2;
}


// CONTINUE: Try to get rid of floor and ceil.
template<typename Index, typename MultiIndexOutputIterator>
inline
void
scanConvert(MultiIndexOutputIterator indices, BBox<double, 3> const& box)
{
  // Make the index bounding box.
  BBox<Index, 3> ib = {{{
        Index(std::ceil(box.lower[0])),
        Index(std::ceil(box.lower[1])),
        Index(std::ceil(box.lower[2]))
      }
    },
    { {
        Index(std::floor(box.upper[0])),
        Index(std::floor(box.upper[1])),
        Index(std::floor(box.upper[2]))
      }
    }
  };

  // Scan convert the integer bounding box.
  scanConvertIndex(indices, ib);
}


template<typename MultiIndexOutputIterator, typename Index>
inline
void
scanConvertIndex(MultiIndexOutputIterator indices, BBox<Index, 3> const& box)
{

  Index const iStart = box.lower[0];
  Index const iEnd = box.upper[0];
  Index const jStart = box.lower[1];
  Index const jEnd = box.upper[1];
  Index const kStart = box.lower[2];
  Index const kEnd = box.upper[2];

  std::array<Index, 3> index;
  for (index[2] = kStart; index[2] <= kEnd; ++index[2]) {
    for (index[1] = jStart; index[1] <= jEnd; ++index[1]) {
      for (index[0] = iStart; index[0] <= iEnd; ++index[0]) {
        *indices++ = index;
      }
    }
  }
}


template<typename MultiIndexOutputIterator, typename Index>
inline
void
scanConvert(MultiIndexOutputIterator indices, BBox<double, 3> const& box,
            BBox<Index, 3> const& domain)
{
  // Make the integer bounding box.
  BBox<Index, 3> ib = {{{
        Index(std::ceil(box.lower[0])),
        Index(std::ceil(box.lower[1])),
        Index(std::ceil(box.lower[2]))
      }
    },
    { {
        Index(std::floor(box.upper[0])),
        Index(std::floor(box.upper[1])),
        Index(std::floor(box.upper[2]))
      }
    }
  };

  // Scan convert the integer bounding box on the specified domain.
  scanConvertIndex(indices, ib, domain);
}


template<typename MultiIndexOutputIterator, typename Index>
inline
void
scanConvertIndex(MultiIndexOutputIterator indices, BBox<Index, 3> const& box,
                 BBox<Index, 3> const& domain)
{
  BBox<Index, 3> inter = intersection(box, domain);
  if (! isEmpty(inter)) {
    scanConvertIndex(indices, inter);
  }
}


} // namespace geom
} // namespace stlib
