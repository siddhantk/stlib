// -*- C++ -*-

#include "stlib/sfc/OverlapQuery.h"

#include <random>

int
main()
{
  std::size_t constexpr Dimension = 1;
  typedef float Float;
  typedef stlib::sfc::Traits<Dimension, Float> Traits;
  typedef stlib::sfc::OverlapQuery<Traits> OverlapQuery;
  typedef stlib::geom::BBox<Float, Dimension> BBox;
  typedef stlib::container::PackedArrayOfArrays<std::size_t> Packed;
  using stlib::geom::bbox;
  using stlib::sfc::overlapQuery;
  using stlib::sfc::overlapQueryBrute;

  // Functor interface for single queries.
  std::vector<std::size_t> stack;
  {
    OverlapQuery overlapQuery(std::vector<BBox>{});
    std::vector<std::size_t> overlapping;
    overlapQuery(BBox{{{0}}, {{1}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping.empty());
  }
  {
    OverlapQuery overlapQuery(std::vector<BBox>{BBox{{{0}}, {{1}}}});
    std::vector<std::size_t> overlapping;
    overlapQuery(BBox{{{-1}}, {{-1}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping.empty());
    overlapQuery(BBox{{{-1}}, {{0}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping == std::vector<std::size_t>{0});
  }
  {
    OverlapQuery overlapQuery(std::vector<BBox>{BBox{{{0}}, {{1}}},
          BBox{{{1}}, {{2}}}});
    std::vector<std::size_t> overlapping;
    overlapQuery(BBox{{{-1}}, {{-1}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping.empty());
    overlapQuery(BBox{{{-1}}, {{0}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping == std::vector<std::size_t>{0});
    overlapping.clear();
    overlapQuery(BBox{{{0}}, {{1}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping == (std::vector<std::size_t>{0, 1}));
    overlapping.clear();
    overlapQuery(BBox{{{2}}, {{3}}}, std::back_inserter(overlapping), &stack);
    assert(overlapping == std::vector<std::size_t>{1});
  }

  // Function interface for multiple queries.
  {
    std::vector<BBox> objectBoxes;
    std::vector<BBox> const queryBoxes;
    Packed const overlapping = overlapQueryBrute(objectBoxes, queryBoxes);
    assert(overlapping.numArrays() == queryBoxes.size());
    assert(overlapQuery(std::move(objectBoxes), queryBoxes) == overlapping);
  }
  {
    std::vector<BBox> objectBoxes = {BBox{{{0}}, {{1}}}};
    std::vector<BBox> const queryBoxes;
    Packed const overlapping = overlapQueryBrute(objectBoxes, queryBoxes);
    assert(overlapping.numArrays() == queryBoxes.size());
    assert(overlapQuery(std::move(objectBoxes), queryBoxes) == overlapping);
  }
  {
    std::vector<BBox> objectBoxes;
    std::vector<BBox> const queryBoxes = {BBox{{{0}}, {{1}}}};
    Packed const overlapping = overlapQueryBrute(objectBoxes, queryBoxes);
    assert(overlapping.numArrays() == queryBoxes.size());
    assert(overlapping.empty(0));
    assert(overlapQuery(std::move(objectBoxes), queryBoxes) == overlapping);
  }
  {
    std::vector<BBox> objectBoxes = {BBox{{{0}}, {{1}}}};
    std::vector<BBox> const queryBoxes = {BBox{{{0}}, {{1}}}};
    Packed const overlapping = overlapQueryBrute(objectBoxes, queryBoxes);
    assert(overlapping.numArrays() == queryBoxes.size());
    assert(overlapping.size(0) == 1);
    assert(overlapping(0, 0) == 0);
    assert(overlapQuery(std::move(objectBoxes), queryBoxes) == overlapping);
  }

  {
    std::mt19937_64 engine;
    std::uniform_real_distribution<Float> distribution(0, 1);
    std::size_t const Size = 100;
    // Random object boxes.
    std::vector<BBox> objectBoxes(Size);
    for (auto& box: objectBoxes) {
      Float const lower = distribution(engine);
      box = BBox{{{lower}}, {{lower + Float(0.01)}}};
    }
    // Random query boxes.
    std::vector<BBox> queryBoxes(Size);
    for (auto& box: queryBoxes) {
      Float const lower = distribution(engine);
      box = BBox{{{lower}}, {{lower + Float(0.01)}}};
    }
    Packed const overlapping = overlapQueryBrute(objectBoxes, queryBoxes);
    assert(overlapQuery(std::move(objectBoxes), queryBoxes) == overlapping);
  }

  return 0;
}
