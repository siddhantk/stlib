// -*- C++ -*-

/*!
  \file ExtremePoints.h
  \brief Implements a class for bounding objects by their extreme points.
*/

#if !defined(__geom_ExtremePoints_h__)
#define __geom_ExtremePoints_h__

#include "stlib/geom/kernel/BBox.h"

namespace stlib
{
namespace geom
{

//! Class for bounding objects by their extreme points.
/*!
  \param _T is the number type.
  \param _N is the dimension.

  This class is an aggregate type. Thus it has no user-defined constructors.
*/
template<typename _T, std::size_t _D>
struct ExtremePoints {
  //
  // Types.
  //

  //! The number type.
  typedef _T Number;
  //! The point type.
  typedef std::array<_T, _D> Point;

  //
  // Constants.
  //

  //! The space dimension.
  BOOST_STATIC_CONSTEXPR std::size_t Dimension = _D;

  //
  // Member data
  //

  //! The extreme points. Index by dimension and direction bit.
  std::array<std::array<Point, 2>, Dimension> points;

  //
  // Member functions.
  //

  //! Return true if no objects are being bounded.
  /*! An empty structure has NaN for its coordinates. */
  bool
  isEmpty() const
  {
    // We just check the first coordinate.
    return points[0][0][0] != points[0][0][0];
  }

  //! Expand to contain the object.
  template<typename _Object>
  ExtremePoints&
  operator+=(const _Object& p);

  //! Expand to contain the point.
  ExtremePoints&
  operator+=(const Point& p);

  //! Expand to contain the other extreme points.
  ExtremePoints&
  operator+=(const ExtremePoints& x);

  //! Bound the sequence of objects.
  template<typename _ForwardIterator>
  void
  bound(_ForwardIterator begin, _ForwardIterator end);

  //! Convert the extreme points to a tight bounding box.
  void
  bbox(BBox<Number, Dimension>* box) const
  {
    for (std::size_t i = 0; i != Dimension; ++i) {
      box->lower[i] = points[i][0][i];
      box->upper[i] = points[i][1][i];
    }
  }

  //
  // The builders are static member functions to make it easier to build
  // the DS from objects that use different floating-point number types.
  //

  //! To "make" the ExtremePoints DS, just return a const reference to the argument.
  static
  ExtremePoints const&
  extremePoints(ExtremePoints const& x)
  {
    return x;
  }

  //! Make an ExtremePoints DS from a single point.
  /*! Note that we can't template on the number type as that would conflict
    with builders that take an array of points as an argument. */
  static
  ExtremePoints
  extremePoints(std::array<float, _D> const& x)
  {
    return _extremePoints(ext::ConvertArray<_T>::convert(x));
  }

  //! Make an ExtremePoints DS from a single point.
  /*! Note that we can't template on the number type as that would conflict
    with builders that take an array of points as an argument. */
  static
  ExtremePoints
  extremePoints(std::array<double, _D> const& x)
  {
    return _extremePoints(ext::ConvertArray<_T>::convert(x));
  }

  //! Make an ExtremePoints DS from a sequence of points of different number type.
  template<typename _T2, std::size_t _N>
  static
  ExtremePoints
  extremePoints(std::array<std::array<_T2, _D>, _N> const& points);

  //! Make an ExtremePoints DS from a sequence of points.
  template<std::size_t _N>
  static
  ExtremePoints
  extremePoints(std::array<std::array<_T, _D>, _N> const& points);

  //! Return an empty data structure, i.e., one that bounds nothing.
  /*! The coordinates are all NaN. */
  static
  ExtremePoints
  makeEmpty();

private:

  //! Make an ExtremePoints DS from a single point.
  static
  ExtremePoints
  _extremePoints(std::array<_T, _D> const& x);
};


//
// Equality Operators.
//


//! Equality.
/*! \relates ExtremePoints
 \note Two empty data structures are considered to be equal, even though the 
 coordinate values are not equal (because they are NaN). */
template<typename _T, std::size_t _D>
inline
bool
operator==(const ExtremePoints<_T, _D>& a, const ExtremePoints<_T, _D>& b)
{
  return (a.isEmpty() && b.isEmpty()) || a.points == b.points;
}


//! Inequality.
/*! \relates ExtremePoints */
template<typename _T, std::size_t _D>
inline
bool
operator!=(const ExtremePoints<_T, _D>& a, const ExtremePoints<_T, _D>& b)
{
  return !(a == b);
}


//
// File I/O.
//


//! Read the extreme points.
/*! \relates ExtremePoints */
template<typename _T, std::size_t _D>
inline
std::istream&
operator>>(std::istream& in, ExtremePoints<_T, _D>& x)
{
  return in >> x.points;
}


//! Write the extreme points box.
/*! \relates ExtremePoints */
template<typename _T, std::size_t _D>
inline
std::ostream&
operator<<(std::ostream& out, const ExtremePoints<_T, _D>& x)
{
  return out << x.points;
}


} // namespace geom
} // namespace stlib

#define __geom_ExtremePoints_ipp__
#include "stlib/geom/kernel/ExtremePoints.ipp"
#undef __geom_ExtremePoints_ipp__

#endif
