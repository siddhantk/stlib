// -*- C++ -*-

#include "stlib/sfc/CellsUniform.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace stlib;

TEST_CASE("CellsUniform. 1-D, void, 0 levels.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  typedef sfc::CellsUniform<Traits, void, true> CellsUniform;
  typedef CellsUniform::Point Point;
  CellsUniform cells(Point{{0}}, Point{{1}}, 0);

  SECTION("I/O.") {
    std::cout << cells;
  }  
  SECTION("Accessors.") {
    REQUIRE(cells.lowerCorner() == Point{{0}});
    REQUIRE(cells.lengths() == Point{{1}});
    REQUIRE(cells.numLevels() == 0);
    REQUIRE(cells.size() == 0);
  }
  SECTION("Copy, assign.") {
    // Copy constructor.
    CellsUniform c = cells;
    // Assignment operator.
    c = cells;
  }
  SECTION("Order constructor.") {
    CellsUniform c(cells.grid());
    REQUIRE(c.lowerCorner() == cells.lowerCorner());
    REQUIRE(c.lengths() == cells.lengths());
    REQUIRE(c.numLevels() == cells.numLevels());
    REQUIRE(c.size() == 0);
  }
  SECTION("Coarsen.") {
    REQUIRE(cells.coarsen(-1) == 0);
  }
}


TEST_CASE("CellsUniform. 1-D, BBox, 20 levels, false. "
          "Uniformly-spaced points.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef sfc::CellsUniform<Traits, Cell, false> CellsUniform;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  SECTION("Build.") {
    CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
    REQUIRE(cells.lowerCorner() == Point{{0}});
    REQUIRE(cells.lengths() == Point{{1}});
    REQUIRE(cells.numLevels() == NumLevels);
    REQUIRE(cells.size() == 0);
    std::cout << cells;

    // Build.
    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());

    SECTION("Build from sorted.") {
      CellsUniform fromSorted(cells.grid());
      fromSorted.buildCells(objects);
      REQUIRE(fromSorted.size() == cells.size());
      for (std::size_t i = 0; i != fromSorted.size(); ++i) {
        REQUIRE(fromSorted[i] == cells[i]);
      }
    }

    SECTION("Rebuild.") {
      cells.clear();
      REQUIRE(cells.size() == 0);
      cells.shrink_to_fit();
      REQUIRE(cells.size() == 0);
      cells.buildCells(&objects);
      REQUIRE(cells.size() == objects.size());
      cells.shrink_to_fit();
      REQUIRE(cells.size() == objects.size());
    }

    SECTION("Coarsen.") {
      while (cells.numLevels() != 0) {
        cells.coarsen();
      }
      REQUIRE(cells.numLevels() == 0);
      REQUIRE(cells.size() == 1);
      REQUIRE(cells[0].lower[0] == 0);
      REQUIRE(cells[0].upper[0] ==
              Float(objects.size() - 1) / objects.size());
    }
  }

  SECTION("Serialize.") {
    CellsUniform x(Point{{0}}, Point{{1}}, NumLevels);
    x.buildCells(&objects);
    std::vector<unsigned char> buffer;
    x.serialize(&buffer);
    CellsUniform y(Point{{0}}, Point{{1}}, NumLevels);
    y.unserialize(buffer);
    REQUIRE(x == y);
  }
}


TEST_CASE("CellsUniform. 1-D, BBox, 20 levels. "
          "Uniformly-spaced points.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef sfc::CellsUniform<Traits, Cell, false> CellsUniform;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  SECTION("Build.") {
    CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
    REQUIRE(cells.lowerCorner() == Point{{0}});
    REQUIRE(cells.lengths() == Point{{1}});
    REQUIRE(cells.numLevels() == NumLevels);
    REQUIRE(cells.size() == 0);

    // Build.
    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());

    SECTION("Build from sorted.") {
      CellsUniform fromSorted(cells.grid());
      fromSorted.buildCells(objects);
      REQUIRE(fromSorted.size() == cells.size());
      for (std::size_t i = 0; i != fromSorted.size(); ++i) {
        REQUIRE(fromSorted[i] == cells[i]);
      }
    }

    SECTION("Rebuild.") {
      cells.clear();
      REQUIRE(cells.size() == 0);
      cells.shrink_to_fit();
      REQUIRE(cells.size() == 0);
      cells.buildCells(&objects);
      REQUIRE(cells.size() == objects.size());
      cells.shrink_to_fit();
      REQUIRE(cells.size() == objects.size());
    }

    SECTION("Coarsen.") {
      while (cells.numLevels() != 0) {
        cells.coarsen();
      }
      REQUIRE(cells.numLevels() == 0);
      REQUIRE(cells.size() == 1);
      REQUIRE(cells[0].lower[0] == 0);
      REQUIRE(cells[0].upper[0] ==
              Float(objects.size() - 1) / objects.size());
    }
  }

  SECTION("Serialize.") {
    CellsUniform x(Point{{0}}, Point{{1}}, NumLevels);
    x.buildCells(&objects);
    std::vector<unsigned char> buffer;
    x.serialize(&buffer);
    CellsUniform y(Point{{0}}, Point{{1}}, NumLevels);
    y.unserialize(buffer);
    REQUIRE(x == y);
  }
}


TEST_CASE("CellsUniform. Order and restore.",
          "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef sfc::CellsUniform<Traits, void, false> CellsUniform;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points, shuffled.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }
  std::random_shuffle(objects.begin(), objects.end());

  std::vector<Point> const originalObjects(objects);
  CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
  sfc::OrderedObjects orderedObjects;
  cells.buildCells(&objects, &orderedObjects);
  std::vector<Point> copy(originalObjects);
  orderedObjects.order(copy.begin(), copy.end());
  REQUIRE(copy == objects);
  orderedObjects.restore(copy.begin(), copy.end());
  REQUIRE(copy == originalObjects);
}


TEST_CASE("CellsUniform. 1-D, void, 20 levels. Coarsen.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef sfc::CellsUniform<Traits, void, true> CellsUniform;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
  cells.buildCells(&objects);

  // Coarsen.
  REQUIRE(cells.coarsen(0) == 0);
  REQUIRE(cells.coarsen(1) == 20 - 9);
  REQUIRE(cells.numLevels() == 9);
  REQUIRE(cells.coarsen(2) == 1);
  REQUIRE(cells.numLevels() == 8);
  REQUIRE(cells.size() == 256);
}


TEST_CASE("CellsUniform. 1-D, void, 3 levels. Coarsen.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 3;
  typedef sfc::CellsUniform<Traits, void, true> CellsUniform;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(8);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
  cells.buildCells(&objects);

  // Coarsen.
  REQUIRE(cells.coarsen(1) == 0);
  REQUIRE(cells.coarsen(2) == 1);
  REQUIRE(cells.numLevels() == 2);
}


TEST_CASE("CellsUniform. 1-D, void, 20 levels. Merge.", "[CellsUniform]")
{
  typedef sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef sfc::CellsUniform<Traits, void, true> CellsUniform;
  typedef CellsUniform::Code Code;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  SECTION("Merge with a copy.") {
    CellsUniform cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    cells.checkValidity();
    REQUIRE(cells.size() == objects.size());
    CellsUniform copy = cells;
    cells += copy;
    cells.checkValidity();
    REQUIRE(cells.size() == objects.size());
    for (std::size_t i = 0; i != cells.size(); ++i) {
      REQUIRE((cells.delimiter(i + 1) - cells.delimiter(i)) ==
              Code(2));
    }
  }
  SECTION("Merge separate halves.") {
    std::vector<Point> objects1(objects.begin(),
                                objects.begin() + objects.size() / 2);
    std::vector<Point> objects2(objects.begin() + objects.size() / 2,
                                objects.end());
    CellsUniform cells1(Point{{0}}, Point{{1}}, NumLevels);
    cells1.buildCells(&objects1);
    REQUIRE(cells1.size() == objects1.size());
    cells1.checkValidity();
    CellsUniform cells2(Point{{0}}, Point{{1}}, NumLevels);
    cells2.buildCells(&objects2);
    REQUIRE(cells2.size() == objects2.size());
    cells2.checkValidity();

    cells1 += cells2;
    cells1.checkValidity();
    REQUIRE(cells1.size() == objects.size());
  }
}
