
:
MPI processes = 16
Ball radius = 0.00610619
Number of objects = 1.04858e+06
Number of relevant objects
  sum = 1.96045e+07, mean = 1.22528e+06, min = 1.19371e+06, max = 1.25626e+06
Total time
  sum = 93.3176, mean = 5.83235, min = 5.75669, max = 5.87216

sfc::gatherOverlapping():
Number of query windows = 1.04858e+06
Number of objects = 1.04858e+06
maxQueryWindowsPerCell = 32
Number of cell query windows
  sum = 939291, mean = 58705.7, min = 58477, max = 58959
Maximum objects per cell = 256
Make a vector of coarsened query windows
  sum = 2.4019, mean = 0.150119, min = 0.147858, max = 0.151531
Build the description of the distributed objects
  sum = 6.31682, mean = 0.394801, min = 0.381097, max = 0.39926
Calculate relevant cells
  sum = 2.16951, mean = 0.135594, min = 0.127635, max = 0.138018
Gather the relevant objects
  sum = 1.02996, mean = 0.0643725, min = 0.0605072, max = 0.0751528
Total time
  sum = 11.9189, mean = 0.744934, min = 0.743104, max = 0.746087

sfc::gatherRelevantPointToPoint():
Number of distributed cells = 355286
Distributed cell storage = 1.42115e+07
Number of local objects = 1.04858e+06
Number of relevant cells
  sum = 414524, mean = 25907.8, min = 25315, max = 26523
Storage for local objects = 1.67772e+07
Storage for relevant objects
  sum = 3.13671e+08, mean = 1.96045e+07, min = 1.90994e+07, max = 2.01002e+07
Number of object cells
  sum = 379245, mean = 23702.8, min = 23429, max = 23954
Number of object cells that are relevant for others
  sum = 379245, mean = 23702.8, min = 23429, max = 23954
Number of sends
  sum = 142, mean = 8.875, min = 7, max = 11
Storage for send buffers
  sum = 4.52359e+07, mean = 2.82724e+06, min = 2.34741e+06, max = 3.3395e+06
Number of receives
  sum = 142, mean = 8.875, min = 6, max = 11
Storage for receive buffers
  sum = 1.45188e+07, mean = 907426, min = 827008, max = 1.40813e+06
Count the number of relevant objects
  sum = 0.0847895, mean = 0.00529935, min = 0.00266166, max = 0.00731406
Build object cells
  sum = 0.566047, mean = 0.0353779, min = 0.0347749, max = 0.0361214
Record the relevant objects.
  sum = 0.0876813, mean = 0.00548008, min = 0.00285247, max = 0.00715678
All-gather the relevant cells
  sum = 0.153916, mean = 0.00961972, min = 0.00390927, max = 0.025597
Initiate sends for the objects
  sum = 0.0609222, mean = 0.00380764, min = 0.00326147, max = 0.00443303
Receive objects
  sum = 0.0648453, mean = 0.00405283, min = 0.00287915, max = 0.00494903
Wait for sends to complete
  sum = 0.0106495, mean = 0.000665594, min = 1.3061e-05, max = 0.00187222
Total time
  sum = 1.02984, mean = 0.0643647, min = 0.0604982, max = 0.0751437

Do overlap tests:
Number of overlapping objects = 1.04858e+06
Total time
  sum = 17.2175, mean = 1.07609, min = 1.04393, max = 1.09716

sfc::doOverlap():
Number of object boxes
  sum = 1.96045e+07, mean = 1.22528e+06, min = 1.19371e+06, max = 1.25626e+06
Number of query boxes = 1.04858e+06
Build the trie
  sum = 2.50517, mean = 0.156573, min = 0.151564, max = 0.159775
Cache topological information
  sum = 0.0154103, mean = 0.000963144, min = 0.000778124, max = 0.00143905
Overlap queries
  sum = 14.2296, mean = 0.889352, min = 0.858254, max = 0.90852
Total time
  sum = 16.7506, mean = 1.04691, min = 1.01411, max = 1.06821

Overlap queries:
Total time
  sum = 61.7141, mean = 3.85713, min = 3.75632, max = 3.9305

sfc::overlapQuery():
Number of object boxes
  sum = 1.96045e+07, mean = 1.22528e+06, min = 1.19371e+06, max = 1.25626e+06
Number of query boxes = 1.04858e+06
Number of overlapping objects
  sum = 2.71276e+08, mean = 1.69548e+07, min = 1.69241e+07, max = 1.69827e+07
Build the trie
  sum = 2.71361, mean = 0.169601, min = 0.160262, max = 0.176061
Cache topological information
  sum = 0.0184625, mean = 0.0011539, min = 0.000825687, max = 0.00184568
Overlap queries
  sum = 56.3072, mean = 3.5192, min = 3.40899, max = 3.57138
Map indices
  sum = 2.16037, mean = 0.135023, min = 0.102737, max = 0.161539
Total time
  sum = 61.2006, mean = 3.82504, min = 3.7246, max = 3.896
