// -*- C++ -*-

#include "stlib/sfc/CellsMultiLevel.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"


TEST_CASE("CellsMultiLevel. 1-D, std::size_t.", "[CellsMultiLevel]") {
  typedef stlib::sfc::Traits<1> Traits;
  typedef std::size_t Cell;
  typedef stlib::sfc::CellsMultiLevel<Traits, Cell, false> CellsMultiLevel;
  typedef CellsMultiLevel::Point Point;
  typedef CellsMultiLevel::BBox BBox;

  CellsMultiLevel cells(Point{{0}}, Point{{1}}, 0);
  REQUIRE(cells.lowerCorner() == Point{{0}});
  REQUIRE(cells.lengths() == Point{{1}});
  REQUIRE(cells.numLevels() == 0);
  REQUIRE(cells.size() == 0);
  REQUIRE(cells.calculateHighestLevel() == 0);
  {
    // Copy constructor.
    CellsMultiLevel c = cells;
    // Assignment operator.
    c = cells;
  }
  {
    // Order constructor.
    CellsMultiLevel c(cells.grid());
    REQUIRE(c.lowerCorner() == cells.lowerCorner());
    REQUIRE(c.lengths() == cells.lengths());
    REQUIRE(c.numLevels() == cells.numLevels());
    REQUIRE(c.size() == 0);
    REQUIRE(c.calculateHighestLevel() == 0);
  }
  {
    // Cell length constructor.
    CellsMultiLevel c(BBox{{{0}}, {{1}}}, 0.1);
    REQUIRE(c.lowerCorner()[0] <= cells.lowerCorner()[0]);
    REQUIRE(c.lengths()[0] >= cells.lengths()[0]);
    REQUIRE(c.numLevels() == 3);
    REQUIRE(c.size() == 0);
    REQUIRE(c.calculateHighestLevel() == 0);
  }
  // setNumLevelsToFit()
  cells.setNumLevelsToFit();
  REQUIRE(cells.numLevels() == 0);
}


TEST_CASE("CellsMultiLevel. 1-D, void, 2 levels.", "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::sfc::BlockCode<Traits> BlockCode;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, true> CellsMultiLevel;
  typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Point Point;

  const Point LowerCorner = {{0}};
  const Point Lengths = {{1}};
  const std::size_t NumLevels = 2;
  BlockCode blockCode(LowerCorner, Lengths, NumLevels);
  CellsMultiLevel cells(LowerCorner, Lengths, NumLevels);
  {
    std::vector<Code> objectCodes;
    objectCodes.push_back(2); // 00 10
    objectCodes.push_back(6); // 01 10
    objectCodes.push_back(10); // 10 10
    objectCodes.push_back(14); // 11 10
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 1);
      // Since we are only interested in delimiters, we use the object codes
      // for the objects.
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 4);
      REQUIRE(cells.code(0) == Code(2)); // 00 10
      REQUIRE(cells.code(1) == Code(6)); // 01 10
      REQUIRE(cells.code(2) == Code(10)); // 10 10
      REQUIRE(cells.code(3) == Code(14)); // 11 10
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(1));
      REQUIRE(cells.delimiter(2) == std::size_t(2));
      REQUIRE(cells.delimiter(3) == std::size_t(3));
      REQUIRE(cells.delimiter(4) == std::size_t(4));
    }
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 2);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 2);
      REQUIRE(cells.code(0) == Code(1)); // 00 01
      REQUIRE(cells.code(1) == Code(9)); // 10 01
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(2));
      REQUIRE(cells.delimiter(2) == std::size_t(4));
    }
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 4);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 1);
      REQUIRE(cells.code(0) == Code(0)); // 00 00
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(4));
    }
  }
  {
    std::vector<Code> objectCodes;
    objectCodes.push_back(2); // 00 10
    objectCodes.push_back(2); // 00 10
    objectCodes.push_back(6); // 01 10
    objectCodes.push_back(10); // 10 10
    objectCodes.push_back(14); // 11 10
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 1);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 4);
      REQUIRE(cells.code(0) == Code(2)); // 00 10
      REQUIRE(cells.code(1) == Code(6)); // 01 10
      REQUIRE(cells.code(2) == Code(10)); // 10 10
      REQUIRE(cells.code(3) == Code(14)); // 11 10
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(2));
      REQUIRE(cells.delimiter(2) == std::size_t(3));
      REQUIRE(cells.delimiter(3) == std::size_t(4));
      REQUIRE(cells.delimiter(4) == std::size_t(5));
    }
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 2);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 3);
      REQUIRE(cells.code(0) == Code(2)); // 00 10
      REQUIRE(cells.code(1) == Code(6)); // 01 10
      REQUIRE(cells.code(2) == Code(9)); // 10 01
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(2));
      REQUIRE(cells.delimiter(2) == std::size_t(3));
      REQUIRE(cells.delimiter(3) == std::size_t(5));
    }
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 3);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 2);
      REQUIRE(cells.code(0) == Code(1)); // 00 01
      REQUIRE(cells.code(1) == Code(9)); // 10 01
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(3));
      REQUIRE(cells.delimiter(2) == std::size_t(5));
    }
    {
      std::vector<Code> cellCodes;
      coarsen(blockCode, objectCodes, &cellCodes, 5);
      cells.buildCells(cellCodes, objectCodes, objectCodes);
      REQUIRE(cells.size() == 1);
      REQUIRE(cells.code(0) == Code(0)); // 00 00
      REQUIRE(cells.delimiter(0) == std::size_t(0));
      REQUIRE(cells.delimiter(1) == std::size_t(5));
    }
  }
  // Build from code/count pairs.
  {
    typedef std::pair<Code, std::size_t> Pair;
    Code const Guard = Traits::GuardCode;

    cells.buildCells(std::vector<Pair>{{Guard, 0}});
    REQUIRE(cells.size() == 0);
    REQUIRE(cells.delimiter(0) == 0);

    cells.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    REQUIRE(cells.size() == 1);
    REQUIRE(cells.code(0) == Code(0));
    REQUIRE(cells.delimiter(0) == 0);
    REQUIRE(cells.delimiter(1) == 1);

    cells.buildCells(std::vector<Pair>{{0, 10}, {Guard, 0}});
    REQUIRE(cells.size() == 1);
    REQUIRE(cells.code(0) == Code(0));
    REQUIRE(cells.delimiter(0) == 0);
    REQUIRE(cells.delimiter(1) == 10);

    cells.buildCells(std::vector<Pair>{{1, 1}, {9, 1}, {Guard, 0}});
    REQUIRE(cells.size() == 2);
    REQUIRE(cells.code(0) == Code(1));
    REQUIRE(cells.code(1) == Code(9));
    REQUIRE(cells.delimiter(0) == 0);
    REQUIRE(cells.delimiter(1) == 1);
    REQUIRE(cells.delimiter(2) == 2);

    // Convert between cell indices and codes.
    {
      // Four cells at level 2.
      // 00 10, 01 10, 10 10, 11 10
      cells.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1},
                                                             {Guard, 0}});
      {
        std::vector<std::size_t> indices;
        for (std::size_t i = 0; i != cells.size() + 1; ++i) {
          REQUIRE(cells.codesToCells(cells.codes(indices)) == indices);
          indices.push_back(i);
        }
      }

      // Cells at levels 1 and 2.
      // 00 01, 10 10, 11 10
      cells.buildCells(std::vector<Pair>{{1, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      {
        std::vector<std::size_t> indices;
        for (std::size_t i = 0; i != cells.size() + 1; ++i) {
          REQUIRE(cells.codesToCells(cells.codes(indices)) == indices);
          indices.push_back(i);
        }
      }
    }
  }
}


TEST_CASE("CellsMultiLevel. 1-D, void, 2 levels, merge", "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, true> CellsMultiLevel;
  typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Point Point;
  typedef std::pair<Code, std::size_t> Pair;
 
  Code const Guard = Traits::GuardCode;
  Point const LowerCorner = {{0}};
  Point const Lengths = {{1}};
  std::size_t const NumLevels = 2;
  CellsMultiLevel a(LowerCorner, Lengths, NumLevels);
  CellsMultiLevel b(LowerCorner, Lengths, NumLevels);
  CellsMultiLevel c(LowerCorner, Lengths, NumLevels);
 
  SECTION("Both empty.") {
    a += b;
    REQUIRE(a.size() == 0);
  }
  SECTION("One is empty. Level 0") {
    a.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    CellsMultiLevel const result = a;
    a += b;
    REQUIRE(a == result);
    b += a;
    REQUIRE(b == result);
  }
  SECTION("One is empty. Level 1 followed by level 2.") {
    // 00 01, 10 10 
    a.buildCells(std::vector<Pair>{{1, 1}, {10, 1}, {Guard, 0}});
    CellsMultiLevel const result = a;
    a += b;
    REQUIRE(a == result);
    b += a;
    REQUIRE(b == result);
  }
  SECTION("One is empty. Level 2 followed by level 1.") {
    // 00 10, 10 01
    a.buildCells(std::vector<Pair>{{2, 1}, {9, 1}, {Guard, 0}});
    CellsMultiLevel const result = a;
    a += b;
    REQUIRE(a == result);
    b += a;
    REQUIRE(b == result);
  }
  SECTION("Level 0. Level 0.") {
    a.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    b.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    c.buildCells(std::vector<Pair>{{0, 2}, {Guard, 0}});
    a += b;
    REQUIRE(a == c);
  }
  SECTION("Level 0. Level 1.") {
    a.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    // 00 01
    b.buildCells(std::vector<Pair>{{1, 1}, {Guard, 0}});
    c.buildCells(std::vector<Pair>{{0, 2}, {Guard, 0}});
    a += b;
    REQUIRE(a == c);
  }
  SECTION("Level 0. Level 1.") {
    // 00 01
    a.buildCells(std::vector<Pair>{{1, 1}, {Guard, 0}});
    b.buildCells(std::vector<Pair>{{0, 1}, {Guard, 0}});
    c.buildCells(std::vector<Pair>{{0, 2}, {Guard, 0}});
    a += b;
    REQUIRE(a == c);
  }
  SECTION("00 01 += 10 10") {
    a.buildCells(std::vector<Pair>{{1, 1}, {Guard, 0}});
    b.buildCells(std::vector<Pair>{{10, 1}, {Guard, 0}});
    c.buildCells(std::vector<Pair>{{1, 1}, {10, 1}, {Guard, 0}});
    a += b;
    REQUIRE(a == c);
  }
  SECTION("01 10 += 10 01") {
    a.buildCells(std::vector<Pair>{{6, 1}, {Guard, 0}});
    b.buildCells(std::vector<Pair>{{9, 1}, {Guard, 0}});
    c.buildCells(std::vector<Pair>{{6, 1}, {9, 1}, {Guard, 0}});
    a += b;
    REQUIRE(a == c);
  }
}


template<bool _StoreDel>
void
testCropVoid()
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef Traits::Code Code;
  typedef Traits::Point Point;
  typedef std::pair<Code, std::size_t> Pair;

  Code const Guard = Traits::GuardCode;
  Point const LowerCorner = {{0}};
  Point const Lengths = {{1}};
  std::size_t const NumLevels = 2;

  {
    typedef stlib::sfc::CellsMultiLevel<Traits, void, _StoreDel>
      CellsMultiLevel;

    CellsMultiLevel a(LowerCorner, Lengths, NumLevels);
    CellsMultiLevel b(LowerCorner, Lengths, NumLevels);

    // Empty.
    {
      a.crop(std::vector<std::size_t>{});
      a.checkValidity();
      REQUIRE((a.size() == 0));
    }
    // 00 10, 01 10, 10 10, 11 10
    {
      a.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      a.crop(std::vector<std::size_t>{});
      a.checkValidity();
      REQUIRE((a.size() == 0));

      a.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      a.crop(std::vector<std::size_t>{0});
      a.checkValidity();
      b.buildCells(std::vector<Pair>{{2, 1}, {Guard, 0}});
      REQUIRE((a == b));

      a.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      a.crop(std::vector<std::size_t>{1, 3});
      a.checkValidity();
      b.buildCells(std::vector<Pair>{{6, 1}, {14, 1}, {Guard, 0}});
      REQUIRE((a == b));

      a.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      a.crop(std::vector<std::size_t>{0, 1, 2, 3});
      a.checkValidity();
      b.buildCells(std::vector<Pair>{{2, 1}, {6, 1}, {10, 1}, {14, 1}, {Guard, 0}});
      REQUIRE((a == b));
    }
  }
}


TEST_CASE("CellsMultiLevel. crop.", "[CellsMultiLevel]")
{
  testCropVoid<false>();
  testCropVoid<true>();
}


template<bool _StoreDel>
void
testCropBBox()
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef Traits::Point Point;
  typedef Traits::BBox BBox;

  Point const LowerCorner = {{0}};
  Point const Lengths = {{1}};
  std::size_t const NumLevels = 2;

  {
    typedef stlib::sfc::CellsMultiLevel<Traits, BBox, _StoreDel>
      CellsMultiLevel;

    CellsMultiLevel a(LowerCorner, Lengths, NumLevels);
    CellsMultiLevel b(LowerCorner, Lengths, NumLevels);

    // Empty.
    {
      a.crop(std::vector<std::size_t>{});
      a.checkValidity();
      REQUIRE((a.size() == 0));
    }
    {
      std::vector<Point> objects = {{{0}}, {{0.25}}, {{0.5}}, {{0.75}}};
      std::vector<Point> subset;

      a.buildCells(&objects);
      a.crop(std::vector<std::size_t>{});
      a.checkValidity();
      REQUIRE((a.size() == 0));

      a.buildCells(&objects);
      a.crop(std::vector<std::size_t>{0});
      a.checkValidity();
      {
        std::vector<Point> subset = {{{0}}};
        b.buildCells(&subset);
      }
      REQUIRE((a == b));

      a.buildCells(&objects);
      a.crop(std::vector<std::size_t>{1, 3});
      a.checkValidity();
      {
        std::vector<Point> subset = {{{0.25}}, {{0.75}}};
        b.buildCells(&subset);
      }
      REQUIRE((a == b));

      a.buildCells(&objects);
      a.crop(std::vector<std::size_t>{0, 1, 2, 3});
      a.checkValidity();
      b.buildCells(&objects);
      REQUIRE((a == b));
    }
  }
}


TEST_CASE("CellsMultiLevel. crop BBox.", "[CellsMultiLevel]")
{
  testCropBBox<false>();
  testCropBBox<true>();
}


TEST_CASE("CellsMultiLevel. 1-D, BBox, 20 levels.", "[CellsMultiLevel]")
{
  const std::size_t NumLevels = 20;
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef stlib::sfc::CellsMultiLevel<Traits, Cell, true> CellsMultiLevel;
  typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  SECTION("Accessors.")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    REQUIRE(cells.lowerCorner() == Point{{0}});
    REQUIRE(cells.lengths() == Point{{1}});
    REQUIRE(cells.numLevels() == NumLevels);
    REQUIRE(cells.size() == 0);
    REQUIRE(cells.calculateHighestLevel() == 0);

    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());
    REQUIRE(cells.calculateHighestLevel() == cells.numLevels());
    for (std::size_t i = 0; i != cells.size(); ++i) {
      REQUIRE((cells.delimiter(i + 1) - cells.delimiter(i)) ==
              Code(1));
    }
    // CONTINUE
#if 0
    // Copy without keeping the object delimiters.
    stlib::sfc::CellsMultiLevel<Traits, Cell, false> copy(cells);
    REQUIRE(copy.codes() == cells.codes());
    REQUIRE(copy.size() == cells.size());
    for (std::size_t i = 0; i != copy.size(); ++i) {
      REQUIRE(copy[i] == cells[i]);
    }
    REQUIRE(copy.grid() == cells.grid());
#endif
  }

  SECTION("setNumLevelsToFit.")
  {
    {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.setNumLevelsToFit();
      REQUIRE(cells.numLevels() == 0);
    }
    {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.buildCells(&objects);
      cells.setNumLevelsToFit();
      REQUIRE(cells.numLevels() == NumLevels);
      cells.coarsenWithoutMerging();
      cells.setNumLevelsToFit();
      REQUIRE(cells.numLevels() == 9);
      REQUIRE(cells.grid().levelBits() == 4);
      for (std::size_t i = 0; i != cells.size(); ++i) {
        REQUIRE(cells.grid().level(cells.code(i)) == cells.numLevels());
        REQUIRE((cells.code(i) >> cells.grid().levelBits()) == i);
      }
    }
  }
  
  SECTION("Serialize.")
  {
    CellsMultiLevel x(Point{{0}}, Point{{1}}, NumLevels);
    x.buildCells(&objects);
    std::vector<unsigned char> buffer;
    x.serialize(&buffer);
    CellsMultiLevel y(Point{{0}}, Point{{1}}, NumLevels);
    y.unserialize(buffer);
    REQUIRE(x == y);
  }

  SECTION("Coarsening with LevelGreaterThan. Level 0.")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    stlib::sfc::LevelGreaterThan pred = {0};
    cells.coarsen(pred);
    REQUIRE(cells.size() == 1);
    REQUIRE(cells.calculateHighestLevel() == 0);
    REQUIRE(cells.delimiter(0) == Code(0));
    REQUIRE(cells.delimiter(cells.size()) == objects.size());
    REQUIRE(cells[0].lower[0] == 0);
    REQUIRE(cells[0].upper[0] ==
            Float(objects.size() - 1) / objects.size());
  }

  SECTION("Coarsening with LevelGreaterThan. Levels 0 through 9.")
  {
    for (std::size_t level = 0; level != 10; ++level) {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.buildCells(&objects);
      stlib::sfc::LevelGreaterThan pred = {level};
      cells.coarsen(pred);
      const std::size_t NumCells = 1 << level;
      REQUIRE(cells.size() == NumCells);
      REQUIRE(cells.calculateHighestLevel() == level);
      REQUIRE(cells.delimiter(0) == Code(0));
      REQUIRE(cells.delimiter(cells.size()) == objects.size());
      REQUIRE(cells[0].lower[0] == 0);
      REQUIRE(cells[0].upper[0] < Float(1) / NumCells);
    }
  }

  SECTION("Coarsening with coarsenCellSize().")
  {
    for (std::size_t size = 1; size <= objects.size(); size *= 2) {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.buildCells(&objects);
      cells.coarsenCellSize(size);
      const std::size_t NumCells = objects.size() / size;
      REQUIRE(cells.size() == NumCells);
      REQUIRE(cells.delimiter(0) == Code(0));
      REQUIRE(cells.delimiter(cells.size()) == objects.size());
      REQUIRE(cells[0].lower[0] == 0);
      REQUIRE(cells[0].upper[0] < Float(1) / NumCells);
    }
  }

  SECTION("Coarsening to a specified number of cells.")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());
    cells.coarsenMaxCells(objects.size() / 2);
    REQUIRE(cells.size() <= objects.size() / 2);
    cells.coarsenMaxCells(std::size_t(1));
    REQUIRE(cells.size() == 1);
    REQUIRE((cells.delimiter(1) - cells.delimiter(0)) == objects.size());
  }

  SECTION("Coarsen without merging.")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());
    cells.coarsenWithoutMerging();
    REQUIRE(cells.size() == objects.size());
  }

  SECTION("Merge with a copy.")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    REQUIRE(cells.size() == objects.size());
    CellsMultiLevel copy = cells;
    cells += copy;
    cells.checkValidity();
    REQUIRE(cells.size() == objects.size());
    for (std::size_t i = 0; i != cells.size(); ++i) {
      REQUIRE((cells.delimiter(i + 1) - cells.delimiter(i)) ==
              Code(2));
    }
  }

  SECTION("Merge separate halves.")
  {
    std::vector<Point> objects1(objects.begin(),
                                objects.begin() + objects.size() / 2);
    std::vector<Point> objects2(objects.begin() + objects.size() / 2,
                                objects.end());
    CellsMultiLevel cells1(Point{{0}}, Point{{1}}, NumLevels);
    cells1.buildCells(&objects1);
    REQUIRE(cells1.size() == objects1.size());
    cells1.checkValidity();
    CellsMultiLevel cells2(Point{{0}}, Point{{1}}, NumLevels);
    cells2.buildCells(&objects2);
    REQUIRE(cells2.size() == objects2.size());
    cells2.checkValidity();

    cells1 += cells2;
    cells1.checkValidity();
    REQUIRE(cells1.size() == objects.size());
  }
}


TEST_CASE("CellsMultiLevel. 1-D, BBox, 20 levels. Objects at 1/2^n.",
          "[CellsMultiLevel]")
{
  const std::size_t NumLevels = 20;
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef stlib::sfc::CellsMultiLevel<Traits, Cell, true> CellsMultiLevel;
  typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Point;

  // 1/2^n.
  std::vector<Point> objects(NumLevels + 1);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(1) / (2 << i);
  }

  SECTION("coarsenCellSize()")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    cells.coarsenCellSize(1);
    REQUIRE(cells.size() == objects.size());
    for (std::size_t i = 0; i != cells.size(); ++i) {
      REQUIRE(cells.delimiter(i) == i);
    }
    REQUIRE(cells.delimiter(cells.size()) == objects.size());
  }

  SECTION("Coarsen LevelGreaterThan.")
  {
    for (std::size_t level = 0; level <= NumLevels; ++level) {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.buildCells(&objects);
      stlib::sfc::LevelGreaterThan pred = {level};
      cells.coarsen(pred);
      REQUIRE(cells.size() == level + 1);
      REQUIRE(cells.delimiter(0) == Code(0));
      REQUIRE(cells.delimiter(cells.size()) == objects.size());
      // Check that the bounding boxes contain the objects.
      for (std::size_t i = 0; i != cells.size(); ++i) {
        for (std::size_t j = cells.delimiter(i);
             j != cells.delimiter(i + 1); ++j) {
          REQUIRE(isInside(cells[i], objects[j]));
        }
      }
    }
  }
}


TEST_CASE("CellsMultiLevel. 1-D, BBox, 20 levels. Objects at 1 - 1/2^n.",
          "[CellsMultiLevel]")
{
  const std::size_t NumLevels = 20;
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef stlib::sfc::CellsMultiLevel<Traits, Cell, true> CellsMultiLevel;
  typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Point;

  // 1 - 1/2^n.
  std::vector<Point> objects(NumLevels);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = 1 - Float(1) / (2 << i);
  }

  SECTION("coarsenCellSize()")
  {
    CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
    cells.buildCells(&objects);
    cells.coarsenCellSize(1);
    REQUIRE(cells.size() == objects.size());
    for (std::size_t i = 0; i != cells.size(); ++i) {
      REQUIRE(cells.delimiter(i) == i);
    }
    REQUIRE(cells.delimiter(cells.size()) == objects.size());
  }

  SECTION("Coarsen LevelGreaterThan.")
  {
    for (std::size_t level = 0; level <= NumLevels; ++level) {
      CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
      cells.buildCells(&objects);
      stlib::sfc::LevelGreaterThan pred = {level};
      cells.coarsen(pred);
      if (level == 0) {
        REQUIRE(cells.size() == 1);
      }
      else {
        REQUIRE(cells.size() == level);
      }
      REQUIRE(cells.delimiter(0) == Code(0));
      REQUIRE(cells.delimiter(cells.size()) == objects.size());
      // Check that the bounding boxes contain the objects.
      for (std::size_t i = 0; i != cells.size(); ++i) {
        for (std::size_t j = cells.delimiter(i); j != cells.delimiter(i + 1);
             ++j) {
          REQUIRE(isInside(cells[i], objects[j]));
        }
      }
    }
  }
}

TEST_CASE("CellsMultiLevel. CellsUniform constructor.", "[CellsMultiLevel]") {
  typedef stlib::sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef stlib::geom::BBox<Traits::Float, Traits::Dimension> Cell;
  typedef stlib::sfc::CellsUniform<Traits, Cell, true> CellsUniform;
  typedef stlib::sfc::CellsMultiLevel<Traits, Cell, true> CellsMultiLevel;
  typedef CellsUniform::Float Float;
  typedef CellsUniform::Point Point;

  // Uniformly-spaced points.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }

  // Build CellsUniform.
  CellsUniform uniform(Point{{0}}, Point{{1}}, NumLevels);
  uniform.buildCells(&objects);

  // Build CellsMultiLevel.
  CellsMultiLevel multiLevel(uniform);
  multiLevel.checkValidity();
  REQUIRE(multiLevel.lowerCorner() == uniform.lowerCorner());
  REQUIRE(multiLevel.lengths() == uniform.lengths());
  REQUIRE(multiLevel.numLevels() == uniform.numLevels());
  REQUIRE(multiLevel.size() == uniform.size());
  for (std::size_t i = 0; i != multiLevel.size(); ++i) {
    REQUIRE(multiLevel[i] == uniform[i]);
  }
}


TEST_CASE("CellsMultiLevel. Order and restore.",
          "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  const std::size_t NumLevels = 20;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, false> CellsMultiLevel;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Point;

  // Uniformly-spaced points, shuffled.
  std::vector<Point> objects(512);
  for (std::size_t i = 0; i != objects.size(); ++i) {
    objects[i][0] = Float(i) / objects.size();
  }
  std::random_shuffle(objects.begin(), objects.end());

  std::vector<Point> const originalObjects(objects);
  CellsMultiLevel cells(Point{{0}}, Point{{1}}, NumLevels);
  stlib::sfc::OrderedObjects orderedObjects;
  cells.buildCells(&objects, &orderedObjects);
  std::vector<Point> copy(originalObjects);
  orderedObjects.order(copy.begin(), copy.end());
  REQUIRE(copy == objects);
  orderedObjects.restore(copy.begin(), copy.end());
  REQUIRE(copy == originalObjects);
}


TEST_CASE("cellsMultiLevel()", "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, true> CellsMultiLevel;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Object;
  using stlib::sfc::cellsMultiLevel;

  {
    // No objects.
    std::vector<Object> objects;
    CellsMultiLevel const cells = cellsMultiLevel<CellsMultiLevel>(&objects);
    REQUIRE(objects.empty());
    REQUIRE(cells.empty());
  }
  {
    // Uniformly-spaced points.
    std::vector<Object> objects(512);
    for (std::size_t i = 0; i != objects.size(); ++i) {
      objects[i][0] = Float(i) / objects.size();
    }

    {
      // Default maximum number of objects per cell.
      CellsMultiLevel const cells = cellsMultiLevel<CellsMultiLevel>(&objects);
      REQUIRE(objects.size() == 512);
      REQUIRE_FALSE(cells.empty());
    }
    {
      CellsMultiLevel const cells =
        cellsMultiLevel<CellsMultiLevel>(&objects, 64);
      for (std::size_t i = 0; i != cells.size(); ++i) {
        REQUIRE((cells.delimiter(i + 1) - cells.delimiter(i)) <= 64);
      }
    }
    {
      CellsMultiLevel const cells =
        cellsMultiLevel<CellsMultiLevel>(&objects, 1);
      REQUIRE(cells.size() == objects.size());
    }
  }
}


TEST_CASE("cellsMultiLevel() with OrderedObjects", "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, true> CellsMultiLevel;
  typedef CellsMultiLevel::Float Float;
  typedef CellsMultiLevel::Point Object;
  using stlib::sfc::cellsMultiLevel;

  stlib::sfc::OrderedObjects orderedObjects;
  {
    // No objects.
    std::vector<Object> const original;
    std::vector<Object> objects = original;
    CellsMultiLevel const cells =
      cellsMultiLevel<CellsMultiLevel>(&objects, &orderedObjects);
    REQUIRE(objects.empty());
    REQUIRE(cells.empty());
    orderedObjects.restore(objects.begin(), objects.end());
    REQUIRE(objects == original);
  }
  {
    // Uniformly-spaced points.
    std::vector<Object> original(512);
    for (std::size_t i = 0; i != original.size(); ++i) {
      original[i][0] = Float(i) / original.size();
    }

    {
      // Default maximum number of objects per cell.
      std::vector<Object> objects = original;
      CellsMultiLevel const cells =
        cellsMultiLevel<CellsMultiLevel>(&objects, &orderedObjects);
      REQUIRE(objects.size() == 512);
      REQUIRE_FALSE(cells.empty());
      orderedObjects.restore(objects.begin(), objects.end());
      REQUIRE(objects == original);
    }
    {
      std::vector<Object> objects = original;
      CellsMultiLevel const cells =
        cellsMultiLevel<CellsMultiLevel>(&objects, &orderedObjects, 64);
      for (std::size_t i = 0; i != cells.size(); ++i) {
        REQUIRE((cells.delimiter(i + 1) - cells.delimiter(i)) <= 64);
      }
      orderedObjects.restore(objects.begin(), objects.end());
      REQUIRE(objects == original);
    }
    {
      std::vector<Object> objects = original;
      CellsMultiLevel const cells =
        cellsMultiLevel<CellsMultiLevel>(&objects, &orderedObjects, 1);
      REQUIRE(cells.size() == objects.size());
      orderedObjects.restore(objects.begin(), objects.end());
      REQUIRE(objects == original);
    }
  }
}


TEST_CASE("CellsMultiLevel. areCompatible().", "[CellsMultiLevel]")
{
  typedef stlib::sfc::Traits<1> Traits;
  typedef stlib::sfc::CellsMultiLevel<Traits, void, true> CellsMultiLevel;
  //typedef CellsMultiLevel::Code Code;
  typedef CellsMultiLevel::Point Point;
  using stlib::sfc::areCompatible;

  // Different lower corner.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{-1}}, Point{{1}}, 3);
    REQUIRE_FALSE(areCompatible(a, b));
  }
  // Different extents.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{2}}, 3);
    REQUIRE_FALSE(areCompatible(a, b));
  }
  // Different levels of refinement.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 4);
    REQUIRE_FALSE(areCompatible(a, b));
  }
  // Both empty.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    REQUIRE(areCompatible(a, b));
  }
  // One is empty.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    std::vector<Point> objects = {{{0}}};
    b.buildCells(&objects, 1);
    REQUIRE(areCompatible(a, b));
  }
  // Same level.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    std::vector<Point> objects = {{{0}}, {{0.5}}};
    a.buildCells(&objects, 1);
    b.buildCells(&objects, 1);
    REQUIRE(areCompatible(a, b));
  }
  // Different level.
  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    std::vector<Point> objects = {{{0}}, {{0.5}}};
    a.buildCells(&objects, 1);
    b.buildCells(&objects, 2);
    REQUIRE_FALSE(areCompatible(a, b));
  }

  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    {
      std::vector<Point> objects = {{{0}}, {{0.25}}};
      a.buildCells(&objects, 1);
    }
    {
      std::vector<Point> objects = {{{0.5}}, {{0.75}}};
      b.buildCells(&objects, 1);
    }
    REQUIRE(areCompatible(a, b));
  }

  {
    CellsMultiLevel a(Point{{0}}, Point{{1}}, 3);
    CellsMultiLevel b(Point{{0}}, Point{{1}}, 3);
    {
      std::vector<Point> objects = {{{0}}};
      a.buildCells(&objects, 1);
    }
    {
      std::vector<Point> objects = {{{0.5}}, {{0.75}}};
      b.buildCells(&objects, 1);
    }
    REQUIRE_FALSE(areCompatible(a, b));
  }
}
