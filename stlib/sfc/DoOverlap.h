// -*- C++ -*-

#if !defined(__sfc_DoOverlap_h__)
#define __sfc_DoOverlap_h__

/*!
  \file
  \brief Overlap tests for objects that are organized with a linear orthant trie.
*/

#include "stlib/sfc/LinearOrthantTrieTopology.h"
#include "stlib/performance/Performance.h"

namespace stlib
{
namespace sfc
{


/// Functor that detects if a box overlaps a set of object bounding boxes.
template<typename _Traits>
class DoOverlap
{
public:

  /// The space dimension.
  BOOST_STATIC_CONSTEXPR std::size_t Dimension = _Traits::Dimension;
  /// A bounding box.
  typedef typename _Traits::BBox BBox;
  
  /// Construct from the object boxes.
  DoOverlap(std::vector<BBox>&& objectBoxes);

  /// Return true if the box overlaps an object bounding box.
  /**
     \param box The bounding box that we test for overlaps.
     \param stack The stack is used as scratch data. We pass this as a parameter
     for the sake of efficiency. Note that we wouldn't want the stack to be
     member data because then the function wouldn't be thread-safe.
  */
  bool
  operator()(BBox const& queryBox, std::vector<std::size_t>* stack) const;
  
private:

  typedef LinearOrthantTrie<_Traits, BBox, true> Trie;

  std::vector<BBox> _objectBoxes;
  Trie _trie;
  LinearOrthantTrieTopology<Dimension> _topology;
};

/** \defgroup sfcDoOverlap Overlap Tests for Sets of Bounding Boxes
    @{
*/

/// For each query box, record if it overlaps any object box.
/**
   \param objectBoxes The vector of object boxes.
   \param queryBoxes The vector of query boxes.
   \return A vector of \c bool that has the same size as queryBoxes.
*/
template<typename _T, std::size_t _D>
std::vector<bool>
doOverlap(std::vector<geom::BBox<_T, _D> >&& objectBoxes,
          std::vector<geom::BBox<_T, _D> > const& queryBoxes);


/// Return true if the box overlaps a leaf bounding box.
/**
   \param trie The linear orthant trie for the objects. The cells are 
   bounding boxes.
   \param topology Cached topology for the linear orthant trie.
   \param stack The stack is used as scratch data. We pass this as a parameter
   for the sake of efficiency. Note that we wouldn't want the stack to be
   static data for the function because then the function wouldn't be 
   thread-safe.
   \param box The bounding box that we test for overlaps.

   Note that this function should be used when either the objects are not 
   available or when calculating the overlap test for the object bounding boxes
   would be too expensive. Here we only check if the input box overlaps a 
   leaf bounding box, which bounds a number of objects.
*/
template<typename _Traits, bool _StoreDel>
inline
bool
doOverlap
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, _StoreDel> const&
 trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& box);


/// Return true if the box overlaps an object bounding box.
/**
   \param trie The linear orthant trie for the objects. The cells are 
   bounding boxes.
   \param topology Cached topology for the linear orthant trie.
   \param objectBoxes Bounding boxes for the objects. These have been organized
   by the linear orthant trie.
   \param stack The stack is used as scratch data. We pass this as a parameter
   for the sake of efficiency. Note that we wouldn't want the stack to be
   static data for the function because then the function wouldn't be 
   thread-safe.
   \param queryBox The bounding box that we test for overlaps.
*/
template<typename _Traits>
bool
doOverlap
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, true> const& trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<geom::BBox<typename _Traits::Float, _Traits::Dimension> > const&
 objectBoxes,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& queryBox);


/** @} */ // End of sfcDoOverlap group.


} // namespace sfc
} // namespace stlib

#define __sfc_DoOverlap_tcc__
#include "stlib/sfc/DoOverlap.tcc"
#undef __sfc_DoOverlap_tcc__

#endif
