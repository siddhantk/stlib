// -*- C++ -*-

#if !defined(__sfc_Codes_tcc__)
#error This file is an implementation detail of Codes.
#endif

namespace stlib
{
namespace sfc
{


template<template<typename> class _Order, typename _Traits>
inline
void
checkValidityOfObjectCodes
(_Order<_Traits> const& order,
 std::vector<typename _Traits::Code> const& codes)
{
  for (auto code : codes) {
    // Check that the code itself is valid.
    assert(order.isValid(code));
  }
  assert(std::is_sorted(codes.begin(), codes.end()));
}


template<template<typename> class _Order, typename _Traits>
inline
void
checkValidityOfCellCodes
(_Order<_Traits> const& order,
 std::vector<typename _Traits::Code> const& codes)
{
  // There must at least be a guard cell.
  assert(! codes.empty());
  assert(codes.back() == _Traits::GuardCode);
  for (std::size_t i = 0; i != codes.size() - 1; ++i) {
    // Check that the code itself is valid.
    assert(order.isValid(codes[i]));
    // The codes must be strictly ascending. That is, they are sorted and
    // there are no repeated cells.
    assert(codes[i] < codes[i + 1]);
  }
}


template<template<typename> class _Order, typename _Traits, typename _T>
inline
void
checkValidityOfCellCodes
(_Order<_Traits> const& order,
 std::vector<std::pair<typename _Traits::Code, _T> > const& pairs)
{
  std::vector<typename _Traits::Code> codes(pairs.size());
  for (std::size_t i = 0; i != codes.size(); ++i) {
    codes[i] = pairs[i].first;
  }
  checkValidityOfCellCodes(order, codes);
}


template<template<typename> class _Order, typename _Traits>
inline
void
checkNonOverlapping
(_Order<_Traits> const& order,
 std::vector<typename _Traits::Code> const& codes)
{
  for (std::size_t i = 0; i != codes.size() - 1; ++i) {
    assert(codes[i + 1] >= order.location(order.next(codes[i])));
  }
}


template<template<typename> class _Order, typename _Traits, typename _T>
inline
void
checkNonOverlapping
(_Order<_Traits> const& order,
 std::vector<std::pair<typename _Traits::Code, _T> > const& pairs)
{
  for (std::size_t i = 0; i != pairs.size() - 1; ++i) {
    assert(pairs[i + 1].first >= order.location(order.next(pairs[i].first)));
  }
}


} // namespace sfc
} // namespace stlib
