// -*- C++ -*-

#if !defined(__sfc_OverlapQuery_tcc__)
#error This file is an implementation detail of OverlapQuery.
#endif

namespace stlib
{
namespace sfc
{


template<typename _Traits>
inline
OverlapQuery<_Traits>::
OverlapQuery(std::vector<BBox>&& objectBoxes) :
  _objectBoxes(std::move(objectBoxes)),
  // Build the linear orthant trie and organize the object bounding boxes.
  _trie(linearOrthantTrie<Trie>(&_objectBoxes)),
  // Cache topological information for the trie.
  _topology(_trie)
{
}


template<typename _Traits>
template<typename _OutputIterator>
inline
void
OverlapQuery<_Traits>::
operator()(BBox const& queryBox, _OutputIterator output,
           std::vector<std::size_t>* stack) const
{
  overlapQuery(_trie, _topology, _objectBoxes, stack, queryBox, output);
}


template<typename _Traits, typename _OutputIterator>
inline
void
overlapQuery
(LinearOrthantTrie<_Traits,
 geom::BBox<typename _Traits::Float, _Traits::Dimension>, true> const& trie,
 LinearOrthantTrieTopology<_Traits::Dimension> const& topology,
 std::vector<geom::BBox<typename _Traits::Float, _Traits::Dimension> > const&
 objectBoxes,
 std::vector<std::size_t>* stack,
 geom::BBox<typename _Traits::Float, _Traits::Dimension> const& queryBox,
 _OutputIterator output)
{
  typedef Siblings<_Traits::Dimension> Siblings;

  // Initialize the stack of cells.
  stack->clear();
  if (! trie.empty()) {
    stack->push_back(0);
  }
  // Traverse the trie.
  while (! stack->empty()) {
    // Get the current cell.
    std::size_t const cell = stack->back();
    stack->pop_back();
    // If the bounding box for this cell overlaps the query box.
    if (doOverlap(trie[cell], queryBox)) {
      if (topology.isInternal(cell)) {
        // Add the children to the stack.
        Siblings const& children = topology.children(cell);
        stack->insert(stack->end(), children.rbegin(), children.rend());
      }
      else {
        // Check the objects in this leaf.
        std::size_t const end = trie.delimiter(trie.next(cell));
        for (std::size_t i = trie.delimiter(cell); i != end; ++i) {
          if (doOverlap(objectBoxes[i], queryBox)) {
            *output++ = i;
          }
        }
      }
    }
  }
}


template<typename _T, std::size_t _D>
inline
container::PackedArrayOfArrays<std::size_t>
overlapQuery(std::vector<geom::BBox<_T, _D> >&& objectBoxes,
             std::vector<geom::BBox<_T, _D> > const& queryBoxes)
{
  typedef Traits<_D, _T> Traits;
  typedef geom::BBox<_T, _D> BBox;
  typedef LinearOrthantTrie<Traits, BBox, true> Trie;
  using performance::start;
  using performance::stop;
  using performance::record;

  performance::Scope _("sfc::overlapQuery()");
  record("Number of object boxes", objectBoxes.size());
  record("Number of query boxes", queryBoxes.size());

  start("Build the trie");
  // Build the linear orthant trie and organize the object bounding boxes.
  OrderedObjects orderedObjects;
  Trie trie = linearOrthantTrie<Trie>(&objectBoxes, &orderedObjects);
  stop();

  start("Cache topological information");
  // Cache topological information for the trie.
  LinearOrthantTrieTopology<Traits::Dimension> topology(trie);
  stop();

  start("Overlap queries");
  // For each query box, record the indices of the object boxes that overlap it.
  container::PackedArrayOfArrays<std::size_t> result;
  auto output = std::back_inserter(result);
  std::vector<std::size_t> stack;
  for (auto const& queryBox: queryBoxes) {
    result.pushArray();
    overlapQuery(trie, topology, objectBoxes, &stack, queryBox, output);
  }
  stop();

  start("Map indices");
  orderedObjects.mapToOriginalIndices(result.begin(), result.end());
  stop();
  record("Number of overlapping objects", result.size());

  return result;
}


template<typename _T, std::size_t _D, typename _OutputIterator>
inline
void
overlapQueryBrute(std::vector<geom::BBox<_T, _D> > const& objectBoxes,
                  geom::BBox<_T, _D> const& queryBox,
                  _OutputIterator output)
{
  for (std::size_t i = 0; i != objectBoxes.size(); ++i) {
    if (doOverlap(objectBoxes[i], queryBox)) {
      *output++ = i;
    }
  }
}


template<typename _T, std::size_t _D>
inline
container::PackedArrayOfArrays<std::size_t>
overlapQueryBrute(std::vector<geom::BBox<_T, _D> > const& objectBoxes,
                  std::vector<geom::BBox<_T, _D> > const& queryBoxes)
{
  container::PackedArrayOfArrays<std::size_t> result;
  auto output = std::back_inserter(result);
  for (auto const& queryBox: queryBoxes) {
    result.pushArray();
    overlapQueryBrute(objectBoxes, queryBox, output);
  }
  return result;
}


} // namespace sfc
} // namespace stlib
