# -*- Makefile -*-

# Using tiling and cookie-cutter meshing to mesh a boundary.

DATA_PATH = ../../../../../data/geom/mesh/32
BIN_PATH = ../../../../../examples/geom/mesh

#TARGETS = boundary.vtu mesh.vtu mesh_s.vtu mesh_sm.vtu mesh_smg.vtu mesh_smgm.vtu mesh_smgmg.vtu
TARGETS = boundary.vtu mesh.vtu mesh_s.vtu \
mesh.01.vtu \
mesh.02.vtu \
mesh.03.vtu \
mesh.04.vtu 
#mesh.05.vtu \
mesh.06.vtu \
mesh.07.vtu \
mesh.08.vtu \
mesh.09.vtu 

.SUFFIXES:
.SUFFIXES: .txt .vtu

# The default target.
all: $(TARGETS)

clean: 
	$(RM) *~

distclean: 
	$(MAKE) clean 
	$(RM) *.txt *.vtu

again: 
	$(MAKE) distclean 
	$(MAKE) 

# Implicit rules.

# Convert the ascii mesh file to a VTK XML file.
.txt.vtu:
	$(BIN_PATH)/iss2vtk33.exe $< $@

# Explicit rules.

# The boundary of the letter 'A'.
boundary.txt:
	cp $(DATA_PATH)/a.txt boundary.txt

# We need an explicit rule for the 2-D manifold.
boundary.vtu: boundary.txt
	$(BIN_PATH)/iss2vtk32.exe $< $@

# Tile with an edge length of 0.0234.  The centroid of each cell lies inside 
# the boundary.
mesh.txt: boundary.txt
	$(BIN_PATH)/tile3.exe -l 0.0234 boundary.txt mesh.txt

# Tile and remove the cells with low adjacencies.
mesh_s.txt: boundary.txt
	$(BIN_PATH)/tile3.exe -s -l 0.0234 boundary.txt mesh_s.txt


mesh.01.txt: mesh_s.txt
	$(BIN_PATH)/move_boundary3.exe -d 0.002 boundary.txt $< $@
	$(BIN_PATH)/smooth3.exe -n 2 $@ $@
	$(BIN_PATH)/top_opt3.exe $@ $@
	$(BIN_PATH)/smooth_cc3.exe -e 0.1 $@ $@

mesh.02.txt: mesh.01.txt
	$(BIN_PATH)/move_boundary3.exe -d 0.002 boundary.txt $< $@
	$(BIN_PATH)/smooth3.exe -n 2 $@ $@
	$(BIN_PATH)/top_opt3.exe $@ $@
	$(BIN_PATH)/smooth_cc3.exe -e 0.1 $@ $@

mesh.03.txt: mesh.02.txt
	$(BIN_PATH)/move_boundary3.exe -d 0.002 boundary.txt $< $@
	$(BIN_PATH)/smooth3.exe -n 2 $@ $@
	$(BIN_PATH)/top_opt3.exe $@ $@
	$(BIN_PATH)/smooth_cc3.exe -e 0.1 $@ $@

mesh.04.txt: mesh.03.txt
	$(BIN_PATH)/move_boundary3.exe boundary.txt $< $@
	$(BIN_PATH)/smooth3.exe -n 2 $@ $@
	$(BIN_PATH)/top_opt3.exe $@ $@
	$(BIN_PATH)/smooth_bc3.exe boundary.txt $@ $@ 

# End of file.