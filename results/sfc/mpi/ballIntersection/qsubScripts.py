#! /usr/bin/env python

# qsubScripts.py
# Write qsub scripts.

import sys
import os.path

if len(sys.argv) > 2:
    print("Usage:")
    print("python qsubScripts.py [options]\n")
    raise "Bad command line arguments. Exiting..."

if len(sys.argv) == 2:
    options = sys.argv[1]
else:
    options = ''
processesPerNode = 16
workingDir = os.path.abspath('.')
stlibDir = '~/Dev'

# The number of processes is 1 << shift.
for shift in range(10):
    numProcesses = 1 << shift
    numMinutes = 5
    numNodes = (numProcesses + processesPerNode - 1) / processesPerNode;

    f = open("ballIntersection_%03d" % numProcesses + ".sh", "w")
    f.write("""#!/bin/sh
### PBS ###
#PBS -N "ballIntersection_%03d"
#PBS -lwalltime=00:%02d:00
#PBS -lnodes=%d:ppn=16
#PBS -j oe
cd %s
mpirun -np %d %s/stlib/test/performance/release/sfc/mpi/ballIntersection -n=1048576 %s > ballIntersection_%03d.txt
""" % (numProcesses, numMinutes, numNodes, workingDir,
       numProcesses, stlibDir, options, numProcesses))
    f.close()

f = open("submit.sh", "w")
for shift in range(10):
    numProcesses = 1 << shift
    f.write("qsub ballIntersection_%03d.sh\n" % numProcesses)
f.close()
